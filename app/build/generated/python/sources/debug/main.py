from biocore import extract_features
from scores import miurascore
from finger import fingerfocus
import numpy as np


μ_SAME_MkII = np.array([0.18027574, 0.16816539])
σ_SAME_MkII = np.array([0.03251114, 0.02956052])
μ_DIFF_MkII = np.array([0.09122658, 0.08890403])
σ_DIFF_MkII = np.array([0.01001323, 0.00932958])
THRESHOLDS_MkII = np.array([0.04755671, 0.04859384])
ROI = [(63, 217, 10, 360), (53, 207, 10, 360)]

def extract(image, finger):
    img = list(image)
    i = 0
    for x in img:
        if x >= 0:
            img[i] = x
        else:
            img[i] = 256 + x
        i = i + 1
    img = np.reshape(img[12:], (240, 376))
    img = fingerfocus(img, ROI[finger])
    img = extract_features(img)
    img = img * 127
    img = img.ravel().astype('b')
    return img

def llr(scores):

    """
    Computes the log-likelihood ratio of a tuple of scores,
    assuming we know the mean and standard deviation of their
    distribution

    @param scoretuple (tuple) : A tuple of scores

    @return temp_score (float) : Their aggregated llr
    """

    scoretuple = list(scores)
    t = np.array(scoretuple)
    contributing = t >= np.array(THRESHOLDS_MkII)
    llrs = (((t - μ_DIFF_MkII)/σ_DIFF_MkII)**2 - ((t - μ_SAME_MkII)/σ_SAME_MkII)**2)/2 + np.log(σ_DIFF_MkII/σ_SAME_MkII)

    return llrs[contributing].sum()

def score(image1, image2):
    img1 = list(image1)
    img2 = list(image2)

    img1 = np.reshape(img1, (240, 376))
    img2 = np.reshape(img2, (240, 376))


    #img1 et img2 c'est des numpy array
    return miurascore(img1, img2)
