package tools;

import java.math.BigInteger;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECPoint;
import java.util.Arrays;

/**
 * Set of Elliptic Point manipulation tools
 * All computations are carried out on Secp256r1
 */
public interface ECTools 
{
	public static final BigInteger THREE = new BigInteger("3");
	public static final BigInteger TWO = new BigInteger("2");
	public static final BigInteger p = new BigInteger(1, Secp256r1.p);
	public static final BigInteger a = new BigInteger(1, Secp256r1.a);
	public static final BigInteger b = new BigInteger(1, Secp256r1.b);

	/**
	 * Creates an Elliptic Point from a given byte array
	 * with X and Y embedded in uncompressed format.
	 * @param rawPoint The byte array input
	 * @return the corresponding ECPoint
	 */
	public static ECPoint createPoint(byte[] rawPoint) 
	{
		BigInteger pX = new BigInteger(1, getXFromUncompressedFormat(rawPoint));
		BigInteger pY = new BigInteger(1, getYFromUncompressedFormat(rawPoint));
		return new ECPoint(pX, pY);
	}

	/**
	 * Computes the opposite of an Elliptic Point.
	 * @param P
	 * @return -P
	 */
	public static ECPoint negatePoint(ECPoint P) 
	{
		return new ECPoint(P.getAffineX(), P.getAffineY().negate().mod(p));
	}

	/**
	 * Computes multiplication of an Elliptic Point by a scalar
	 * @param P The point
	 * @param k The scalar
	 * @return k*P
	 */
	public static ECPoint scalmult(ECPoint P, BigInteger k)
	{
		// check if P = inf
		if(P.equals(ECPoint.POINT_INFINITY)) 
			return ECPoint.POINT_INFINITY;

		ECPoint R = ECPoint.POINT_INFINITY, S = P;
		int length = k.bitLength();
		byte[] binarray = new byte[length];

		// generate bit array from the point
		for(int i = 0; i<=length - 1; i++)
		{
			binarray[i] = k.mod(TWO).byteValue();
			k = k.divide(TWO); 
		}

		// baby step - giant step
		for(int i = length - 1; i >= 0; i--)
		{
			R = doublePoint(R);
			if(binarray[i] == 1) 
				R = addPoint(R, S);
		}

		return R;
	}

	/**
	 * Adds two Elliptic Points.
	 * @param r
	 * @param s
	 * @return r + s
	 */
	public static ECPoint addPoint(ECPoint r, ECPoint s) {

		if (r.equals(s))
			return doublePoint(r);
		else if (r.equals(ECPoint.POINT_INFINITY))
			return s;
		else if (s.equals(ECPoint.POINT_INFINITY))
			return r;

		BigInteger slope = (r.getAffineY().subtract(s.getAffineY())).multiply(r.getAffineX().subtract(s.getAffineX()).modInverse(p)).mod(p);

		BigInteger Xout = (slope.modPow(TWO, p).subtract(r.getAffineX())).subtract(s.getAffineX()).mod(p);

		BigInteger Yout = s.getAffineY().negate().mod(p);

		Yout = Yout.add(slope.multiply(s.getAffineX().subtract(Xout))).mod(p);

		ECPoint out = new ECPoint(Xout, Yout);
		return out;
	}

	/**
	 * Computes twice an Elliptic Point (mostly used for scalmult)
	 * @param r
	 * @return 2*r
	 */
	public static ECPoint doublePoint(ECPoint r) {
		if (r.equals(ECPoint.POINT_INFINITY)) 
			return r;
		BigInteger slope = (r.getAffineX().pow(2)).multiply(THREE);
		slope = slope.add(a);
		slope = slope.multiply((r.getAffineY().multiply(TWO)).modInverse(p));

		BigInteger Xout = slope.pow(2).subtract(r.getAffineX().multiply(TWO)).mod(p);
		BigInteger Yout = (r.getAffineY().negate()).add(slope.multiply(r.getAffineX().subtract(Xout))).mod(p);

		ECPoint out = new ECPoint(Xout, Yout);

		return out;
	}

	/**
	 * Extracts the X coordinate in an XY-Uncompressed byte array point.
	 * @param publicKeyRaw the byte array point.
	 * @return the X coordinate of publicKeyRaw
	 */
	public static byte[] getXFromUncompressedFormat(byte[] publicKeyRaw)
	{
		if (publicKeyRaw[0] != 0x04)
		{
			throw new IllegalStateException("EC component not in uncompressed format");
		}

		final int intLength = (publicKeyRaw.length - 1) / 2;
		return Arrays.copyOfRange(publicKeyRaw, 1, intLength + 1);
	}

	/**
     * Extracts the Y coordinate in an XY-Uncompressed byte array point.
     * @param publicKeyRaw the byte array point.
     * @return the Y coordinate of publicKeyRaw
     */
	public static byte[] getYFromUncompressedFormat(byte[] publicKeyRaw)
	{
		if (publicKeyRaw[0] != 0x04)
		{
			throw new IllegalStateException("EC component not in uncompressed format");
		}

		final int intLength = (publicKeyRaw.length - 1) / 2;
		return Arrays.copyOfRange(publicKeyRaw, intLength + 1, publicKeyRaw.length);
	}

	/**
	 * Extracts the XY-Uncompressed byte array point from a 
	 * Java ECPublicKey object.
	 * @param pubKey the ECPublicKey object
	 * @return the XY-Uncompressed byte array point
	 */
	public static byte[] getWAsByte(ECPublicKey pubKey) {

		byte[] prefix = {(byte) 0x04};
		byte[] arrX = pubKey.getW().getAffineX().toByteArray();
		byte[] arrY = pubKey.getW().getAffineY().toByteArray();
		byte[] baX = null;
		byte[] baY = null;
		if(arrX.length == 33) {
			baX = Arrays.copyOfRange(arrX, 1, arrX.length);
		}
		else {
			baX = arrX;
		}

		if(arrY.length == 33) {
			baY = Arrays.copyOfRange(arrY, 1, arrY.length);
		}
		else {
			baY = arrY;
		}
		return Utils.concatByteArrays(prefix, baX, baY );
	}

	public static byte[] getWAsByte(ECPoint P)
	{
		byte[] prefix = {(byte) 0x04};
		byte[] arrX = P.getAffineX().toByteArray();
		byte[] arrY = P.getAffineY().toByteArray();
		byte[] baX = null;
		byte[] baY = null;

		if(arrX.length == 31)
		{
			baX = new byte[32];
			baX[0] = (byte) 0x00;
			for(int i = 0; i < arrX.length; ++i)
				baX[i+1] = arrX[i];
		}
		else
		{
			if(arrX.length == 33)
				baX = Arrays.copyOfRange(arrX, 1, arrX.length);
			else
				baX = arrX;
		}

		if(arrY.length == 31)
		{
			baY = new byte[32];
			baY[0] = (byte) 0x00;
			for(int i = 0; i < arrY.length; ++i)
				baY[i+1] = arrY[i];
		}
		else
		{
			if(arrY.length == 33)
				baY = Arrays.copyOfRange(arrY, 1, arrY.length);
			else
				baY = arrY;
		}
		return Utils.concatByteArrays(prefix, baX, baY);
	}
	
	/**
	 * Checks if a given point is on the curve SECP256R1.
	 * @param point
	 * @return true if it is, false if it isn't
	 */
	public static boolean verifyPoint(ECPoint point) 
    {
        BigInteger x = point.getAffineX();
        BigInteger y = point.getAffineY();

        return (y.modPow(TWO, p).equals(  (x.modPow(THREE, p).add(a.multiply(x))).add(b).mod(p)  ) ); 
    }
}