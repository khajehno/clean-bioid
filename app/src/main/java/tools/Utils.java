package tools;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


//Set of utility functions
public interface Utils 
{
    //Concatenates several byte arrays left-to-right
    public static byte[] concatByteArrays(byte[]... byteArrays) 
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        
        for(byte[] currentByteArray : byteArrays)
        {   
            if (currentByteArray == null) 
                continue;

            try 
            {
                outputStream.write(currentByteArray);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        return outputStream.toByteArray();
    }
    
    //Converts a byte array to an hexadecimal string
	public static String byteArraytoHexString(byte[] data) 
	{
		StringBuilder sb = new StringBuilder();
		
		for (byte b: data)
			sb.append(String.format("%02x", b&0xff));
		
		return sb.toString();
	}

	//Converts an hexadecimal string to a byte array
	public static byte[] hexStringToByteArray(String s) 
	{
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2)
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i+1), 16));

		return data;
	}

	//Removes one leading zero in a byte array
	public static byte[] shrink(byte[] arr) 
	{
		if (arr[0] == 0) 
		{
			byte[] tmp = new byte[arr.length - 1];
			System.arraycopy(arr, 1, tmp, 0, tmp.length);
			arr = tmp;
		}

		return arr;
	}

	//Key Derivation Function, computes SHA-256(data || param)
	public static byte[] KDF(byte[] data, int param) throws NoSuchAlgorithmException 
	{
		byte[] kdfInput = Utils.concatByteArrays(data, new byte[]{(byte)param});
		MessageDigest md = MessageDigest.getInstance("SHA-256");

		return md.digest(kdfInput);
	}

	//Converts a long to a byte array in big endian
	public static byte[] longToBytes(long l) 
	{
		int sizeRatio = Long.SIZE/Byte.SIZE;
		byte[] result = new byte[sizeRatio];

		for (int i = sizeRatio-1; i >= 0; i--) 
		{
			result[i] = (byte)(l & 0xFF);
			l >>= sizeRatio;
		}

		return result;
	}

	//Converts a byte array in big endian to a long
	public static long bytesToLong(byte[] b) 
	{
		int sizeRatio = Long.SIZE/Byte.SIZE;
		long result = 0;

		for (int i = 0; i < sizeRatio; i++) 
		{
			result <<= sizeRatio;
			result |= (b[i] & 0xFF);
		}

		return result;
	}

	//Appends (or not) some lines to the file at path
//	public static boolean printLinesToFile(String path, boolean append, String... lines)
//	{
//		try(PrintWriter file = new PrintWriter(new FileOutputStream(path, append)))
//		{
//			for(String s : lines)
//				file.println(s);
//			return true;
//		}
//		catch(FileNotFoundException e)
//		{
//			e.printStackTrace();
//			return false;
//		}
//	}
}