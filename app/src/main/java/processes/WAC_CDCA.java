package processes;

import com.shayan.BioID.MatchingClient;

import libraries.CardException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.ECPublicKeySpec;
import java.util.Arrays;
import java.util.Random;
import javax.crypto.KeyAgreement;
import javax.crypto.spec.IvParameterSpec;

import libraries.ResponseAPDU;
import tools.ECTools;
import tools.Secp256r1;
import tools.Utils;

/**
 * Weak Access Control / Confirmer Data Chip Auth Protocol.
 * Establishes a secure channel with the card thanks to a password
 * and retrieves the ID data from it.
 * @author Julien Corsin SC-MA1 @ LASEC EPFL
 * @author Betül Durak PhD @ LASEC EPFL
 */
public final class WAC_CDCA extends Terminal {
    //Weak Access Control instructions
    private final byte INS_GET_M = (byte) 0xA6; //Get M from chip
    private final byte INS_SEND_KL = (byte) 0xA7; //Send L and Kverify to chip

    //Confirmer Data/Chip Authentication instructions
    private final byte INS_SEND_NT_REC_NC = (byte) 0xA8;

    //Initialisation vector for AES
    private final byte[] IV = new byte[16];

    public WAC_CDCA() throws CardException {}

    public String[] protocol(String... args) throws Exception, CardException, IOException, GeneralSecurityException
    {
        MatchingClient.print("");
        MatchingClient.print("WAC/CDCA Protocol started !\n");

        MatchingClient.print("Password access control...");
        KeyPairGenerator kg = KeyPairGenerator.getInstance("EC");
        kg.initialize(new ECGenParameterSpec("secp256r1")); // NIST P-256

        //Generate b and B
        KeyPair kp = kg.generateKeyPair();
        ECPublicKey kB = (ECPublicKey) kp.getPublic();
        ECPrivateKey kb = (ECPrivateKey) kp.getPrivate();

        ECPoint B = kB.getW();

        BigInteger pwd = new BigInteger(1, args[0].getBytes());
        ECPoint pwdG2 = ECTools.scalmult(ECTools.createPoint(Secp256r1.G2), pwd);
        ECPoint pwdG3 = ECTools.scalmult(ECTools.createPoint(Secp256r1.G3), pwd);
        MatchingClient.print("B");
        ResponseAPDU rapdu = getData(INS_GET_M, 1); //Get rawM from card
        MatchingClient.print("C");
        ECPoint A = ECTools.addPoint(ECTools.createPoint(rapdu.getData()), ECTools.negatePoint(pwdG2)); //A = M - pwd*G2

        KeyAgreement ka = KeyAgreement.getInstance("ECDH");

        //Initialize key agreement with b
        ka.init(kb);

        //Initialize key agreement with A
        ka.doPhase(KeyFactory.getInstance("EC").generatePublic(new ECPublicKeySpec(A, kB.getParams())), true);

        //x coordinate of K = b*A
        byte[] rawxK = ka.generateSecret();

        //Hashing it with SHA-1 because javacard uses ALG_EC_SVDP_DH (not PLAIN) //TODO make javacard use PLAIN?
        byte[] hashed_ss = MessageDigest.getInstance("SHA-1").digest(rawxK);
        byte[] kverifyT = Utils.KDF(hashed_ss, 7);

        rapdu = sendData(INS_SEND_KL, Utils.concatByteArrays(kverifyT, ECTools.getWAsByte(ECTools.addPoint(B, pwdG3)))); //L = B + pwd*G3

        MatchingClient.print("✓\n");

        MatchingClient.print("Key exchange...");
        byte[] kenc = Utils.KDF(hashed_ss, 8); //Kenc = KDF8(rawxK)
        byte[] kEncWAC = Arrays.copyOfRange(kenc, 0, 16);
        MatchingClient.print("✓\n");

        MatchingClient.print("Decrypting ID data...");
        IvParameterSpec ivspec = new IvParameterSpec(IV);

        //Reveal DG2 with encrypted channel
        byte[] dg2 = recoverBioData(kEncWAC, ivspec, INDEX_DG2);

        String[] idata = parseID(new String(Arrays.copyOfRange(dg2, 12, dg2.length), StandardCharsets.UTF_8));

        MatchingClient.print("✓\n");
        MatchingClient.print("Getting data authentication token...");

        // Confirmer data/chip authentication
        byte[] u_chip = Arrays.copyOfRange(dg2, 0, 4);  //4  bytes
        byte[] m = Utils.KDF(dg2, 6); //32 bytes

        //Getting a random string of l bits
        byte[] N_t = new byte[32];
        new Random().nextBytes(N_t); //32 bytes
        //Getting time in a byte array
        long time = System.currentTimeMillis();
        byte[] time_raw = Utils.longToBytes(time); // 8 bytes

        //Getting ready to encrypt the data
        byte[] pad = new byte[] {0,0,0,0,0,0,0,0};
        byte[] data = Utils.concatByteArrays(N_t, time_raw, pad);

        rapdu = sendEncryptedData(INS_SEND_NT_REC_NC, data, kEncWAC, ivspec);

        byte[] rec = decrypt(rapdu.getData(), kEncWAC, ivspec, (short) 64, false);

        if(( 64 < rec.length && rec[64] != 0) || (rec.length < 64))
            throw new IllegalStateException("N_c is not l bits long!");

        MatchingClient.print("✓\n");

        byte[] N_c = Arrays.copyOfRange(rec, 0, 32); //32 bytes
        byte[] sig = Arrays.copyOfRange(rec, 32, 64); //32 bytes
        byte[] wholeData = Utils.concatByteArrays(u_chip, N_c, N_t, time_raw, m, sig); // 32 + 32 + 8 + 32 + 4 + 32 = 140

        String wholeDataString = Utils.byteArraytoHexString(wholeData);

        try(BufferedReader in = new BufferedReader(new InputStreamReader(new URL(String.format("http://%s:%s/confirmWAC?input=%s", args[1], args[2], wholeDataString)).openStream())))
        {
            String response;
            while ((response = in.readLine())!= null)
            {
                if (response.equals("1"))
                    MatchingClient.print("Data authenticated by confirmer ✓\n");
                else
                    System.out.println("Server: " + response);
            }
        }
        catch(IOException e)
        {
            throw new IOException("Could not reach the confirmer server");
        }

        return idata;
    }
}