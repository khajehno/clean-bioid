package processes;

import libraries.CardException;

import com.shayan.BioID.MatchingClient;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.ECPrivateKeySpec;
import java.security.spec.ECPublicKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.zip.InflaterOutputStream;

import javax.crypto.KeyAgreement;
import javax.crypto.spec.IvParameterSpec;

import libraries.ResponseAPDU;

import tools.ECTools;
import tools.Utils;

/**
 * Strong Access Control / Data Chip Auth Protocol.
 * Establishes a secure channel with the card and 
 * retrieves the biometric and ID data from it.
 * @author Julien Corsin SC-MA1 @ LASEC EPFL
 * @author Betül Durak PhD @ LASEC EPFL
 */
public final class SAC_DCA extends Terminal
{
    //Secure Channel Establishment instructions
    private final byte INS_SEND_PUB = (byte) 0xB1; //Send terminal public key
    private final byte INS_GET_PUB_C = (byte) 0xB2; //Receive chip public key
    private final byte INS_GEN_SS = (byte) 0xB3; //Generate shared secret on the chip
    private final byte INS_SEND_VERIFY = (byte) 0xB4; //Send verification key

    //Data/Chip Authentication instruction
    private final byte INS_REC_PK_ISSUER = (byte) 0xC0;
    private final byte INS_SEND_H = (byte) 0xC1; //Send h to chip get U from chip
    private final byte INS_GET_R = (byte) 0xC2; //Get R from chip
    private final byte INS_SEND_v = (byte) 0xC3; //Send v
    private final byte INS_SEND_r = (byte) 0xC4; //Send r
    private final byte INS_GET_new_s = (byte) 0xC5; //Get s'

    //Initialisation vector for AES
    private final byte[] IV = new byte[16];

    //Private key of the terminal
    private final byte[] PRIVATE_KEY_TERMINAL = {
        (byte) 0x51, (byte) 0xc7, (byte) 0x4e, (byte) 0x1d, (byte) 0xa7, (byte) 0x05, (byte) 0x83, (byte) 0x83, 
        (byte) 0xc0, (byte) 0xc8, (byte) 0x7c, (byte) 0xac, (byte) 0xed, (byte) 0x7d, (byte) 0x62, (byte) 0x04, 
        (byte) 0x0f, (byte) 0x81, (byte) 0xfa, (byte) 0x47, (byte) 0xd8, (byte) 0x20, (byte) 0xc8, (byte) 0x37, 
        (byte) 0xa7, (byte) 0x39, (byte) 0xfe, (byte) 0x3d, (byte) 0x22, (byte) 0x79, (byte) 0xc3, (byte) 0x6b};

    //Public key of the certificate issuer
    private final byte[] PUBLIC_KEY_ISSUER = { //TODO: Put in Terminal.java as it's also needed in LBT.java
        (byte) 0x04, (byte) 0xab, (byte) 0xc1, (byte) 0xdc, (byte) 0x2d, 
        (byte) 0x4b, (byte) 0xcb, (byte) 0x24, (byte) 0xa5, (byte) 0x9e, 
        (byte) 0x57, (byte) 0x04, (byte) 0xc4, (byte) 0x40, (byte) 0x24, 
        (byte) 0x23, (byte) 0x1b, (byte) 0x02, (byte) 0x89, (byte) 0xb5, 
        (byte) 0x8f, (byte) 0x68, (byte) 0xdb, (byte) 0x01, (byte) 0x2a, 
        (byte) 0x83, (byte) 0x3e, (byte) 0xc2, (byte) 0xe0, (byte) 0x84, 
        (byte) 0x64, (byte) 0xa7, (byte) 0x95, (byte) 0xcf, (byte) 0x1a, 
        (byte) 0x6a, (byte) 0xb4, (byte) 0xce, (byte) 0x25, (byte) 0x95, 
        (byte) 0x04, (byte) 0xef, (byte) 0x6b, (byte) 0xd3, (byte) 0x23, 
        (byte) 0xb4, (byte) 0xb9, (byte) 0x2f, (byte) 0x51, (byte) 0x82, 
        (byte) 0xbf, (byte) 0x69, (byte) 0x4c, (byte) 0xd4, (byte) 0xc2, 
        (byte) 0xd9, (byte) 0xbe, (byte) 0x68, (byte) 0x25, (byte) 0x3e, 
        (byte) 0xd1, (byte) 0x67, (byte) 0x66, (byte) 0x31, (byte) 0x03};

    public List<byte[]> getListImages() {
        return listImages;
    }

    private List<byte[]> listImages = new ArrayList<>();

    public SAC_DCA() throws CardException {}



    @Override
    public String[] protocol(String... args) throws Exception, GeneralSecurityException
    {
        MatchingClient.print("");
        MatchingClient.print("SAC/DCA Protocol started !\n");

        MatchingClient.print("Terminal authentication...✓\n");
        MatchingClient.print("Key exchange...");

        //Generate key pair for the terminal using PRIVATE_KEY_TERMINAL array
        //Generate curve parameter spec
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC");
        kpg.initialize(new ECGenParameterSpec ("secp256r1")); // NIST P-256 

        KeyPair kp = kpg.generateKeyPair(); //Generate key pair for obtaining parameters

        ECParameterSpec spec = ((ECPublicKey) kp.getPublic()).getParams(); //Getting parameters thanks to temporary public key
        ECPoint generator = spec.getGenerator(); //Obtain generator from specs  
        BigInteger privKey = new BigInteger(1, PRIVATE_KEY_TERMINAL); // Initialize private key here  
        ECPoint pubPoint = ECTools.scalmult(generator, privKey); //generate publickey
        KeyFactory kf = KeyFactory.getInstance("EC"); //create key factory to generate public key    
        ECPublicKey pubKeyT = (ECPublicKey) kf.generatePublic(new ECPublicKeySpec(pubPoint, spec)); //create public key
        ECPrivateKey privKeyT = (ECPrivateKey) kf.generatePrivate(new ECPrivateKeySpec(privKey, spec));

        byte[] baPubKeyT;
        baPubKeyT = ECTools.getWAsByte(pubKeyT);
        KeyAgreement ka = KeyAgreement.getInstance("ECDH");

        // send public key Y
        ResponseAPDU rapdu = sendData(INS_SEND_PUB, baPubKeyT, 1);

        // get public key of the chip.
        rapdu = getData(INS_GET_PUB_C, 1);

        // create public key point

        byte[] rawPubKey = rapdu.getData();

        // obtain curve coordinates
        BigInteger PubX = new BigInteger(1, ECTools.getXFromUncompressedFormat(rawPubKey));
        BigInteger PubY = new BigInteger(1, ECTools.getYFromUncompressedFormat(rawPubKey));    
        ECPoint chipPub = new ECPoint(PubX, PubY); // create curve point  
        ECPublicKeySpec chipSpec = new ECPublicKeySpec(chipPub, spec); // create public key specification       
        ECPublicKey pubKeyC = (ECPublicKey) kf.generatePublic(chipSpec); // obtain the public key object

        // check if the public key is a valid point on the curve
        if(pubKeyC.getW().equals(ECPoint.POINT_INFINITY) || !ECTools.verifyPoint(pubKeyC.getW())) {
            throw new CardException("Malformed curve point");
        }
        // store public key of the chip as a byte array (for KDF).
        byte[] baPubKeyC = ECTools.getWAsByte(pubKeyC);

        // initialize key agreement with private key
        ka.init(privKeyT);
        // initialize key agreement with pubKeyC
        ka.doPhase(pubKeyC, true);
        // generate shared secret
        byte[] ss = ka.generateSecret();
        // obtain SHA-1 digest object
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        // obtain SHA-1 digest of the shared secret
        byte[] hashed_ss = md.digest(ss);

        // concat. hashed shared secret and pubKeyC back to back
        byte[] kdfData = Utils.concatByteArrays(hashed_ss, baPubKeyC);
        // generate encryption key using KDF with parameter 1
        byte[] kEncHelper = Utils.KDF( kdfData, 1);
        //Was new byte[16] attribute
        byte[] kEnc = Arrays.copyOfRange(kEncHelper, 0, 16);
        // generate verification key using KDF with parameter 2
        byte[] kVerify = Utils.KDF(kdfData, 2);

        // generate shared secret on the chip
        rapdu = getData(INS_GEN_SS, 1);
        rapdu = sendData(INS_SEND_VERIFY, kVerify, 1);

        MatchingClient.print("✓\n");
        MatchingClient.print("Decrypting biometric/ID data...");

        IvParameterSpec ivspec = new IvParameterSpec(IV);

        // Reveal DG2 and DG3 with encrypted channel
        byte[] dg2 = recoverBioData(kEnc, ivspec, INDEX_DG2); // u_chip || password ||  name || lastName || dateOfBirth = 4+8+ 15+15+9
        byte[] dg3 = recoverBioData(kEnc, ivspec, INDEX_Bio);

        String[] idata = parseID(new String(Arrays.copyOfRange(dg2, 12, dg2.length), StandardCharsets.UTF_8));

        int byLenL = (dg3[1] << 8) | (dg3[0] & 0xFF);
        int byLenC = (dg3[3] << 8) | (dg3[2] & 0xFF);

        byte[] byLCompressed = Arrays.copyOfRange(dg3, 4, 4 + byLenL);
        //TODO: remove byC, it is remaining code from when we had three images from the scanner and not two
        byte[] byC = Arrays.copyOfRange(dg3, 4 + byLenL, 4 + byLenL + byLenC);
        byte[] byRCompressed = Arrays.copyOfRange(dg3, 4 + byLenL + byLenC, dg3.length);

        byte[] byL = decompress(byLCompressed);
        byte[] byR = decompress(byRCompressed);

        listImages.clear();
        listImages.add(byL);
        listImages.add(byR);

        MatchingClient.print("✓\n");

        // verify the public key of the issuer coming from the chip : VERIFY_PK_ISSUER
        // get public key of the chip.
        rapdu = getData(INS_REC_PK_ISSUER, 1);

        byte[] tempPKi = rapdu.getData();
        tempPKi = decrypt(tempPKi, kEnc, ivspec, (short) 65, true);
        // check if the public key is valid
        if( !Utils.byteArraytoHexString(tempPKi).equals(Utils.byteArraytoHexString(PUBLIC_KEY_ISSUER))) {
            throw new CardException("issuer public key verification failed");
        }
        // generate r
        byte[] r = new byte[32];
        Random rand = new Random();
        rand.nextBytes(r);
        byte[] range = new byte[32]; // generate v in Z_q
        rand.nextBytes(range);
        BigInteger q = spec.getOrder();
        BigInteger bigV = new BigInteger(1, range);
        while(bigV.compareTo(bigV.mod(q)) != 0 || bigV.equals(BigInteger.ZERO)) { // sample again if larger than q, to get rid of bias
            rand.nextBytes(range);
            bigV = new BigInteger(1, range);
        }
        byte[] v = bigV.toByteArray();
        v = Utils.shrink(v);
        byte[] h = Utils.KDF(Utils.concatByteArrays(v,r), 5); // calculate h = H_5(v | r)
        // send h to the card and receive U  
        rapdu = sendEncryptedData(INS_SEND_H, h, kEnc, ivspec);

        byte[] tempU = rapdu.getData();
        tempU = decrypt(tempU, kEnc, ivspec, (short) 65, true);
        ECPoint U = ECTools.createPoint(tempU);


        // receive R from the card
        rapdu = getData(INS_GET_R, 1);

        byte[] tempR = rapdu.getData();
        tempR = decrypt(tempR, kEnc, ivspec, (short) 65, true);
        ECPoint R = ECTools.createPoint(tempR);

        if(U.equals(ECPoint.POINT_INFINITY) || !ECTools.verifyPoint(U) || R.equals(ECPoint.POINT_INFINITY) || !ECTools.verifyPoint(R))
            throw new IllegalArgumentException("Malformed curve point");
        // send v
        rapdu = sendEncryptedData(INS_SEND_v, v, kEnc, ivspec);

        // send r
        rapdu = sendEncryptedData(INS_SEND_r, r, kEnc, ivspec);

        // get s'
        rapdu = getData(INS_GET_new_s, 1);

        byte[] tempS = rapdu.getData();  
        tempS = decrypt(tempS, kEnc, ivspec, (short) 32, false);
        BigInteger sNew = new BigInteger(1, tempS);

        /** computation of e
         * byte[] hashData = Utils.concatByteArrays( dg2, dg3, tempR);
         * byte[] e = Utils.KDF( kdfData, 4);
         * sNew*G + e*PK_issuer==? R + v*U
         **/
        byte[] e = Utils.KDF(Utils.concatByteArrays(dg2,dg3, tempR), 4);
        ECPoint vU = ECTools.scalmult(U, new BigInteger(1, v)); //generate publickey  which is vU
        ECPoint Rplus_vU = ECTools.addPoint(R, vU);
        ECPoint PKI = ECTools.createPoint(PUBLIC_KEY_ISSUER);
        BigInteger eBigInt = new BigInteger(1, e);
        ECPoint ePKI = ECTools.scalmult(PKI, eBigInt );
        ECPoint sNewG = ECTools.scalmult(generator, sNew);
        ECPoint sNewGplus_eBigIntPKI = ECTools.addPoint(ePKI, sNewG);

        if(!Rplus_vU.equals(sNewGplus_eBigIntPKI)) 
            throw new CardException("s' is not verified");

        if(!sNew.equals(sNew.mod(q)) || sNew== BigInteger.ZERO) 
            throw new CardException("s' not in Z_q*");

        MatchingClient.print("Data/Chip authentication...✓\n");

        return idata;
    }

    public static byte[] decompress(byte[] in) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InflaterOutputStream infl = new InflaterOutputStream(out);
            infl.write(in);
            infl.flush();
            infl.close();

            return out.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(150);
            return null;
        }
    }
}