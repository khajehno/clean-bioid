package processes;

import com.shayan.BioID.MatchingClient;
import com.shayan.BioID.Enrollment;

import libraries.CardException;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import libraries.CommandAPDU;
import libraries.ResponseAPDU;
import tools.Utils;

/**
 * Abstract class representing a connection to the SmartCard terminal.
 * Implements the AutoCloseable interface so that the connection is cleanly closed on protocol end.
 * @author Julien Corsin SC-MA1 @ LASEC EPFL
 * @author Betül Durak PhD @ LASEC EPFL
 */
abstract class Terminal
{
    //To differenciate what from what class are we calling the functions
    protected final int ENROLLMENT = 0;
    protected final int MATCHING_CLIENT = 1;


    //Applet ID
    protected final byte[] AID = {
            (byte)0xA0, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x52,
            (byte)0x03, (byte)0x01, (byte)0x0C, (byte)0x05, (byte)0x03};

    //Command class
    protected final byte CLA = 0x00;
    public int ok = 1;
    //Select Instruction
    protected final byte INS_SELECT = (byte) 0xA4;

    //Maximum bytes for transfer
    protected final short MAX_BUFFER_BYTES = (short) 240; //Change the max buffer bytes to 96....

    //Biometric data EEPROM indices
    protected final short INDEX_SIG = (short) 0;
    protected final short INDEX_PKterm = (short) 1;
    protected final short INDEX_PKissuer = (short) 2;
    protected final short INDEX_Bio = (short) 3;
    protected final short INDEX_DG2 = (short) 4;
    protected final short INDEX_Kchip = (short) 5;
    protected final short INDEX_PWDG = (short) 6;


    //Instructions for recovering biometric data
    private final byte INS_REQ_DATASIZE = (byte) 0xD4;
    private final byte INS_REC_EEPROM = (byte) 0xD5;

    /**
     * Actual protocol implemented by the concrete subclass
     * @param args Input data
     * @return Identity data extracted from the card if the protocol is WAC/SAC, null for LBT
     * @throws CardException if the communication with the card failed at some point
     * @throws IOException if WAC's confirmer cannot be reached, or if LBT's picture copy failed
     * @throws GeneralSecurityException if some cryptographic operation failed at some point
     */
    public abstract String[] protocol(String... args) throws CardException, IOException, GeneralSecurityException, Exception;


    /**
     * Send data to the card.
     * @param ins Instruction used
     * @param data Data sent
     * @return ResponseAPDU with the card's response
     * @throws CardException if the card's response contains an error
     */
    public ResponseAPDU sendData(byte ins, byte[] data) throws Exception {
        return sendData(ins, data, ENROLLMENT);
    }

    public ResponseAPDU sendData(byte ins, byte[] data, int client) throws Exception
    {
        ResponseAPDU r;
        //need to differentiate if we use the "transceives" method from "Enrollment" or from "MatchingClient"
        //TODO: optimize this, so "transceives" becomes a general method of the app
        if (client == ENROLLMENT) {
            r = Enrollment.transceives(new CommandAPDU(CLA, ins, 0, 0, data));
        }
        else{
            r = MatchingClient.transceives(new CommandAPDU(CLA, ins, 0, 0, data));
        }
        if (r.getSW() != 0x9000)
            throw new Exception(String.format("Instruction 0x%X returned 0x%X : ", ins, r.getSW()));
        return r;
    }

    /**
     * Receives data from the card.
     * @param ins Instruction used
     * @return ResponseAPDU with the card's response
     * @throws CardException if the card's response contains an error
     */

    public ResponseAPDU sendData(byte ins) throws Exception {
        return getData(ins,ENROLLMENT);
    }
    public ResponseAPDU getData(byte ins, int client) throws Exception
    {
        ResponseAPDU r;
        //need to differentiate if we use the "transceives" method from "Enrollment" or from "MatchingClient"
        //TODO: optimize this, so "transceives" becomes a general method of the app
        if (client == ENROLLMENT) {
            r = Enrollment.transceives(new CommandAPDU(CLA, ins, 0, 0, 0));

        } else {
            r = MatchingClient.transceives(new CommandAPDU(CLA, ins, 0, 0, 0));

        }
        if (r.getSW() != 0x9000)
            throw new CardException(String.format("Instruction 0x%X returned 0x%X : ", ins, r.getSW()) + exceptionIdentifier(r.getSW()));
        return r;
    }

    /**
     * Uses an AES cipher in CBC/NOPAD mode to encrypt the data and sends it to the card.
     * @param ins Instruction used
     * @param data Data sent
     * @param symKey The symmetric key used
     * @param ivspec IV used
     * @return ReponseAPDU with card's response
     * @throws CardException if the card's response contains an error
     * @throws GeneralSecurityException if the data encryption failed
     */
    public ResponseAPDU sendEncryptedData(byte ins, byte[] data, byte[] symKey, IvParameterSpec ivspec) throws Exception, GeneralSecurityException
    {
        SecretKeySpec skey = new SecretKeySpec(symKey, "AES");
        Cipher c = Cipher.getInstance("AES/CBC/NoPadding");
        c.init(Cipher.ENCRYPT_MODE, skey, ivspec);

        return sendData(ins, c.doFinal(data), MATCHING_CLIENT);
    }

    /**
     * Uses an AES cipher in CBC/NOPAD mode to decrypt the data transferred on the secure channel.
     * @param ciphertext Ciphertext received
     * @param symKey The symmetric key used
     * @param ivspec IV used
     * @param isPadded if the data was padded prior to encryption
     * @param plaintextLengthWithoutPadding The data length prior to encryption
     * @return the decrypted data
     * @throws GeneralSecurityException if the data decryption failed
     */
    public byte[] decrypt(byte[] ciphertext, byte[] symKey, IvParameterSpec ivspec, short plaintextLengthWithoutPadding, boolean isPadded) throws GeneralSecurityException
    {
        SecretKeySpec skey = new SecretKeySpec(symKey, "AES");
        Cipher c = Cipher.getInstance("AES/CBC/NoPadding");
        c.init(Cipher.DECRYPT_MODE, skey, ivspec);
        byte[] plaintext = c.doFinal(ciphertext);

        if(isPadded) // we need to trim from 80 bytes to ciphertextLengthWithoutPadding bytes
            return Arrays.copyOfRange(plaintext, 0, plaintextLengthWithoutPadding);

        return plaintext;
    }

    /**
     * Asks the card for some data in its EEPROM
     * @param symKey The symmetric key used
     * @param ivspec The IV used
     * @param dataID The ID of the data we want to retrieve
     * @return the data retrieved
     * @throws CardException if the card's answer contains an error
     * @throws GeneralSecurityException if the decryption failed
     */
    protected byte[] recoverBioData(byte[] symKey, IvParameterSpec ivspec, short dataID) throws Exception {
        byte[] baDataID = ByteBuffer.allocate(2).putShort(dataID).array();

        ResponseAPDU rapdu = sendData(INS_REQ_DATASIZE, baDataID, MATCHING_CLIENT);

        short dataSize = ByteBuffer.wrap(rapdu.getData()).getShort();
        byte[] data = new byte[dataSize];

        for(int i = 0; i < dataSize; i += MAX_BUFFER_BYTES)
        {
            short bytesToProcess = (dataSize - i) >= MAX_BUFFER_BYTES ? MAX_BUFFER_BYTES : (short) (dataSize - i);

            byte[] baOffset = ByteBuffer.allocate(2).putShort((short) i).array();
            byte[] baSize = ByteBuffer.allocate(2).putShort(bytesToProcess).array();
            byte[] request = Utils.concatByteArrays(baDataID, baOffset, baSize);

            byte[] buffer = sendData(INS_REC_EEPROM, request, MATCHING_CLIENT).getData();
            buffer = decrypt(buffer, symKey, ivspec, bytesToProcess, (dataSize - i) >= MAX_BUFFER_BYTES ? false : true); //4th parameter was hardcoded (short) 256 in case (dataSize - i) >= MAX_BUFFER_BYTES, but it doesn't matter if the data is not padded.
            System.arraycopy(buffer, 0, data, i, bytesToProcess);
        }
        return data;
    }

    /**
     * Parses the ID data from the raw String extracteed from EEPROM
     * @param rawIDString The raw String
     * @return an array of 3 Strings : Last name, First name, Date/Of/Birth
     */
    protected String[] parseID(String rawIDString)
    {
        String[] idata = {"", "", ""};

        for(int i = 0; i < rawIDString.length(); ++i)
        {
            if(rawIDString.charAt(i) != 0 && i < 15)
                idata[1] += rawIDString.charAt(i);
            if(rawIDString.charAt(i) != 0 && i > 14 && i < 30)
                idata[0] += rawIDString.charAt(i);
            if(rawIDString.charAt(i) != 0 && i > 29 && i < rawIDString.length())
                idata[2] += rawIDString.charAt(i);
        }

        idata[2] = idata[2].substring(0, 2) + '/' + idata[2].substring(2, 4) + '/' + idata[2].substring(4);

        return idata;
    }

    /**
     * Gets a proper error message from a JavaCard ISO 7816 SW code
     * @param exceptionID The SW code
     * @return The error message
     */
    private String exceptionIdentifier(int exceptionID)
    {
        switch(exceptionID)
        {
            case 0x6000:
                return "INVALID DATAID TO READ/WRITE";
            case 0x6001:
                return "NO BIOMETRY DATA IS STORED";
            case 0x6003:
                return "INVALID ARGUMENT!";
            case 0x6F00:
                return "NO PRECISE DIAGNOSIS FOUND!";
            case 0xF100:
                return "NO SUCH INSTRUCTION";
            case 0xF101:
                return "INSTRUCTION ORDER IS NOT FOLLOWED!";
            case 0xF0B1:
                return "EC SK/PK PAIR COULD NOT BE GENRATED!";
            case 0xF0B3:
                return "SHARED SECRET KEY GENERATION FAILED!";
            case 0xF0B4:
                return "VERIFICATION FAILED!";
            case 0xF0C1:
                return "GENERATING PUBLIC KEY FAILED!";
            case 0xF0C2:
                return "v IS ZERO!";
            case 0xF0C3:
                return "v IS NOT IN Z*_q!";
        }
        return "THE ERROR IS NOT LISTED";
    }

}