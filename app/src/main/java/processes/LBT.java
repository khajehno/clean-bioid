package processes;

import android.util.Base64;

import libraries.CardException;
import com.shayan.BioID.Enrollment;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Random;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECPoint;

import tools.ECTools;
import tools.Secp256r1;
import tools.Utils;

/**
 * Load Biometry Protocol, destined to push biometric data and some keys inside the card
 * @author Julien Corsin SC-MA1 @ LASEC EPFL
 * @author Betül Durak PhD @ LASEC EPFL
 */
public final class LBT extends Terminal
{
    //Public key from the certificate issuer
    private final byte[] PUBLIC_KEY_ISSUER = { //TODO: Unify in Termial.java since it's also needed in SAC_DCA
            (byte) 0x04, (byte) 0xab, (byte) 0xc1, (byte) 0xdc, (byte) 0x2d,
            (byte) 0x4b, (byte) 0xcb, (byte) 0x24, (byte) 0xa5, (byte) 0x9e,
            (byte) 0x57, (byte) 0x04, (byte) 0xc4, (byte) 0x40, (byte) 0x24,
            (byte) 0x23, (byte) 0x1b, (byte) 0x02, (byte) 0x89, (byte) 0xb5,
            (byte) 0x8f, (byte) 0x68, (byte) 0xdb, (byte) 0x01, (byte) 0x2a,
            (byte) 0x83, (byte) 0x3e, (byte) 0xc2, (byte) 0xe0, (byte) 0x84,
            (byte) 0x64, (byte) 0xa7, (byte) 0x95, (byte) 0xcf, (byte) 0x1a,
            (byte) 0x6a, (byte) 0xb4, (byte) 0xce, (byte) 0x25, (byte) 0x95,
            (byte) 0x04, (byte) 0xef, (byte) 0x6b, (byte) 0xd3, (byte) 0x23,
            (byte) 0xb4, (byte) 0xb9, (byte) 0x2f, (byte) 0x51, (byte) 0x82,
            (byte) 0xbf, (byte) 0x69, (byte) 0x4c, (byte) 0xd4, (byte) 0xc2,
            (byte) 0xd9, (byte) 0xbe, (byte) 0x68, (byte) 0x25, (byte) 0x3e,
            (byte) 0xd1, (byte) 0x67, (byte) 0x66, (byte) 0x31, (byte) 0x03};

    //Public key from this terminal
    private final byte[] PUBLIC_KEY_TERMINAL = {
            (byte) 0x04, (byte) 0x7b, (byte) 0x77, (byte) 0x2f, (byte) 0x99,
            (byte) 0xf7, (byte) 0x64, (byte) 0x20, (byte) 0x72, (byte) 0xa4,
            (byte) 0x33, (byte) 0xf1, (byte) 0x9a, (byte) 0xb2, (byte) 0xd0,
            (byte) 0x78, (byte) 0xb8, (byte) 0xb3, (byte) 0xd4, (byte) 0x09,
            (byte) 0x3b, (byte) 0x1d, (byte) 0x12, (byte) 0xac, (byte) 0x13,
            (byte) 0x5f, (byte) 0xeb, (byte) 0x7c, (byte) 0xfe, (byte) 0x32,
            (byte) 0xe9, (byte) 0x7f, (byte) 0x2c, (byte) 0xbe, (byte) 0x2e,
            (byte) 0xa8, (byte) 0x96, (byte) 0xb4, (byte) 0x6a, (byte) 0x29,
            (byte) 0x40, (byte) 0xf1, (byte) 0x61, (byte) 0xd1, (byte) 0x34,
            (byte) 0x13, (byte) 0xa3, (byte) 0xd7, (byte) 0x12, (byte) 0x9c,
            (byte) 0x79, (byte) 0xb6, (byte) 0x38, (byte) 0x3c, (byte) 0xb9,
            (byte) 0xf7, (byte) 0x59, (byte) 0xaf, (byte) 0xf1, (byte) 0x14,
            (byte) 0x90, (byte) 0xe4, (byte) 0xb7, (byte) 0x37, (byte) 0x4f};

    //Private key from the certificate issuer
    private final String HEX_STRING_PRIVATE_KEY_ISSUER= "78a7b3082f96c659088cd79a64e956f147984ab901a94ab386e6725f4e81cb50";

    //Confirmer's master secret
    private final byte[] Kconf = {0x2d, 0x2a, 0x2d, 0x42, 0x55, 0x49, 0x4c, 0x44, 0x41, 0x43, 0x4f, 0x44, 0x45, 0x2d, 0x2a, 0x2d};

    //Instructions to act on the card's EEPROM
    private final byte INS_REQ_STORAGE = (byte) 0xD6;
    private final byte INS_SENDTO_EEPROM = (byte) 0xD7;
    private final byte INS_DELETE_FROM_EEPROM = (byte) 0xD8;

    public LBT() throws CardException {}

    @Override
    public String[] protocol(String... args) throws Exception {
        Enrollment.print("");
        Enrollment.print("Enrollment started !\n");

        byte[] uChip = new byte[4];
        new Random().nextBytes(uChip);
        byte[] input = Utils.concatByteArrays(Kconf, uChip);

        BigInteger pwd = new BigInteger(1, args[3].getBytes());
        ECPoint G2 = ECTools.createPoint(Secp256r1.G2);
        ECPoint G3 = ECTools.createPoint(Secp256r1.G3);
        ECPoint pwdG2 = ECTools.scalmult(G2, pwd);
        ECPoint pwdG3 = ECTools.scalmult(G3, pwd);
        byte[] rawpwdG2 = ECTools.getWAsByte(pwdG2);
        byte[] rawpwdG3 = ECTools.getWAsByte(pwdG3);


        byte[] byL = Base64.decode(args[4], Base64.DEFAULT);
        byte[] byR = Base64.decode(args[5], Base64.DEFAULT);
        //TODO: delete this variable as it is not used (remaining code from when we had three images from the scanner and not two)
        byte[] byC = new byte[byR.length];

        byte[] sizes = {(byte) (byL.length & 0xFF), (byte) ((byL.length >>> 8) & 0xFF), (byte) (byC.length & 0xFF), (byte) ((byC.length >>> 8) & 0xFF)};

        String padding = "";
        for(int i = 0; i < 15 - args[1].length(); ++i)
            padding += (char) 0;

        args[1] += padding;

        padding = "";
        for(int i = 0; i < 15 - args[0].length(); ++i)
            padding += (char) 0;

        args[0] += padding;

        padding = "";
        for(int i = 0; i < 8 - args[3].length(); ++i)
            padding += (char) 0;

        args[3] += padding;

        //Was a non-initialized attribute
        byte[] biodataBytes = Utils.concatByteArrays(sizes, byL, byC, byR);
        //Was new byte[32] attribute
        byte[] Kchip = Utils.KDF(input, 9);
        //Was new byte[51] attribute : u_chip || password || name || lastName || dateOfBirth = 4 + 8 + 15 + 15 + 9
        byte[] dg2 = Utils.concatByteArrays(uChip, args[3].getBytes(), args[1].getBytes(), args[0].getBytes(), args[2].getBytes());
        //Was new byte[101] attribute
        byte[] sigma = schnorrSign(Utils.concatByteArrays(dg2, biodataBytes)); // signature to be loaded to the card
        //Was new byte[130] attribute
        byte[] points = Utils.concatByteArrays(rawpwdG2, rawpwdG3);

        Enrollment.print("Wiping previous data...");
        sendData(INS_DELETE_FROM_EEPROM, ByteBuffer.allocate(2).putShort(INDEX_Bio).array());
        sendData(INS_DELETE_FROM_EEPROM, ByteBuffer.allocate(2).putShort(INDEX_SIG).array());
        sendData(INS_DELETE_FROM_EEPROM, ByteBuffer.allocate(2).putShort(INDEX_PKterm).array());
        sendData(INS_DELETE_FROM_EEPROM, ByteBuffer.allocate(2).putShort(INDEX_PKissuer).array());
        sendData(INS_DELETE_FROM_EEPROM, ByteBuffer.allocate(2).putShort(INDEX_DG2).array());
        sendData(INS_DELETE_FROM_EEPROM, ByteBuffer.allocate(2).putShort(INDEX_Kchip).array());
        sendData(INS_DELETE_FROM_EEPROM, ByteBuffer.allocate(2).putShort(INDEX_PWDG).array());
        Enrollment.print("✓\n");

        Enrollment.print("Storing fingervein data...");
        putBioDataInCard(INDEX_Bio, biodataBytes);
        Enrollment.print("✓\n");

        Enrollment.print("Storing Schnorr signature...");
        putBioDataInCard(INDEX_SIG, sigma);
        Enrollment.print("✓\n");

        Enrollment.print("Storing public keys...");
        putBioDataInCard(INDEX_PKterm, PUBLIC_KEY_TERMINAL);
        putBioDataInCard(INDEX_PKissuer, PUBLIC_KEY_ISSUER);
        Enrollment.print("✓\n");

        Enrollment.print("Storing identity data...");
        putBioDataInCard(INDEX_DG2, dg2);
        Enrollment.print("✓\n");

        putBioDataInCard(INDEX_Kchip, Kchip);
        putBioDataInCard(INDEX_PWDG, points);

        Enrollment.print("Card Ready ✓\n");
        return null;
    }

    /**
     * Takes a message, identity issuer private key, which is hardcoded for the demo, and curve parameters and signs the message into byte array
     * @param message : message
     * @return signature
     * @throws NoSuchAlgorithmException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchProviderException
     */
    private byte[] schnorrSign(byte[] message) throws NoSuchAlgorithmException, InvalidAlgorithmParameterException, NoSuchProviderException
    {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("EC");
        generator.initialize(new ECGenParameterSpec("secp256r1"));
        KeyPair secp256EphKeyPair = generator.generateKeyPair();

        ECPrivateKey ephSK = (ECPrivateKey) secp256EphKeyPair.getPrivate();
        ECPublicKey ephPK = (ECPublicKey) secp256EphKeyPair.getPublic();

        // compute s <- (k - idIssuerSK*H(4|| m || R)) mod p, where m is name, last name, age
        // Preparing the input of hash: 4|| message || R and saving it in keyDigest byte array

        byte[] kdfData = Utils.concatByteArrays(message, ECTools.getWAsByte(ephPK));
        byte[] keyDigest = Utils.KDF( kdfData, 4);
        // computing \sigma <-(s, R). s= k - x*h mod p and R=ephPK.getEncoded()

        BigInteger k = ephSK.getS();
        BigInteger x = new BigInteger(HEX_STRING_PRIVATE_KEY_ISSUER ,16);
        BigInteger h = new BigInteger(1, keyDigest);
        BigInteger s = k.subtract(x.multiply(h)).mod(ephPK.getParams().getOrder());
        byte[] ba_s = s.toByteArray();
        ba_s = Utils.shrink(ba_s);
        /**
         * The signatures is in the following form: sizeS || s || sizeR || R.
         * first byte is the length of s
         * sizeS^{th} byte is the length of R
         * Start reading R from (sizeR+1)th byte
         * It makes it easy to recover R later from stored signatures on the card.
         */

        byte[] sizeS = ByteBuffer.allocate(2).putShort((short) ba_s.length).array();
        byte[] sizeR = ByteBuffer.allocate(2).putShort((short) ECTools.getWAsByte(ephPK).length).array();
        return Utils.concatByteArrays(sizeR, ECTools.getWAsByte(ephPK), sizeS, ba_s ); // concat( concat(sizeS, s.toByteArray()), concat(sizeR, ephPK.getEncoded()));
    }

    /**
     * Uploads specific data to the card's EEPROM
     * @param dataID The ID of the data to be uploaded
     * @param data The data to be uploaded
     * @throws CardException if the card's reponse contains an error
     */
    private void putBioDataInCard(short dataID, byte[] data) throws Exception
    {
        byte[] storage = ByteBuffer.allocate(2).putShort((short) data.length).array();
        byte[] baDataID = ByteBuffer.allocate(2).putShort(dataID).array();
        byte[] requestStorage = Utils.concatByteArrays(storage, baDataID);
        sendData(INS_REQ_STORAGE, requestStorage);
        for (int i = 0; i < data.length; i += MAX_BUFFER_BYTES)
        {
            short bytesToSend = (data.length - i) >= MAX_BUFFER_BYTES ? MAX_BUFFER_BYTES : (short)(data.length - i);
            byte[] buffer = new byte[bytesToSend];
            System.arraycopy(data, i, buffer, 0, bytesToSend);

            byte[] baOffset = ByteBuffer.allocate(2).putShort((short) i).array();
            byte[] baSize = ByteBuffer.allocate(2).putShort(bytesToSend).array();
            byte[] returnData = Utils.concatByteArrays(baDataID, baOffset, baSize, buffer);
            sendData(INS_SENDTO_EEPROM, returnData);
        }
    }
}