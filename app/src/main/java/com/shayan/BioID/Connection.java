package com.shayan.BioID;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.view.View.OnClickListener;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.chaquo.python.Python;
import com.chaquo.python.android.AndroidPlatform;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;

import java.util.Enumeration;
import java.util.List;


public class Connection extends Activity {

    //used to display messages live
    private static final Handler mainHandler = new Handler(Looper.getMainLooper());

    //different urls of scanner for requests
    final String url_switch = "http://192.168.100.1:8000/api/switch_wifi";
    final String url_set = "http://192.168.100.1:8000/api/set_wifi";
    final String url_beep = "http://192.168.100.1:8000/api/beep?duration=0.1";

    // elements of layout
    private Button mConnect;
    private Button mSetWifi;
    private EditText ssid;
    private EditText pass;
    private RequestQueue mRequestQueue;
    static TextView txtLog;

    //ip of scanner
    public void setIpScanner(String ipScanner) {
        Connection.ipScanner = ipScanner;
    }
    public static String getIpScanner() {
        return ipScanner;
    }
    public static String ipScanner;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connection);
        getActionBar().hide();

        txtLog = findViewById(R.id.textLog);
        ssid = findViewById(R.id.edit_ssid);
        pass = findViewById(R.id.edit_pass);

        mSetWifi = findViewById(R.id.button_set);
        mSetWifi.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mRequestQueue = Volley.newRequestQueue(Connection.this);
                JSONObject wifiInfo = new JSONObject(); //contains wifi info we send to the scanner
                try {
                    wifiInfo.put("ssid", ssid.getText().toString());
                    wifiInfo.put("psk", pass.getText().toString());
                    wifiInfo.put("anonymized", false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //set wifi info on scanner
                CustomJsonObjectRequest objReq1 = new CustomJsonObjectRequest(Request.Method.POST, url_set, wifiInfo, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONObject json2 = new JSONObject();
                        try {
                            json2.put("target_mode", "Client");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //set wifi on client mode
                        CustomJsonObjectRequest objReq2 = new CustomJsonObjectRequest(Request.Method.POST, url_switch, json2, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                //makes the scanner beep
                                CustomJsonObjectRequest objReq3 = new CustomJsonObjectRequest(Request.Method.GET, url_beep, null, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        print("WiFi is correctly set.");
                                        print("You can now restart the scanner.");
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                    }
                                });
                                mRequestQueue.add(objReq3);

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                            }
                        });
                        mRequestQueue.add(objReq2);

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
                mRequestQueue.add(objReq1);
            }
        });

        mConnect = findViewById(R.id.button_connect);
        mConnect.setOnClickListener(new OnClickListener() { //connect to the scanner in client mode
            @Override
            public void onClick(View v) {
                requests(getNetworkIPs(), 0);
            }
        });
    }

    //recursive method to find the ip of the scanner in the list of all ips connected to the same network as us
    public void requests(String[] ips, int index){
        mRequestQueue = Volley.newRequestQueue(this);
        String preurl = "http://";
        String beep = ":8000/api/beep?duration=0.1";
        String url = preurl + ips[index] + beep;
        CustomJsonObjectRequest jsonObjectRequest = new CustomJsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                setIpScanner(ips[index]);
                print("The scanner is well connected.");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                print("argh");
//                print(error.toString());
                if (index < ips.length - 1) {
                    requests(ips, index + 1);
                } else if (index == ips.length - 1) {
                    print("Scanner not found.");
                }

            }
        });

        mRequestQueue.add(jsonObjectRequest);
    }

    //get the list of all ips connected to the same network as us
    public static String[] getNetworkIPs() {
        List<String> ok = new ArrayList<>();
        String ip2;
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface iface = interfaces.nextElement();
                if (iface.isLoopback() || !iface.isUp()){
                    continue;
                }
                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while(addresses.hasMoreElements()){
                    InetAddress addr = addresses.nextElement();
                    ip2 = addr.getHostAddress();
                    if (ip2 != null && !ip2.contains(":")) {
//                        System.out.println(iface.getDisplayName() + " " + ip2);
                        String[] splittedArray = ip2.split("\\.");
                        for (int i = 0; i <= 255; i++) {
                            final int j = i;  // i as non-final variable cannot be referenced from inner class
                            new Thread(new Runnable() {   // new thread for parallel execution
                                public void run() {
                                    try {
                                        splittedArray[3] = String.valueOf(j);
                                        String newip = splittedArray[0] + "." + splittedArray[1] + "." + splittedArray[2] + "." + splittedArray[3];
                                        InetAddress address = InetAddress.getByName(newip);
                                        String output = address.toString().substring(1);
                                        if (address.isReachable(5000)) {
                                            ok.add(output);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();     // dont forget to start the thread
                        }
                    }
                }
            }
            return ok.toArray(new String[0]);
        } catch (Exception e) {
            return ok.toArray(new String[0]);     // exit method, otherwise "ip might not have been initialized"
        }

    }

    //custom version of JsonObjectRequest such that getting no/null responses is handled
    public static class CustomJsonObjectRequest extends JsonObjectRequest {

        public CustomJsonObjectRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
            super(method, url, jsonRequest, listener, errorListener);
        }

        @Override
        protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
            try {
                String jsonString = new String(response.data,
                        HttpHeaderParser.parseCharset(response.headers));

                JSONObject result = null;

                if (jsonString.length() > 0)
                    result = new JSONObject(jsonString);

                return Response.success(result,
                        HttpHeaderParser.parseCacheHeaders(response));
            } catch (UnsupportedEncodingException | JSONException e) {
                return Response.error(new ParseError(e));
            }
        }
    }

    //print messages on app
    public static void print(String s)
    {
        mainHandler.post(new Runnable() {
        @Override
        public void run() {
            txtLog.append(s);
            txtLog.append("\r\n");
        }
    });
    }
}
