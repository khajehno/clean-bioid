package com.shayan.BioID;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Main extends Activity{

    //layout elements
    private Button mEnrollment;
    private Button mScanner;
    private Button mMatching;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        getActionBar().hide();

        mEnrollment = findViewById(R.id.enroll);
        mEnrollment.setOnClickListener(new View.OnClickListener() { // go to the "Enrollment" part
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Main.this, Enrollment.class));
            }
        });

        mScanner = findViewById(R.id.scan);
        mScanner.setOnClickListener(new View.OnClickListener() { // go to the settings
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Main.this, Connection.class));
            }
        });

        mMatching = findViewById(R.id.match);
        mMatching.setOnClickListener(new View.OnClickListener() { // go to the "MatchingClient" Part
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Main.this, MatchingClient.class));
            }
        });
    }
}
