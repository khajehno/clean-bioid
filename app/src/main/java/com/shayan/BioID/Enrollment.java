package com.shayan.BioID;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.DeflaterOutputStream;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.os.Parcelable;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.chaquo.python.PyObject;
import com.chaquo.python.Python;
import com.chaquo.python.android.AndroidPlatform;

import org.json.JSONArray;
import org.json.JSONObject;

import processes.LBT;
import libraries.CommandAPDU;
import libraries.ResponseAPDU;

public class Enrollment extends Activity {

    //values needed for the APDUs sent to the card
    protected static final byte INS_SELECT = (byte) 0xA4;
    protected static final byte CLA = 0x00;
    protected static final byte[] AID = {
            (byte) 0xA0, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x52,
            (byte) 0x03, (byte) 0x01, (byte) 0x0C, (byte) 0x05, (byte) 0x03};

    //used to build url request for the scanner
    final String preurl = "http://";
    final String get_image = ":8000/api/debug/get_image_plain";

    //used to send requests to scanner
    private RequestQueue mRequestQueue;

    //list containing images sent by the scanner, encoded in base 64
    private List<String> listString = new ArrayList<>();
    //list containing the extracted and compressed images, encoded in base 64
    private List<String> listIm64 = new ArrayList<>();

    //used to display messages live
    private static final Handler mainHandler = new Handler(Looper.getMainLooper());


    //used for NFC communications
    private NfcAdapter mAdapter = null;
    private PendingIntent mPendingIntent;
    private String[][] mTechLists;
    private IntentFilter[] mFilters;
    static IsoDep myTag;
    boolean mFirstDetected = false;
    static byte[] byteAPDU = null;
    static byte[] respAPDU = null;


    //elements of layout
    private Button mClearLogButton;
    private Button mSetNFCButton;
    private Button mSendToCard;
    private EditText mName;
    private EditText mLastName;
    private EditText mBirthday;
    private EditText mPassword;
    private Button mPicture;
    static ImageView icoNfc;
    static ImageView icoCard;
    static TextView TextNfc;
    static TextView TextCard;
    static TextView txtLog;
    static ScrollView scroll;
    static ImageView imgView1;
    static ImageView imgView2;





    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enrollment);
        getActionBar().hide();
        if (!Python.isStarted()) { //initiate Python
            Python.start(new AndroidPlatform(this));
        }

        byteAPDU = null;
        respAPDU = null;

        //linking the elements of the layout to the code
        imgView1 = findViewById(R.id.imgfinger1);
        imgView2 = findViewById(R.id.imgfinger2);
        scroll = findViewById(R.id.scroll);
        txtLog = findViewById(R.id.textLog);
        icoNfc = findViewById(R.id.imageNfc);
        icoNfc.setImageResource(R.drawable.ic_nfc_off);
        icoCard = findViewById(R.id.imageCard);
        icoCard.setImageResource(R.drawable.ic_icc_off);
        TextNfc = findViewById(R.id.textNfc);
        TextCard = findViewById(R.id.textCard);
        mName = findViewById(R.id.edit_name);
        mLastName = findViewById(R.id.edit_lastname);
        mBirthday = findViewById(R.id.edit_anni);
        mPassword = findViewById(R.id.edit_pass);

        mPicture = findViewById(R.id.button_co);
        mPicture.setOnClickListener(new OnClickListener() { //actions of button "Take Picture"
            @Override
            public void onClick(View v) {
                clearlog();
                listString.clear();
                takePictureRunnable runnable = new takePictureRunnable();
                new Thread(runnable).start();
            }
        });

        mSendToCard = findViewById(R.id.button_sendName);
        mSendToCard.setOnClickListener(new OnClickListener() { //actions of button "Send to Card"
            @Override
            public void onClick(View view) {
                if (mFirstDetected && myTag.isConnected()) {
                    icoCard.setImageResource(R.drawable.ic_icc_on);
                    clearlog();
                    loadDataRunnable runnable = new loadDataRunnable();
                    new Thread(runnable).start();

                } else {
                    icoCard.setImageResource(R.drawable.ic_icc_off);
                    clearlog();
                    TextCard.setText("PLEASE TAP CARD");
                }
            }
        });


        mClearLogButton = findViewById(R.id.button_ClearLog);
        mClearLogButton.setOnClickListener(new OnClickListener() { //actions of button "Clear Log"
            @Override
            public void onClick(View v) {
                if (mFirstDetected && myTag.isConnected()) {
                    icoCard.setImageResource(R.drawable.ic_icc_on);
                } else {
                    icoCard.setImageResource(R.drawable.ic_icc_off);
                    TextCard.setText("PLEASE TAP CARD");
                }
                clearlog();
            }
        });

        mSetNFCButton = findViewById(R.id.button_SetNFC);
        mSetNFCButton.setOnClickListener(new OnClickListener() { //actions of button "Set NFC"
            @Override
            public void onClick(View v) {
                if (mFirstDetected && myTag.isConnected()) {
                    icoCard.setImageResource(R.drawable.ic_icc_on);
                } else {
                    icoCard.setImageResource(R.drawable.ic_icc_off);
                    clearlog();
                    TextCard.setText("PLEASE TAP CARD");
                }
                clearlog();
                startActivityForResult(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS), 0);
            }
        });


        mClearLogButton.setEnabled(true);
        mSetNFCButton.setEnabled(true);

        resolveIntent(getIntent());

        mAdapter = NfcAdapter.getDefaultAdapter(this);
        mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        try {
            ndef.addDataType("*/*");
        } catch (MalformedMimeTypeException e) {
            throw new RuntimeException("fail", e);
        }
        mFilters = new IntentFilter[]{ndef,};
        mTechLists = new String[][]{new String[]{IsoDep.class.getName()}};
    }


    // code from "APDU Sender Contactless", taking care of the NFC connection
    @Override
    public void onResume() {
        super.onResume();

        byteAPDU = null;
        respAPDU = null;

        if (mFirstDetected && (myTag.isConnected())) {
            icoCard.setImageResource(R.drawable.ic_icc_on);
        } else {
            icoCard.setImageResource(R.drawable.ic_icc_off);
        }
        if ((mAdapter == null) || (!mAdapter.isEnabled())) {
            if (mAdapter == null) {
                clearlog();
                TextCard.setText("PLEASE TAP CARD");
                mSetNFCButton.setEnabled(false);
                print("    No NFC hardware found.");
                print("    Program will NOT function.");
            } else if (mAdapter.isEnabled()) {
                clearlog();
                TextNfc.setText("NFC ENABLED");
            } else {
                clearlog();
                TextCard.setText("PLEASE TAP CARD");
                mSetNFCButton.setEnabled(true);
                icoNfc.setImageResource(R.drawable.ic_nfc_off);
                TextNfc.setText("NFC DISABLED");
            }
        }
        if (mAdapter != null) {
            if (mAdapter.isEnabled()) {
                clearlog();
                TextNfc.setText("NFC ENABLED");
                icoNfc.setImageResource(R.drawable.ic_nfc_on);
            }
            mAdapter.enableForegroundDispatch(this, mPendingIntent, mFilters, mTechLists);
        } else {
            clearlog();
            icoNfc.setImageResource(R.drawable.ic_nfc_off);
            TextNfc.setText("NO READER DETECTED");
            TextCard.setText("PLEASE TAP CARD");
            mSetNFCButton.setEnabled(false);
            print("    No NFC hardware found.");
            print("    Program will NOT function.");
        }
    }

    //code from "APDU Sender Contactless"
    @Override
    public void onPause() {
        super.onPause();

        byteAPDU = null;
        respAPDU = null;

        if (mFirstDetected && (myTag.isConnected())) {
            icoCard.setImageResource(R.drawable.ic_icc_on);
        } else {
            icoCard.setImageResource(R.drawable.ic_icc_off);
        }
        mAdapter.disableForegroundDispatch(this);
    }

    //code from "APDU Sender Contactless"
    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        resolveIntent(intent);
    }

    //function clearing the messages displayed
    private static void clearlog() {
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                txtLog.setText("");
                imgView1.setImageResource(android.R.color.transparent);
                imgView2.setImageResource(android.R.color.transparent);
            }
        });
    }

    //function displaying messages on the app
    public static void print(String s) {
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                txtLog.append(s);
                txtLog.append("\r\n");
                scroll.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }

    //function sending an APDU, returns a "ResponseAPDU"
    public static ResponseAPDU transceives(CommandAPDU apdu) throws Exception {
        return new ResponseAPDU(transceives(apdu.getBytes()));
    }

    //function sending an APDU, returns a byte array
    private static byte[] transceives(byte[] data) {
        byte[] ra = null;

//        try
//        {
//            print("***COMMAND APDU***");
//            print("");
//            print("IFD - " + getHexString(data));
//        }
//        catch (Exception e1)
//        {
//            e1.printStackTrace();
//        }

        try {
            ra = myTag.transceive(data);
        } catch (IOException e) {

            print("************************************");
            print("         NO CARD RESPONSE");
            print("************************************");

        }
//        try {
//            print("");
//            print("ICC - " + getHexString(ra));
//        } catch (Exception e1) {
//            e1.printStackTrace();
//        }

        return (ra);
    }


    // code from "APDU Sender Contactless", taking care of the NFC connection
    private void resolveIntent(Intent intent) {
        String action = intent.getAction();
        clearlog();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action) || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action) || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            final Tag t = (Tag) tag;
            myTag = IsoDep.get(t);
            mFirstDetected = true;
            if (!myTag.isConnected()) {
                try {
                    myTag.connect();
                    myTag.setTimeout(5000);
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
            }
            if (myTag.isConnected()) {
                icoCard.setImageResource(R.drawable.ic_icc_on);
                vShowCardRemovalInfo();
                TextCard.setText("CARD DETECTED  ");

                clearlog();
            } else {
                icoCard.setImageResource(R.drawable.ic_icc_off);
            }
        }
        if (mFirstDetected && myTag.isConnected()) {
            icoCard.setImageResource(R.drawable.ic_icc_on);
        } else {
            icoCard.setImageResource(R.drawable.ic_icc_off);
        }
    }

    //code from "APDU Sender Contactless", says if card is removed
    private void vShowCardRemovalInfo() {
        Context context = getApplicationContext();
        CharSequence text = "Card Removal will NOT be detected";
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    //function that compresses the byte array reprentation of an image, so it fits to the card memory
    public static byte[] compress(byte[] in) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            DeflaterOutputStream defl = new DeflaterOutputStream(out);
            defl.write(in);
            defl.flush();
            defl.close();

            return out.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(150);
            return null;
        }
    }

    //runnable to send the informations on the card
    class loadDataRunnable implements Runnable {
        @Override
        public void run() {
            try {
                transceives(new CommandAPDU(CLA, INS_SELECT, 0x04, 0x00, AID));
                LBT lbt = new LBT();
                lbt.protocol(mLastName.getText().toString(), mName.getText().toString(), mBirthday.getText().toString(), mPassword.getText().toString(), listIm64.get(0), listIm64.get(1));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // send the request to the scanner to get the images
    public void receiveImage(){
        mRequestQueue = Volley.newRequestQueue(this);
        String url = preurl + Connection.getIpScanner() + get_image;
        JsonObjectRequest objReq = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("c");
                    JSONObject datacam1 = jsonArray.getJSONObject(0);
                    JSONObject datacam2 = jsonArray.getJSONObject(1);
                    String img1_64 = datacam1.getString("image");
                    String img2_64 = datacam2.getString("image");
                    listString.add(img1_64);
                    listString.add(img2_64);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                print("You can remove your finger.");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                print("Error connecting to scanner, make sure you have connected through the settings first.");
                print(error.toString());
            }
        });
        mRequestQueue.add(objReq);
    }

    //decodes the images received by the scanner and extract the veins
    class takePictureRunnable implements Runnable {
        private final int leftFinger = 0;
        private final int rightFinger = 1;
        @Override
        public void run() {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    receiveImage();
                }
            });
            t.start();
            while(listString.isEmpty()){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            byte[] decodedString1 = Base64.decode(listString.get(0), Base64.DEFAULT);
            byte[] decodedString2 = Base64.decode(listString.get(1), Base64.DEFAULT);
            Python py = Python.getInstance();
            PyObject pyobj = py.getModule("main");
            PyObject obj = pyobj.callAttr("extract", decodedString1, leftFinger);
            PyObject obj2 = pyobj.callAttr("extract", decodedString2, rightFinger);
            byte[] extracted1 = obj.toJava(byte[].class);
            byte[] extracted2 = obj2.toJava(byte[].class);
            String img1 = Base64.encodeToString(compress(extracted1), Base64.DEFAULT);
            String img2 = Base64.encodeToString(compress(extracted2), Base64.DEFAULT);
            listIm64.clear();
            listIm64.add(img1);
            listIm64.add(img2);
            mainHandler.post(new Runnable() {
                @Override
                public void run() {
                    imgView1.setImageBitmap(byteToBitmap(extracted1));
                    imgView2.setImageBitmap(byteToBitmap(extracted2));
                    txtLog.setText("");
                }
            });
            print("The images are ready.");
        }
    }

    //function that takes in parameter an image represented by a byte array with one value per pixel (grayscale), and returns an Bitmap representation of the image
    public Bitmap byteToBitmap(byte[] image) {
        int imgHeight = 240;
        int imgWidth = 376;
        int[] pixels = new int[imgWidth*imgHeight];
        Bitmap bitmap = Bitmap.createBitmap(imgWidth, imgHeight, Bitmap.Config.ARGB_8888);
        for (int i = 0; i < imgWidth*imgHeight; i++) {
            int byteval = image[i];
            if (byteval < 0) {
                byteval = ~((int) image[i]) + 1;
            }
            pixels[i] = (0xFF << 24) | (byteval << 16) | (byteval << 8) | byteval;
        }
        bitmap.setPixels(pixels, 0, imgWidth,0,0,imgWidth,imgHeight);
        return bitmap;
    }
}

