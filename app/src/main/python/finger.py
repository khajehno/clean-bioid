import numpy as np
import scipy.ndimage as si

from skimage.filters import threshold_otsu
from skimage.morphology import convex_hull_image


#nxn matrix of -1's with an n² - 1 at its center
NMS_FILTER = lambda n : np.array([[-1] * n] * (n//2)
                                +[[-1] * (n//2) + [n**2 - 1] + [-1] * (n//2)]
                                +[[-1] * n] * (n//2))

def fingerfocus(img, roi, sigma = 1, hystd = (0,.1), min_area = 150, nms_order = 17):

    """ Background elimination method

    @return img (numpy.ndarray) : The resulting image
    """

    img = img.copy()

    # debug = log.getEffectiveLevel() <= logging.DEBUG

    xmin, xmax, ymin, ymax = roi

    # if debug: show_uint16(img, "Original Image")

    cropped = img[xmin : xmax, ymin : ymax]

    # if debug: show_uint16(cropped, "Cropped")

    smoothed = si.gaussian_filter(cropped, sigma)

    # if debug: show_uint16(smoothed, "Smoothed")

    # Gradient

    gx, gy = np.gradient(smoothed)

    gradient = np.hypot(gx, gy)
    gradient = (gradient / gradient.max() * 65535).astype(np.uint16)

    otsu = threshold_otsu(cropped, 1 << 16)
    μ_grad, σ_grad = gradient.mean(), gradient.std()

    # if debug: show_uint16(gradient, "Gradient")

    #Non-maxima suppression

    nonmax = si.convolve(gradient, NMS_FILTER(nms_order), output = np.int64, mode = "reflect") <= 0
    gradient *= (~nonmax)

    # if debug: show_bool(gradient, "Local Maxima")

    #Hysteresis

    if hystd is not None:

        hys_low = μ_grad - hystd[0] * σ_grad
        hys_high = μ_grad + hystd[1] * σ_grad
        # log.info(f"Manual hysteresis thresholds : [{hys_low:.0f}, {hys_high:.0f}]")

    else:

        hys_low = .5 * otsu
        hys_high = otsu
        # log.info(f"Otsu hysteresis thresholds : [{hys_low:.0f}, {hys_high:.0f}]")

    rubbish = gradient <= hys_low
    strong = gradient >= hys_high
    weak = (~rubbish) * (~strong)

    # if debug: show_bool(strong, "Strong")
    # if debug: show_bool(weak, "Weak")

    before = weak.sum()

    weak *= si.binary_dilation(strong, structure = np.array([[1, 1, 1],
                                                             [1, 0, 1],
                                                             [1, 1, 1]]))

    # log.info(f"{before - weak.sum()}/{before} weak edge pixels filtered")

    gradient *= (~rubbish) * (strong + weak)

    # if debug: show_bool(gradient, "Hysteresis")

    #Orientation Discontinuity suppression

    orientations = np.arctan2(gy, gx)
    laplace = si.laplace(smoothed, output = np.int64, mode = "reflect")
    laplace = (laplace / laplace.max() * 65535).astype(np.uint16)

    # if debug: show_bool(orientations, "Orientations")
    # if debug: show_uint16(laplace, "Laplacian")

    ogx, ogy = np.gradient(orientations)

    ogradient = np.hypot(ogx, ogy)
    ogradient = (ogradient / np.abs(ogradient).max() * np.pi)

    # if debug: show_bool(ogradient / ogradient.max() * 2, "Orientations gradient")

    ofilters = ((ogradient < ogradient.mean())
             * (gradient > gradient.mean()))

    o2 = orientations.copy()
    o2 *= ofilters

    # if debug: show_bool(o2, "Orientations filtered")

    gradient *= ofilters

    # if debug: show_bool(gradient, "Orientations corrected gradient")


    #Connected components

    blobs, labnbr = si.label(gradient, structure = np.array([[0, 1, 0],
                                                             [1, 1, 1],
                                                             [0, 1, 0]]))

    pixels = blobs.ravel()
    areas = np.bincount(pixels)[1:]
    kept_labels = np.argwhere(areas > min_area) + 1
    mask = np.isin(blobs, kept_labels)

    # if debug:

    #     colors = np.zeros((*blobs.shape, 3), np.uint8)
    #     for i in range(1, labnbr):
    #         colors[blobs == i] = np.random.choice(range(256), size = 3)
    #     cv.imshow("Connected components", colors)

    # if debug: show_bool(mask, f"Connected components > {min_area}")

    #Filling

    h, w = mask.shape

    mask[:h//2, :w//2] = si.binary_dilation(mask[:h//2, :w//2],
                                            structure = np.array([[0, 0, 0],
                                                                  [0, 1, 1],
                                                                  [0, 1, 0]]), iterations = -1)
    mask[:h//2, w//2:] = si.binary_dilation(mask[:h//2, w//2:],
                                            structure = np.array([[0, 0, 0],
                                                                  [1, 1, 0],
                                                                  [0, 1, 0]]), iterations = -1)
    mask[h//2:, :w//2] = si.binary_dilation(mask[h//2:, :w//2],
                                            structure = np.array([[0, 1, 0],
                                                                  [0, 1, 1],
                                                                  [0, 0, 0]]), iterations = -1)
    mask[h//2:, w//2:] = si.binary_dilation(mask[h//2:, w//2:],
                                            structure = np.array([[0, 1, 0],
                                                                  [1, 1, 0],
                                                                  [0, 0, 0]]), iterations = -1)

    # if debug: show_bool(mask, f"Quadrant-bloated mask")

    mask = convex_hull_image(mask)

    # if debug: show_bool(mask, "Final mask")

    img[:xmin] = img[xmax:] = 0
    img[:, :ymin] = img[:, ymax:] = 0
    img[xmin : xmax, ymin : ymax] *= mask

    ratio = 1 - mask.sum() / img.size

    # if ratio > .6:
    #     log.warning(f"{ratio * 100 :.1f}% of the image was removed !")
    # else:
    #     log.info(f"DONE : {ratio * 100 :.1f}% of the image was removed")

    # if debug: show_uint16(img, "End result")

    # if debug: show_bool(extract_features(img), "Extracted")

    # if debug:
    #     cv.waitKey()
    #     cv.destroyAllWindows()

    return img