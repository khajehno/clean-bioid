import cv2 as cv
import logging
import numpy as np
import sage
import scipy.linalg as la
import scipy.ndimage as si
import scipy.signal as sp
import sys

from numpy.polynomial import Polynomial
from sage.all import GF, QQ, ZZ, matrix, vector, Integers, PolynomialRing
from sage.coding.grs_code import ReedSolomonCode, DecodingError

from biometrics import extract_features, fingerfocus
from crypto import key_gen, DEFAULT_KEY_BIT_SIZE, sha256, rand_bytes
from utils import show_bool, show_uint16, bytes_to_uint, bytes_to_int, digits, int_to_bytelist, int_to_bytes, uint_to_bytes, tile
from devices.scanners.ScannerMkII import ScannerMkII

""" Adapted from :
A crypto-biometric scheme based on 
iris-templates with fuzzy extractors

    By : Álvarez, Álvarez, Encinas
"""

log = logging.getLogger()
log.setLevel(logging.INFO)

logstream = logging.StreamHandler(sys.stdout)
#logfile = logging.FileHandler(f"fuzzy.txt", "w+")

log.addHandler(logstream)
#log.addHandler(logfile)

DEFAULT_KEY_BIT_SIZE = 128
#t = 4, 11, crop first and last column 
def gen(W, b = 20, t = (4, 9)):

    n = t[0] * t[1]

    rng = np.random.default_rng()

    x_low = -(1 << 8)
    #x_low = np.iinfo(int).min # - 2**63
    x_high = (1 << 8)
    #x_high = np.iinfo(int).max # 2**63 - 1
    max_S = (1 << DEFAULT_KEY_BIT_SIZE) - 1
    max_p = Polynomial(digits(max_S, b)[::-1])
    max_x = int_to_bytes(max(x_high, -x_low))
    max_y = int_to_bytes(int(max_p(max(x_high, -x_low))))

    lx = len(max_x)
    ly = len(max_y)

    print(f"lx, ly = {lx,ly}")

    S = rand_bytes(DEFAULT_KEY_BIT_SIZE // 8) #16 bytes
    S = bytes_to_uint(S) #max 'ff' x 16 = 2^128 - 1
    print(f"Secret S = {S}")
    d = digits(S, b)[::-1] #max : [255] * 16
    print(f"Secret d = {d}")
    #dmax = digits(max_S, b)
    print(f"d = {len(d) - 1}")
    assert n >= len(d), f"n too small ! (must be >= {len(d)})"

    p = ZZ['x'](d)

    #y_i = p(x_i)
    x = rng.integers(x_low, x_high, n, int) #TODO: prevent duplicates (use rng.choice instead)
    y = [p(i) for i in x]

    #c_i = x_i || y_i
    C = [int(x[i]).to_bytes(lx, byteorder = "big", signed = True)
       + int(y[i]).to_bytes(ly, byteorder = "big", signed = True)
         for i in range(n)]

    H = [sha256(c) for c in C]

    mRS = 8
    nRS = 2**mRS - 1
    kRS = lx + ly
    tRS = (nRS - kRS)//2

    rs = ReedSolomonCode(GF(nRS + 1), ZZ(nRS), ZZ(kRS))# primitive_root = GF(nRS + 1).multiplicative_generator())

    print(f"Using {rs} with decoding radius {tRS}")

    C = [vector([GF(nRS + 1).fetch_int(ci) for ci in c]) for c in C]
    C = [rs.encode(c) for c in C]
    C = [np.array([ci.integer_representation() for ci in c], np.uint8) for c in C]
    C = [np.unpackbits(c).astype(bool) for c in C]

#    ch, cw = 0, 1
#    h, w = W.shape
#    print(W.shape)
#    W = W[ch : h - ch, cw : w - cw]
#    print(W.shape)
#    W = W.astype(bool).ravel()
#
#    B = np.split(W, n)

    B = tile(W, *t)

    D = [B[i].ravel().astype(bool) ^ C[i] for i in range(len(B))]

    return D, H, b, lx, n, len(d) - 1, t, nRS, kRS


def rep(W, D, H, b, lx, n, d, t, nRS, kRS):

#    ch, cw = 0, 1
#    h, w = W.shape
#
#    W = W[ch : h - ch, cw : w - cw]
#
#    W = W.astype(bool).ravel()
#    B = np.split(W, n)

    B = tile(W, *t)

    C = [B[i].ravel().astype(bool) ^ D[i] for i in range(len(D))]
    C = [np.packbits(c) for c in C]

    rs = ReedSolomonCode(GF(nRS + 1), ZZ(nRS), ZZ(kRS))
    mRS = int(np.log(nRS + 1))

    C = [vector(GF(nRS + 1) , [GF(nRS + 1).fetch_int(ci) for ci in c]) for c in C]

    C2 = []
    for i,c in enumerate(C):
        try:
            C2 += [rs.decode_to_code(c)]
        except DecodingError:
            print(i)
            C2 += [rs.random_element()]

    C = [rs.unencode(c) for c in C2]
    C = [[ci.integer_representation() for ci in c] for c in C]
    C = [b''.join([uint_to_bytes(ci) for ci in c]) for c in C]

    H2 = [sha256(c) for c in C]

    npoints = np.count_nonzero((np.array(H) == np.array(H2)))

    if npoints < d + 1: raise ValueError(f"Not enough points were retrieved ({npoints} < {d + 1})")

    print(f"{d + 1} <= {npoints} <= {n} points retrieved")

    x = [bytes_to_int(c[:lx]) for i,c in enumerate(C) if H[i] == H2[i]]#[:d+1]
    y = [bytes_to_int(c[lx+1:]) for i,c in enumerate(C) if H[i] == H2[i]]#[:d+1]

    #Not working everytime
    V = matrix.vandermonde(x, ZZ)
    y = vector(ZZ, y)
    p = V.solve_right(y)
    p = QQ['x'](p.list())

    S = p(b)

    print(f"Retrieved secret = {S}")

    return S

#W = extract_features(fingerfocus(np.load("/home/holbormon/elpis/bio/samples/ds_0_2_3.npz")["p0"], roi = ScannerMkII.ROI[0]))
xmin, xmax, ymin, ymax = 36, 240, 0, 360

#W ds_0_2_3
W = np.load("hashtest.npy")[xmin:xmax, ymin:ymax]
print(W.shape)
#W2 ds_0_2_4
W2 = np.load("hashtest2.npy")[xmin:xmax, ymin:ymax]
#W3 ds_0_3_4
W3 = np.load("hashtest3.npy")[xmin:xmax, ymin:ymax]
#W4 ds_0_7_4
W4 = np.load("hashtest4.npy")[xmin:xmax, ymin:ymax]
#W5 ds_0_8_4
W5 = np.load("hashtest5.npy")[xmin:xmax, ymin:ymax]

show_bool(W, "Gen")

use = W2

show_bool(use, "Rep")
cv.waitKey()
cv.destroyAllWindows()

rep(use, *gen(W))