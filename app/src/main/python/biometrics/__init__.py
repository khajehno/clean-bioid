"""
" All the biometric routines used in 
" the various applications
"""

import logging

from .biocore import extract_features

#Silencing those pesky, bulky messages from matplotlib
logging.getLogger("matplotlib").setLevel(logging.WARNING)