import cv2 as cv
import logging
import numpy as np
import sage
import scipy.linalg as la
import scipy.ndimage as si
import scipy.signal as sp
import sys
import tqdm

from utils import show_bool, shift
from biometrics.scores import miurascore

log = logging.getLogger()
log.setLevel(logging.INFO)

logstream = logging.StreamHandler(sys.stdout)
#logfile = logging.FileHandler(f"hashing.txt", "w+")

log.addHandler(logstream)
#log.addHandler(logfile)
import time
def rand_M(h, w, ch, cw):

    rng = np.random.default_rng()
    M = rng.integers(low = -1, high = 1, size = (h, w))
    M = 2*M + 1
    M[:ch, :] = 0
    M[h - ch:, :] = 0
    M[:, :cw] = 0
    M[:, w - cw:] = 0

    return M

#def dot(A, W): return (A * W).sum()
#
#def hsh(A, W, ε):
#
#    d = dot(A, W)
#
#    return np.sign(d) if np.abs(d) > ε else 0
#
#def H(K, W, ε = 43): return np.array([hsh(A, W, ε) for A in K])
#
#def vec_H(K, W, ε = 43):
#
#    kw = (K * W).sum(axis = (1, 2))
#    kw[np.abs(kw) <= ε] = 0
#
#    return np.sign(kw)

def H(K, W, ε = 55):

    kw = np.einsum("kij,ij->k", K, W)
    kw[np.abs(kw) <= ε] = 0

    return np.sign(kw)

def V_K(K, W, X, ch, cw):

    hkw = H(K, W)
    max_V = 0
    for t in tqdm.tqdm(range(0, 2*ch + 1)):
        for s in range(0, 2*cw + 1):
            if (V := H(K, shift(X, t, s)) @ hkw) > max_V:
                max_V = V

    return max_V

#from biometrics import extract_features, fingerfocus
#from devices.scanners.ScannerMkII import ScannerMkII
#W6 = extract_features(fingerfocus(np.load("/home/holbormon/elpis/bio/samples/ds_0_2_2.npz")["p0"], roi = ScannerMkII.ROI[0]))

#W ds_0_2_3
W = np.load("hashtest.npy")
#W2 ds_0_2_4
W2 = np.load("hashtest2.npy")
#W3 ds_0_3_4
W3 = np.load("hashtest3.npy")
#W4 ds_0_7_4
W4 = np.load("hashtest4.npy")
#W5 ds_0_8_4
W5 = np.load("hashtest5.npy")

d = 80
h, w = W.shape
ch, cw = 30, 90

#print(np.count_nonzero(W[ch : h - ch, cw : w - cw]))
#print(miurascore(W, W2, retmax = True))

#t0 = 41
#s0 = 96

#show_bool(W[ch : h - ch, cw : w - cw], "W")
#show_bool(W2[t0:t0 + h - 2 * ch, s0:s0 + w - 2 * cw], "W2")
#cv.waitKey()
#print(np.count_nonzero(W[ch : h - ch, cw : w - cw].astype(bool) ^ W2[t0:t0 + h - 2 * ch, s0:s0 + w - 2 * cw].astype(bool)))

v = []
for _ in range(1):

    K = np.array([rand_M(h, w, ch, cw) for _ in range(d)])
    v.append(V_K(K, W, W2, ch, cw))

print(v)

### EPSILON = 0
#W vs W : 80, 80, 80
#W vs W2 : 36, 42, 31, 43, 46, 40, 40, 41
#W vs W3 : 36, 38, 40, 34, 42
#W vs W4 : 38, 36, 34
#W vs W5 : 37, 34

### EPSILON = 74 = sqrt(5600)
#W vs W2 : 4, 7, 5, 7
#W vs W3 : 5
#W vs W4 : 5

### EPSILON = 43
#W vs W2 : 12, 12, 15, 14, 12
#W vs W3 : 13, 14, 14
#W vs W4 : 10