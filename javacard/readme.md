# Bio\* Projects: Development guide

###### Corsin Julien
###### JULY 2020

## 0. Index  

1. **General structure** : file structure of the repository
2. **Detailed documentation** : main development guide, one sub-section per project
3. **Server info** : some info about GlobalID's server
4. **Future work** : self-explanatory

## 1. General structure

In this repository you can find code relative to two applications : BioID and BioLocker.  
Each project name is prefixed with the application it refers to : id_ for BioID, lk_ for BioLocker, and tl_ for general purpose tools.  
Executables are generally named in CamelCase whereas regular modules are in lowercase.  
The file structure is as follows :

- code/ch/epfl/lasec :

    - **common** : Regroups utility routines used across all devices and projects

        - biometrics (Python package)
        - cards (Python package)
        - crypto (Python package)
        - databases (Python package)
        - gui (Python package)
        - protocols (Python package)
        - utils (Python package)

    - **desktop** : Regroups all the projects running terminal-side (and some tools)

        - id_demo : Terminal part of BioID's demo  (Java/Python project)

            - biometry : Python/C++ scripts needed for the image processing part of BioID
            - jars : \*.jar libraries used by BioID, with their source/reference if any
            - sfx : Some sound effects/images used by BioID's demo GUI
            - src : Java source of BioID terminal and GUI

        - id_gui : Enrollment and matching interface for BioID (Python Project)

            - keys : RSA Keys folder, must contain enrollment machine secret key and globalID server's public key
            - BioID.py

        - lk_gui : Enrollment and matching interface for BioLocker (Python Project)

            - keys : RSA Keys folder, must contain enrollment machine secret key and globalID server's public key
            - BioLocker.py

        - lk_pGina : Windows login plugin for BioLocker (C# Project)

        - tl_stats : Utility to produce biometric stats from sample sets (Python project)

            - mass_capture : Dataset with the following arboresence
                - user_i :
                    - triplet_j

            - results : Stats output as text files

    - **scanner** : Regroups all the projects running scanner-side

        - idlk_bioscanner : Biometric scanner implementation (Python Project)

            - hardware : Hardware module regrouping low-level routines for the rpi
            - images : Some images for the biometric scanner's TFT screen
            - keys : RSA Keys folder, must contain the scanner's secret key and signing key, globalID server's public key and verification key
            - BioScanner.py : Main executable for the scanner
            - bioscanner.service : Systemd service for rapid setup of a new scanner
            - MassCapture.py : Simple script used to take and store many pictures at once (useful for building a database)

    - **server** : Regroups all the projects running server-side (and some tools)

        - idlk_bioserver : Biometric server implementation (Python Project)

            - keys : Various keys used in the different protocols
            - BioServer.py : Main executable for the server

        - tl_vkedit : Manual verification key database editor (Python Project)

            - VKEdit.py : Main script

    - **smartcard** :

        - id_cardapplet (JavaCard Project)

            - build : Contains a compact version of the tools needed to build a JavaCard applet
            - src : Java source of the BioID applet

    - push_server.sh : Utility script to quickly upload code to the test server (Bash script)
    - push_scanner.sh : Utility script to quickly upload code to a connected scanner (Bash script)

- doc :

    - BioID
    - BioLocker

- samples :

    - Some fingervein samples, with the following file format : ds_{user_number}_{finger_number}_{instance_number}.npz
      Such samples can be generated using common.stats.acquire method
      They are numpy archives to be loaded with np.load(), and contain the original (p0,p1), background eliminated (f0, f1), and vein extracted (v0,v1) pictures


## 2. Detailed documentation


The minimum python version to run any code in this repository is usually 3.6, because of the extensive use of f-strings.  
Specific module dependencies are listed below per-project.  
However, you probably don't need to worry about these requirements because the relevant environments are already installed on the relevant machines.  


### common

As of the big code overhaul of june 2020, many common routines have been extracted in this side library.  
Hence, when exporting a project, one must also take care of exporting the right dependencies needed (signaled by the italic square brackets next to the big section titles below)
Note that **push_scanner.sh** and **push_server.sh** take care of this for the projects **idlk_server** and **idlk_scanner**.

#### biometrics

When this dependency is needed, the target machine may need to recompile **background_elimination.cpp** with the following command (in the **biometrics/** folder) :  

```g++ -std=c++11 background_elimination.cpp -o background_elimination `pkg-config --cflags --libs opencv` ```

For this to work, the target machine needs the package libopencv-dev in version 3.x. Version 4.x also works, but `opencv` must be changed to `opencv4` in the above compiling instruction.

#### databases

Important note : The login info contained within **login.py** is used for *LOCAL* access to the database only (when ssh'ing the server and running DBEdit.py from there).  
                 For security reasons the login is **different** for a remote user who wishes to run DBEdit.py from elsewhere. For now, the remote access login.py file is located at **/home/betul/login.py** on the server.  
                 Note that the reason for creating a remote user is the least privilege security principle : only SELECT, UPDATE, INSERT, DELETE access to the verification keys table is provided, and SSL is enforced. 


### id_demo *[biometrics]*

The demo is a full java GUI to communicate with the java smartcard applet and the biometric scanner (displays a live output for the demo), and has some python scripts for fingervein extraction in a separate folder (**biometry/**).  
The GUI uses some popular open-source libraries called Processing and ControlP5, which can be found in **jars/**. 

Except for the Processing library (which is very intuitive to get used to) and the python/C++ scripts which are called as subprocesses by the JVM, the project is a plain and simple
java project whose development should be straightforward.

#### Installation notes

Prerequisites on the target machine :

- Having followed the guide below for the **Card Applet** (library installation + additional troubleshoot if the demo shows a "Card Terminal not found" error)
- Having Java >=11 installed

Since the GUI is a java project, you can simply export it as a .jar file. If using eclipse, make sure that you check "extract required libraries into JAR" so that you do not have to distribute the jars/ folder.
You can then distribute this jar along with the folders **sfx/** and **biometry/** on the target machine (make sure you place them on the same folder structure level) and run it with java.

#### Side note

This project may be soon deprecated as I have recoded the main Enroll/WAC/SAC protocols in python using pyscard...


### id_gui *[cards][crypto][gui][protocols][utils]*

The GUI for BioID consists of a Tkinter interface (BioID.py)


### lk_gui *[crypto][gui][protocols][utils]*
 
The GUI for BioLocker consists of a Tkinter interface (BioLocker.py)


### id_cardapplet *[]*

This project's development process is a bit trickier, as JavaCard is quite the exotic programming "language", but hopefully this guide and the files I pushed should make it easier to get started quickly.  
Prerequisites :

- Having Apache Ant installed somewhere, in order to build the uploadable .cap file.
- Having the card reader set up and working. On Linux systems, this is pretty straightforward :

    `sudo apt install pcscd libpcsclite1 pcsc-tools`

    Optional: Depending on the model of the card reader you are using, you may need additional packages, which you can find using the command :

    `apt-cache search "PC/SC driver"`

    You can then check that the card is recognized, by running `pcsc_scan` and looking at the live output, which updates as you disconnect and reconnect the card.

The development flow is as follows : 

1. Write the java applet code. Be careful : **JavaCard is not Java ** : limited types, nontrivial EEPROM/RAM management, and different exception handling. The language requires a bit of experience to get used to.
2. CD to the *build* directory, and use ant to run the build.xml file : `ant -f build.xml`. It should be setup correctly, but if you changed the applet id inside the java source files, or the location of the source files, or the JCDK version, you have to correct the build.xml file accordingly.
3. Connect the card to the terminal, and run the script *pushcap.sh*. Again, if you changed the applet ID, you have to correct this script accordingly.
4. Once the script terminates (hopefully with a success if your applet compiles correctly), you can run your terminal program to test/debug.

**Additional Troubleshoot** : If when trying to run the terminal program, Java throws a card-related error (e.g TerminalFactory.list() failed) even though `pcsc_scan` finds your card without problem, it may mean that the JVM cannot find libpcsclite.so on your system. In that case, a simple symlink will do the trick :

`sudo ln -s /usr/lib/x86_64-linux-gnu/libpcsclite.so.1 /usr/lib64/libpcsclite.so`

**Please note that ** :  
- You may have to create /usr/lib64 beforehand.  
- The location/version of libpcsclite.so.\* may be different on your system, so you may need to do some digging (e.g with *find* or *locate*) in order to find the correct left argument of the command.


### idlk_bioscanner *[crypto]*

The scanner consists of a runnable flask webserver (BioScanner.py) with a helper (protocols.py) and some scripts communicating with the hardware (ST_VL6180X.py, ST7735.py, board.py)

#### Python module dependencies :

- opencv-python
- Adafruit-GPIO
- pycryptodomex
- flask
- cherrypy
- paste

#### Installation notes :

- Move the contents of **idlk_bioscanner/ to **/home/pi/bioscanner/** on the scanner (The script **lasec/push_scanner.sh** allows for a quick way, if **bioscanner/** already exists on the target pi).  
- Move the secret key of the scanner to bioscanner/keys/skB.pem.  
- Edit the bioscanner.service file to put the right scanner serial number instead of the placeholder (**scannerN**) and move the file to **/etc/systemd/system/**.  
- Run `systemctl enable bioscanner.service && sudo reboot` for a permanent on-boot start, or `systemctl start bioscanner.service` to simply start the server until next reboot.  

#### Quickly capture several triplets of images (auto led power) :

- In **bioscanner/**, run `python3 MassCapture.py #` where # is the desired number of triplets.  
- A new user will be created if the folder **mass_capture/** has not been emptied beforehand.  
- You can then press enter when prompted to do so to capture each triplet of images.  
- The folder mass_capture will be filled with the captures, following the hierarchy seen in **1. General Structure**.  


### idlk_bioserver *[biometrics][crypto][databases][protocols][utils]*

The server consists of a runnable flask webserver (BioServer.py)

#### Python module dependencies :

- numpy
- scipy
- opencv-python
- mysql-connector-python
- pycryptodomex
- flask
- cherrypy
- paste


### tl_vkedit *[databases]*

VKEdit.py is a manual database editor to quickly add/remove/update verification keys of scanners. 
It is pretty straightforward to use : simply follow the command-line instructions, make your choices and input what is asked for.
Be careful to have the right **login.py** file as mentioned in the **common** section above.


### tl_stats *[biometrics]*

The stat utility was written to compare different background elimination methods, and glove filters :  

- Gather enough pictures from the scanner (for instance using mass_capture.py, see **[BioID&Locker] BioScanner**)  
- Place them in the folder **mass_capture/** according to the hierarchy indicated in **1. General Structure**  
- If you used mass_capture.py, you can just use `./get_mass_capture.sh` to retrieve the images in a folder called mass_capture_to_merge which already follows the right hierarchy  
- Once it is done, just run stats.py : information will be printed, and also written to a file in **results/**   

Note : You can change the background elimination method being used by changing the METHOD variable at the top of the script.  
Available methods are **backelcpp**, **fingerfocus**, **raw**.


### lk_gui *[crypto]*

This is a GUI for BioLocker which consists of simple runnable python script (with a helper).  
Fill the required fields and press "Enroll" or "Match".  
There are no proper exception codes, so if you see "query not well formatted" or "Nonetype is not subscriptable" error messages, it probably means the rsa keys are not matching (Check server logs to be sure).  

#### Python module dependencies :

- tkinter
- pycryptodomex


### lk_pGina *[]*

pGina is an utility to replace windows' default user access control. Plugins for pGina are written in C#. This folder contains the ones written to work with BioLocker, which means they basically send queries to GlobalID's server, and to the biometric scanner as well.
That's all I know, ask Lois for the rest !


## 3. Server info

The server at GlobalID's currently has 4 non-usual ports opened :

- 3306 : For mySQL remote access
- 5000 & 5001 : Were previously used for biolocker and bioID, but now they are unified in the server on port 5000. I think someone relaunched an old server on port 5001 by now.
- 5003 : Test port, used for development of new server versions.

More info on the network configuration can be found on the server in **/home/betul/netconf**

## 4. Future work

Scanner Bug #1 : 

Sometimes the flask applet will be very slow to answer http requests. Rebooting the scanner immediately fixes the problem, so it is hard to see what the bug is.  
One lead was [this stackoverflow post](https://stackoverflow.com/questions/11150343/slow-requests-on-local-flask-server).  
Maybe the problem comes from Scanner.java in the demo, because get requests from my browser are faster, but it would make no sense that on scanner reboot the problem is fixed...  
UPDATE : This looks more and more like a scanner-related problem. Originally it was discovered because the images took more than 1mn to arrive to the bioid-demo screen, but it looks like when it happens it also affects
biolocker requests. Some HTTP answers even look like they're truncated !

Scanner Bug #2 :

Sometimes in the middle of usage, the raspberry pi ('s led module, I assume) will "reset" and flash its rainbow leds, just as it does on startup. If this happens when **BioScanner.py** is running, the setLeds commands will not work anymore and 
thus every picture taken will be pitch black. Rebooting fixes the problem.


One last thing to do is to use TLS for the demo mode protocol between the scanner and the desktop, as suggested at the bottom of **Documentation/BioID/demo mode scanner protocol.txt**.  
Although, this protocol is probably to be deprecated in the future.  

Finally, try to have better statistics for the matching :  
- Try to see if gloves influence the matching, and if so design a filter to apply to a raw picture so that it matches well with a "gloved" picture.  
- Try to see if fingerfocus inside biometrics.py is equally good or better than the background_elimination.cpp script, in terms of stats.
    It's already as performant as the c++ script (I've made sure of that) and it's written in python using numpy/scipy, so it has some practical advantages in terms of code integration (especially considering the demo portability).
  
To the next person working on this I wish good luck, as it is not easy to coordinate all the parts of this big project. But it's a really interesting and cool project, I think, so it's definitely worth it.