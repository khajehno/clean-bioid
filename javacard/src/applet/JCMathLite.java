package applet;

import javacard.framework.ISOException;
import javacard.framework.JCSystem;
import javacard.framework.Util;
import javacard.security.ECPrivateKey;
import javacard.security.ECPublicKey;
import javacard.security.KeyAgreement;
import javacard.security.KeyBuilder;
import javacard.security.KeyPair;
import javacard.security.RSAPublicKey;
import javacardx.crypto.Cipher;

/**
 * @author Based on Vasilios Mavroudis and Petr Svenda's jcmathlib
 */
public class JCMathLite
{
    //The size of speedup engine used for fast modulo exponent computation (must be larger than biggest Bignat used)
    private final static short MODULO_RSA_ENGINE_MAX_LENGTH_BITS = (short) 512;

    //The size of speedup engine used for fast multiplication of large numbers (must be larger than 2x biggest Bignat used)
    private final static short MULT_RSA_ENGINE_MAX_LENGTH_BITS = (short) 1024;

    //The size of largest ECC point used
    private final static short MAX_POINT_SIZE = (short) 64;

    //The size of largest integer used in computations
    private final static short MAX_BIGNAT_SIZE = (short) ((short) (MODULO_RSA_ENGINE_MAX_LENGTH_BITS / 8) + 1);

    private static Bignat helper_BN_A, helper_BN_B, helper_BN_C, helper_BN_F;
    private static Bignat helperEC_BN_A, helperEC_BN_B, helperEC_BN_C, helperEC_BN_D, helperEC_BN_E, helperEC_BN_F;

    private static byte[] uncompressed_point_arr1;

    private static KeyAgreement fnc_multiplication_x_keyAgreement;

    private static byte[] helper_BN_array1, helper_BN_array2;

    private static boolean FLAG_FAST_MULT_VIA_RSA = false; //Set true only if successfully allocated and tested below
    private final static short FAST_MULT_VIA_RSA_TRESHOLD_LENGTH = (short) 16;
    private static Cipher fnc_mult_cipher;
    private static KeyPair fnc_mult_keypair;
    private static RSAPublicKey fnc_mult_pubkey_pow2;
    private static byte[] CONST_TWO = {0x02};

    private static Bignat ONE, TWO, THREE;
    private static Cipher fnc_NmodE_cipher;
    private static RSAPublicKey fnc_NmodE_pubKey;

    static void initlib() 
    {
        helper_BN_A = new Bignat(MAX_BIGNAT_SIZE, JCSystem.MEMORY_TYPE_TRANSIENT_RESET);
        helper_BN_B = new Bignat(MAX_BIGNAT_SIZE, JCSystem.MEMORY_TYPE_TRANSIENT_RESET);
        helper_BN_C = new Bignat(MAX_BIGNAT_SIZE, JCSystem.MEMORY_TYPE_TRANSIENT_RESET);
        helper_BN_F = new Bignat((short) (MAX_BIGNAT_SIZE + 2), JCSystem.MEMORY_TYPE_TRANSIENT_RESET); // +2 is to correct for infrequent RSA result with two or more leading zeroes 

        helperEC_BN_A = new Bignat(MAX_POINT_SIZE, JCSystem.MEMORY_TYPE_TRANSIENT_RESET);
        helperEC_BN_B = new Bignat(MAX_POINT_SIZE, JCSystem.MEMORY_TYPE_TRANSIENT_RESET);
        helperEC_BN_C = new Bignat(MAX_POINT_SIZE, JCSystem.MEMORY_TYPE_TRANSIENT_RESET);
        helperEC_BN_D = new Bignat(MAX_POINT_SIZE, JCSystem.MEMORY_TYPE_TRANSIENT_RESET);
        helperEC_BN_E = new Bignat(MAX_POINT_SIZE, JCSystem.MEMORY_TYPE_TRANSIENT_RESET);
        helperEC_BN_F = new Bignat(MAX_POINT_SIZE, JCSystem.MEMORY_TYPE_TRANSIENT_RESET);

        uncompressed_point_arr1 = JCSystem.makeTransientByteArray((short) (MAX_POINT_SIZE + 1), JCSystem.CLEAR_ON_RESET);
        fnc_multiplication_x_keyAgreement = KeyAgreement.getInstance(KeyAgreement.ALG_EC_SVDP_DH_PLAIN, false);

        helper_BN_array1 = JCSystem.makeTransientByteArray((short)(MULT_RSA_ENGINE_MAX_LENGTH_BITS / 8), JCSystem.CLEAR_ON_RESET);
        helper_BN_array2 = JCSystem.makeTransientByteArray((short)(MULT_RSA_ENGINE_MAX_LENGTH_BITS / 8), JCSystem.CLEAR_ON_RESET);

        //Only reading => Can store in EEPROM
        ONE = new Bignat((short) 1, JCSystem.MEMORY_TYPE_PERSISTENT);
        ONE.one();
        TWO = new Bignat((short) 1, JCSystem.MEMORY_TYPE_PERSISTENT);
        TWO.two();
        THREE = new Bignat((short) 1, JCSystem.MEMORY_TYPE_PERSISTENT);
        THREE.three();

        try //Subsequent code may fail on some real (e.g., Infineon CJTOP80K) cards - catch exception
        {
            fnc_mult_cipher = Cipher.getInstance(Cipher.ALG_RSA_NOPAD, false);
            fnc_mult_keypair = new KeyPair(KeyPair.ALG_RSA_CRT, MULT_RSA_ENGINE_MAX_LENGTH_BITS);
            fnc_mult_keypair.genKeyPair();
            fnc_mult_pubkey_pow2 = (RSAPublicKey) fnc_mult_keypair.getPublic();

            //mult_privkey_pow2 = (RSAPrivateCrtKey) mult_keypair.getPrivate();
            fnc_mult_pubkey_pow2.setExponent(CONST_TWO, (short) 0, (short) CONST_TWO.length);
            fnc_mult_cipher.init(fnc_mult_pubkey_pow2, Cipher.MODE_ENCRYPT);
            // Try operation - if doesn't work, exception SW_CANTALLOCATE_BIGNAT is emitted
            Util.arrayFillNonAtomic(helper_BN_array1, (short) 0, (short) helper_BN_array1.length, (byte) 6);
            fnc_mult_cipher.doFinal(helper_BN_array1, (short) 0, (short) helper_BN_array1.length, helper_BN_array1, (short) 0);
            FLAG_FAST_MULT_VIA_RSA = true;
        }
        catch (Exception ignored) {}

        fnc_NmodE_cipher = Cipher.getInstance(Cipher.ALG_RSA_NOPAD, false);
        fnc_NmodE_pubKey = (RSAPublicKey) KeyBuilder.buildKey(KeyBuilder.TYPE_RSA_PUBLIC, MODULO_RSA_ENGINE_MAX_LENGTH_BITS, false);
    }

    static class Bignat 
    {
        /**
         * Bitmask for extracting a digit out of a longer int/short value. short
         * 0xff for the short/short configuration, long 0xffffffffL the int/long
         * configuration.
         */
        private static final short digit_mask = 0xff;

        //Size in bits of one digit. 8 for the short/short configuration, 32 for the int/long configuration.
        private static final short digit_len = 8;

        //Size in bits of a double digit. 16 for the short/short configuration, 64 for the int/long configuration.
        private static final short double_digit_len = 16;

        /**
         * Bitmask for erasing the sign bit in a double digit. short 0x7fff for the
         * short/short configuration, long 0x7fffffffffffffffL for the int/long
         * configuration.
         */
        private static final short positive_double_digit_mask = 0x7fff;

        //Internal storage array for this Bignat. The current version uses byte array with intermediate values stored which can be quickly processed with 
        private byte[] value;               
        private short size = -1;     // Current size of stored Bignat. Current number is encoded in first {@code size} of value array, starting from value[0]
        private short max_size = -1; // Maximum size of this Bignat. Corresponds to value.length

        Bignat(short maxSize, byte allocatorType)
        {
            this.size = maxSize;
            this.max_size = maxSize;
            if(allocatorType == JCSystem.MEMORY_TYPE_PERSISTENT)
                this.value = new byte[this.max_size];
            else
                this.value = JCSystem.makeTransientByteArray(this.max_size, allocatorType);
        }

        /**
         * Construct a Bignat with provided array used as internal storage as well as initial value.
         * No copy of array is made. If this Bignat is used in operation which modifies the Bignat value, 
         * content of provided array is changed.
         * @param valueBuffer internal storage
         */
        Bignat(byte[] valueBuffer) 
        {
            this.size = (short) valueBuffer.length;
            this.max_size = (short) valueBuffer.length;
            this.value = valueBuffer;
        }

        /**
         * Return this Bignat as byte array. For the short/short configuration
         * simply the digit array is returned. For other configurations a new short
         * array is allocated and returned. Modifying the returned short array
         * therefore might or might not change this bignat.
         * IMPORTANT: this function returns directly the underlying storage array. 
         * Current value of this Bignat can be stored in smaller number of bytes. 
         * Use {@code getLength()} method to obtain actual size.
         *
         * @return this bignat as byte array
         */
        byte[] asByteArray() 
        {
            return value;
        }

        /**
         * Serialize this Bignat value into a provided buffer
         * @param buffer target buffer
         * @param bufferOffset start offset in buffer
         * @return number of bytes copied
         */
        private short copyToBuffer(byte[] buffer, short bufferOffset) 
        {
            Util.arrayCopyNonAtomic(value, (short) 0, buffer, bufferOffset, size);
            return size;
        }

        /**
         * Return the size in digits. Provides access to the internal size field.
         * <P>
         * The return value is adjusted by setSize().
         * 
         * @return size in digits.
         */
        short length() 
        {
            return size;
        }

        /**
         * Sets internal size of Bignat. Previous value are kept so value is either non-destructively trimmed or enlarged. 
         * @param newSize new size of Bignat. Must be in range of [0, max_size] where max_size was provided during object creation
         */
        private void setSize(short newSize) 
        {
            if(newSize < 0 || newSize > max_size)
                ISOException.throwIt(SW_BIGNAT_RESIZETOLONGER);
            else
                this.size = newSize;
        }

        /**
         * Resize internal length of this Bignat to maximum size given during object
         * creation. If required, object is also zeroized
         *
         * @param bZeroize if true, all bytes of internal array are also set to
         * zero. If false, previous value is kept.
         */
        private void resize_to_max(boolean zeroize)
        {
            setSize(max_size);
            if(zeroize)
                zero();
        }

        /**
         * Create Bignat with different number of bytes used. Will cause longer number 
         * to shrink (loss of the more significant bytes) and shorter to be prepended with zeroes
         *
         * @param new_size new size in bytes
         */
        private void deepResize(short new_size) 
        {
            if(new_size > this.max_size)
                ISOException.throwIt(SW_BIGNAT_REALLOCATIONNOTALLOWED);

            if(new_size != this.size)
            {
                short this_start, other_start, len;
                byte[] fnc_deep_resize_tmp = new byte[(short) (MULT_RSA_ENGINE_MAX_LENGTH_BITS / 8)];
                if (this.size >= new_size) 
                {
                    this_start = (short) (this.size - new_size);
                    other_start = 0;
                    len = new_size;

                    // Shrinking/cropping 
                    Util.arrayCopyNonAtomic(value, this_start, fnc_deep_resize_tmp, (short) 0, len);
                    Util.arrayCopyNonAtomic(fnc_deep_resize_tmp, (short) 0, value, (short) 0, len); // Move bytes in item array towards beggining

                    // Erase rest of allocated array with zeroes (just as sanitization)
                    short toErase = (short) (this.max_size - new_size);

                    if (toErase > 0)
                        Util.arrayFillNonAtomic(value, new_size, toErase, (byte) 0);
                }
                else
                {
                    this_start = 0;
                    other_start = (short) (new_size - this.size);
                    len = this.size;

                    // Enlarging => Insert zeroes at begging, move bytes in item array towards the end
                    Util.arrayCopyNonAtomic(value, this_start, fnc_deep_resize_tmp, (short) 0, len);
                    // Move bytes in item array towards end
                    Util.arrayCopyNonAtomic(fnc_deep_resize_tmp, (short) 0, value, other_start, len);

                    // Fill begin of array with zeroes (just as sanitization)
                    if (other_start > 0)
                        Util.arrayFillNonAtomic(value, (short) 0, other_start, (byte) 0);
                }

                setSize(new_size);
            }
        }


        /**
         * Appends zeros in the suffix to reach the defined byte length 
         * Essentially multiplies the number with 16 (HEX) 
         * @param targetLength required length including appended zeroes
         * @param outBuffer output buffer for value with appended zeroes
         * @param outOffset start offset inside outBuffer for write
         */
        private void append_zeros(short targetLength, byte[] outBuffer, short outOffset) 
        {
            Util.arrayCopyNonAtomic(value, (short) 0, outBuffer, outOffset, this.size); //copy the value
            Util.arrayFillNonAtomic(outBuffer, (short) (outOffset + this.size), (short) (targetLength - this.size), (byte) 0); //append zeros
        }
        /**
         * Prepends zeros before the value of this Bignat up to target length. 
         *
         * @param targetLength required length including prepended zeroes
         * @param outBuffer output buffer for value with prepended zeroes
         * @param outOffset start offset inside outBuffer for write
         */
        private void prepend_zeros(short targetLength, byte[] outBuffer, short outOffset) 
        {
            short other_start = (short) (targetLength - this.size);

            if (other_start > 0)
                Util.arrayFillNonAtomic(outBuffer, outOffset, other_start, (byte) 0); //fill prefix with zeros

            Util.arrayCopyNonAtomic(value, (short) 0, outBuffer, (short) (outOffset + other_start), this.size); //copy the value
        }

        //Remove leading zeroes (if any) from Bignat value and decrease size accordingly
        private void shrink() 
        {
            short i = 0;

            for (i = 0; i < this.length(); i++) 
            { 
                if (this.value[i] != 0) // Find first non-zero byte
                    break;
            }

            short new_size = (short)(this.size - i);

            if (new_size < 0)
                ISOException.throwIt(SW_BIGNAT_INVALIDRESIZE);

            this.deepResize(new_size);
        }

        //Stores zero in this object for currently used subpart given by internal size.
        private void zero() {
            Util.arrayFillNonAtomic(value, (short) 0, this.size, (byte) 0);
        }

        //Stores one in this object. Keeps previous size of this Bignat (1 is prepended with required number of zeroes).
        private void one() 
        {
            this.zero();
            value[(short) (size - 1)] = 1;
        }

        //Stores two in this object. Keeps previous size of this Bignat (2 is prepended with required number of zeroes).
        private void two() 
        {
            this.zero();
            value[(short) (size - 1)] = 0x02;
        }

        private void three() 
        {
            this.zero();
            value[(short) (size - 1)] = 0x03;
        }

        /**
         * Copies {@code other} into this. No size requirements. If {@code other}
         * has more digits then the superfluous leading digits of {@code other} are
         * asserted to be zero. If this bignat has more digits than its leading
         * digits are correctly initilized to zero. This function will not change size 
         * attribute of this object.
         *
         * @param other
         *            Bignat to copy into this object.
         */
        private void copy(Bignat other) 
        {
            short this_start, other_start, len;
            if (this.size >= other.size) {
                this_start = (short) (this.size - other.size);
                other_start = 0;
                len = other.size;
            } else {
                this_start = 0;
                other_start = (short) (other.size - this.size);
                len = this.size;
                // Verify here that other have leading zeroes up to other_start
                for (short i = 0; i < other_start; i ++) {
                    if (other.value[i] != 0) {
                        ISOException.throwIt(SW_BIGNAT_INVALIDCOPYOTHER);
                    }
                }
            }

            if (this_start > 0) {
                // if this bignat has more digits than its leading digits are initilized to zero
                Util.arrayFillNonAtomic(this.value, (short) 0, this_start, (byte) 0);
            }
            Util.arrayCopyNonAtomic(other.value, other_start, this.value, this_start, len);
        }

        /**
         * Copies content of {@code other} into this and set size of this to {@code other}. 
         * The size attribute (returned by length()) is updated. If {@code other}
         * is longer than maximum capacity of this, internal buffer is reallocated if enabled 
         * (ALLOW_RUNTIME_REALLOCATION), otherwise exception is thrown.
         * @param other 
         *            Bignat to clone into this object.
         */
        private void clone(Bignat other) 
        {
            //Reallocate array only if current array cannot store the other value and reallocation is enabled by ALLOW_RUNTIME_REALLOCATION
            if (this.max_size < other.length())
                ISOException.throwIt(SW_BIGNAT_REALLOCATIONNOTALLOWED);

            //Copy value from other into proper place in this (this can be longer than other so rest of bytes will be filled with 0)
            other.copyToBuffer(this.value, (short) 0);

            if (this.max_size > other.length())
                Util.arrayFillNonAtomic(this.value, other.length(), (short) (this.max_size - other.length()), (byte) 0);

            this.size = other.length();
        }

        /**
         * Addition of big integers x and y stored in byte arrays with specified offset and length.
         * The result is stored into x array argument. 
         * @param x          array with first bignat
         * @param xOffset    start offset in array of {@code x}
         * @param xLength    length of {@code x}
         * @param y          array with second bignat
         * @param yOffset    start offset in array of {@code y}
         * @param yLength    length of {@code y}
         * @return true if carry of most significant byte occurs, false otherwise  
         */
        private boolean add(byte[] x, short xOffset, short xLength, byte[] y, short yOffset, short yLength)
        {
            short result = 0;
            short i = (short) (xLength + xOffset - 1);
            short j = (short) (yLength + yOffset - 1);

            for (; i >= xOffset && j >= 0; i--, j--) 
            {
                result = (short) (result + (short) (x[i] & digit_mask) + (short) (y[j] & digit_mask));

                x[i] = (byte) (result & digit_mask);
                result = (short) ((result >> digit_len) & digit_mask);
            }

            while (result > 0 && i >= xOffset) 
            {
                result = (short) (result + (short) (x[i] & digit_mask));
                x[i] = (byte) (result & digit_mask);
                result = (short) ((result >> digit_len) & digit_mask);
                i--;
            }

            return result != 0;
        }

        /**
         * Subtracts big integer y from x specified by offset and length.
         * The result is stored into x array argument.
         * @param x array with first bignat
         * @param xOffset start offset in array of {@code x}
         * @param xLength length of {@code x}
         * @param y array with second bignat
         * @param yOffset start offset in array of {@code y}
         * @param yLength length of {@code y}
         * @return true if carry of most significant byte occurs, false otherwise
         */
        private static boolean subtract(byte[] x, short xOffset, short xLength, byte[] y, short yOffset, short yLength)
        {
            short i = (short) (xLength + xOffset - 1);
            short j = (short) (yLength + yOffset - 1);
            short carry = 0;
            short subtraction_result = 0;

            for (; i >= xOffset && j >= yOffset; i--, j--) {
                subtraction_result = (short) ((x[i] & digit_mask) - (y[j] & digit_mask) - carry);
                x[i] = (byte) (subtraction_result & digit_mask);
                carry = (short) (subtraction_result < 0 ? 1 : 0);
            }
            for (; i >= xOffset && carry > 0; i--) {
                if (x[i] != 0) {
                    carry = 0;
                }
                x[i] -= 1;
            }

            return carry > 0;
        }

        /**
         * Subtract other Bignat from this Bignat.
         * @param other Bignat to be subtracted from this
         */
        private void subtract(Bignat other)
        {
            this.times_minus(other, (short) 0, (short) 1);
        }

        /**
         * Scaled subtraction. Subtracts {@code mult * 2^(}{@link #digit_len}
         * {@code  * shift) * other} from this.
         * <P>
         * That is, shifts {@code mult * other} precisely {@code shift} digits to
         * the left and subtracts that value from this. {@code mult} must be less
         * than {@link #bignat_base}, that is, it must fit into one digit. It is
         * only declared as short here to avoid negative values.
         * <P>
         * {@code mult} has type short.
         * <P>
         * No size constraint. However, an assertion is thrown, if the result would
         * be negative. {@code other} can have more digits than this object, but
         * then sufficiently many leading digits must be zero to avoid the
         * underflow.
         * <P>
         * Used in division.
         * 
         * @param other
         *            Bignat to subtract from this object
         * @param shift
         *            number of digits to shift {@code other} to the left
         * @param mult
         *            of type short, multiple of {@code other} to subtract from this
         *            object. Must be below {@link #bignat_base}.
         */
        private void times_minus(Bignat other, short shift, short mult) 
        {
            short akku = 0;
            short subtraction_result;
            short i = (short) (this.size - 1 - shift);
            short j = (short) (other.size - 1);
            for (; i >= 0 && j >= 0; i--, j--) {
                akku = (short) (akku + (short) (mult * (other.value[j] & digit_mask)));
                subtraction_result = (short) ((value[i] & digit_mask) - (akku & digit_mask));

                value[i] = (byte) (subtraction_result & digit_mask);
                akku = (short) ((akku >> digit_len) & digit_mask);
                if (subtraction_result < 0) {
                    akku++;
                }
            }

            // deal with carry as long as there are digits left in this
            while (i >= 0 && akku != 0) {
                subtraction_result = (short) ((value[i] & digit_mask) - (akku & digit_mask));
                value[i] = (byte) (subtraction_result & digit_mask);
                akku = (short) ((akku >> digit_len) & digit_mask);
                if (subtraction_result < 0) {
                    akku++;
                }
                i--;
            }
        }

        /**
         * Quick function for decrement of this Bignat value by 1. Faster than subtract(Bignat.one())
         */
        private void decrement_one() 
        {
            short tmp = 0;
            for (short i = (short) (this.size - 1); i >= 0; i--) {
                tmp = (short) (this.value[i] & 0xff);
                this.value[i] = (byte) (tmp - 1);
                if (tmp != 0) {
                    break; // CTO
                }
                else {
                    // need to modify also one byte up, continue with cycle
                }
            }
        }

        /**
         * Index of the most significant 1 bit.
         * <P>
         * {@code x} has type short.
         * <P>
         * Utility method, used in division.
         * 
         * @param x
         *            of type short
         * @return index of the most significant 1 bit in {@code x}, returns
         *         {@link #double_digit_len} for {@code x == 0}.
         */
        private static short highest_bit(short x) {
            for (short i = 0; i < double_digit_len; i++) 
            {
                if (x < 0) {
                    return i;
                }
                x <<= 1;
            }
            return double_digit_len;
        }

        /**
         * Shift to the left and fill. Takes {@code high} {@code middle} {@code low}
         * as 4 digits, shifts them {@code shift} bits to the left and returns the
         * most significant {@link #double_digit_len} bits.
         * <P>
         * Utility method, used in division.
         * 
         * 
         * @param high
         *            of type short, most significant {@link #double_digit_len} bits
         * @param middle
         *            of type byte, middle {@link #digit_len} bits
         * @param low
         *            of type byte, least significant {@link #digit_len} bits
         * @param shift
         *            amount of left shift
         * @return most significant {@link #double_digit_len} as short
         */
        private static short shift_bits(short high, byte middle, byte low,
                short shift) {
            // shift high
            high <<= shift;

            // merge middle bits
            byte mask = (byte) (digit_mask << (shift >= digit_len ? 0 : digit_len
                    - shift));
            short bits = (short) ((short) (middle & mask) & digit_mask);
            if (shift > digit_len) {
                bits <<= shift - digit_len;
            }
            else {
                bits >>>= digit_len - shift;
            }
            high |= bits;

            if (shift <= digit_len) {
                return high;
            }

            // merge low bits
            mask = (byte) (digit_mask << double_digit_len - shift);
            bits = (short) ((((short) (low & mask) & digit_mask) >> double_digit_len - shift));
            high |= bits;

            return high;
        }

        /**
         * Scaled comparison. Compares this number with {@code other * 2^(}
         * {@link #digit_len} {@code * shift)}. That is, shifts {@code other}
         * {@code shift} digits to the left and compares then. This bignat and
         * {@code other} will not be modified inside this method.
         * <P>
         * 
         * As optimization {@code start} can be greater than zero to skip the first
         * {@code start} digits in the comparison. These first digits must be zero
         * then, otherwise an assertion is thrown. (So the optimization takes only
         * effect when <a
         * href="../../../overview-summary.html#NO_CARD_ASSERT">NO_CARD_ASSERT</a>
         * is defined.)
         * 
         * @param other
         *            Bignat to compare to
         * @param shift
         *            left shift of other before the comparison
         * @param start
         *            digits to skip at the beginning
         * @return true if this number is strictly less than the shifted
         *         {@code other}, false otherwise.
         */
        private boolean shift_lesser(Bignat other, short shift, short start) {
            short j;

            j = (short) (other.size + shift - this.size + start);
            short this_short, other_short;
            for (short i = start; i < this.size; i++, j++) {
                this_short = (short) (this.value[i] & digit_mask);
                if (j >= 0 && j < other.size) {
                    other_short = (short) (other.value[j] & digit_mask);
                }
                else {
                    other_short = 0;
                }
                if (this_short < other_short) {
                    return true; // CTO
                }
                if (this_short > other_short) {
                    return false;
                }
            }
            return false;
        }

        /**
         * Comparison of this and other.
         *
         * @param other
         *            Bignat to compare with
         * @return true if this number is strictly lesser than {@code other}, false
         *         otherwise.
         */
        boolean lesser(Bignat other) 
        {
            return this.shift_lesser(other, (short) 0, (short) 0);
        }

        /**
         * Test equality with zero.
         *
         * @return true if this bignat equals zero.
         */
        boolean is_zero()
        {
            for (short i = 0; i < size; i++)
            {
                if (value[i] != 0) {
                    return false; // CTO
                }
            }
            return true;
        }

        /**
         * Remainder and Quotient. Divide this number by {@code divisor} and store
         * the remainder in this. If {@code quotient} is non-null store the quotient
         * there.
         * <P>
         * There are no direct size constraints, but if {@code quotient} is
         * non-null, it must be big enough for the quotient, otherwise an assertion
         * is thrown.
         * <P>
         * Uses schoolbook division inside and has O^2 complexity in the difference
         * of significant digits of the divident (in this number) and the divisor.
         * For numbers of equal size complexity is linear.
         * 
         * @param divisor
         *            must be non-zero
         * @param quotient
         *            gets the quotient if non-null
         */
        @SuppressWarnings("unused")
        private void remainder_divide(Bignat divisor, Bignat quotient) 
        {
            // There are some size requirements, namely that quotient must
            // be big enough. However, this depends on the value of the
            // divisor and is therefore not stated here.

            // zero-initialize the quotient, because we are only adding to it below
            if (quotient != null) {
                quotient.zero();
            }

            // divisor_index is the first nonzero digit (short) in the divisor
            short divisor_index = 0;
            while (divisor.value[divisor_index] == 0) {
                divisor_index++;
            }

            // The size of this might be different from divisor. Therefore,
            // for the first subtraction round we have to shift the divisor
            // divisor_shift = this.size - divisor.size + divisor_index
            // digits to the left. If this amount is negative, then
            // this is already smaller then divisor and we are done.
            // Below we do divisor_shift + 1 subtraction rounds. As an
            // additional loop index we also count the rounds (from
            // zero upwards) in division_round. This gives access to the
            // first remaining divident digits.
            short divisor_shift = (short) (this.size - divisor.size + divisor_index);
            short division_round = 0;

            // We could express now a size constraint, namely that
            // divisor_shift + 1 <= quotient.size
            // However, in the proof protocol we divide x / v, where
            // x has 2*n digits when v has n digits. There the above size
            // constraint is violated, the division is however valid, because
            // it will always hold that x < v * (v - 1) and therefore the
            // quotient will always fit into n digits.
            // System.out.format("XX this size %d div ind %d div shift %d " +
            // "quo size %d\n" +
            // "%s / %s\n",
            // this.size,
            // divisor_index,
            // divisor_shift,
            // quotient != null ? quotient.size : -1,
            // this.to_hex_string(),
            // divisor.to_hex_string());
            // The first digits of the divisor are needed in every
            // subtraction round.
            short first_divisor_digit = (short) (divisor.value[divisor_index] & digit_mask);
            short divisor_bit_shift = (short) (highest_bit((short) (first_divisor_digit + 1)) - 1);
            byte second_divisor_digit = divisor_index < (short) (divisor.size - 1) ? divisor.value[(short) (divisor_index + 1)]
                    : 0;
            byte third_divisor_digit = divisor_index < (short) (divisor.size - 2) ? divisor.value[(short) (divisor_index + 2)]
                    : 0;

            // The following variables are used inside the loop only.
            // Declared here as optimization.
            // divident_digits and divisor_digit hold the first one or two
            // digits. Needed to compute the multiple of the divisor to
            // subtract from this.
            short divident_digits, divisor_digit;

            // To increase precisision the first digits are shifted to the
            // left or right a bit. The following variables compute the shift.
            short divident_bit_shift, bit_shift;

            // Declaration of the multiple, with which the divident is
            // multiplied in each round and the quotient_digit. Both are
            // a single digit, but declared as a double digit to avoid the
            // trouble with negative numbers. If quotient != null multiple is
            // added to the quotient. This addition is done with quotient_digit.
            short multiple, quotient_digit;
            short numLoops = 0;
            short numLoops2 = 0;
            while (divisor_shift >= 0) {
                numLoops++; // CTO number of outer loops is constant (for given length of divisor)
                // Keep subtracting from this until
                // divisor * 2^(8 * divisor_shift) is bigger than this.
                while (!shift_lesser(divisor, divisor_shift,
                        (short) (division_round > 0 ? division_round - 1 : 0))) {
                    numLoops2++; // BUGBUG: CTO - number of these loops fluctuates heavily => strong impact on operation time 
                    // this is bigger or equal than the shifted divisor.
                    // Need to subtract some multiple of divisor from this.
                    // Make a conservative estimation of the multiple to subtract.
                    // We estimate a lower bound to avoid underflow, and continue
                    // to subtract until the remainder in this gets smaller than
                    // the shifted divisor.
                    // For the estimation get first the two relevant digits
                    // from this and the first relevant digit from divisor.
                    divident_digits = division_round == 0 ? 0
                            : (short) ((short) (value[(short) (division_round - 1)]) << digit_len);
                    divident_digits |= (short) (value[division_round] & digit_mask);

                    // The multiple to subtract from this is
                    // divident_digits / divisor_digit, but there are two
                    // complications:
                    // 1. divident_digits might be negative,
                    // 2. both might be very small, in which case the estimated
                    // multiple is very inaccurate.
                    if (divident_digits < 0) {
                        // case 1: shift both one bit to the right
                        // In standard java (ie. in the test frame) the operation
                        // for >>= and >>>= seems to be done in integers,
                        // even if the left hand side is a short. Therefore,
                        // for a short left hand side there is no difference
                        // between >>= and >>>= !!!
                        // Do it the complicated way then.
                        divident_digits = (short) ((divident_digits >>> 1) & positive_double_digit_mask);
                        divisor_digit = (short) ((first_divisor_digit >>> 1) & positive_double_digit_mask);
                    } else {
                        // To avoid case 2 shift both to the left
                        // and add relevant bits.
                        divident_bit_shift = (short) (highest_bit(divident_digits) - 1);
                        // Below we add one to divisor_digit to avoid underflow.
                        // Take therefore the highest bit of divisor_digit + 1
                        // to avoid running into the negatives.
                        bit_shift = divident_bit_shift <= divisor_bit_shift ? divident_bit_shift
                                : divisor_bit_shift;

                        divident_digits = shift_bits(
                                divident_digits,
                                division_round < (short) (this.size - 1) ? value[(short) (division_round + 1)]
                                        : 0,
                                        division_round < (short) (this.size - 2) ? value[(short) (division_round + 2)]
                                                : 0, bit_shift);
                        divisor_digit = shift_bits(first_divisor_digit,
                                second_divisor_digit, third_divisor_digit,
                                bit_shift);

                    }

                    // add one to divisor to avoid underflow
                    multiple = (short) (divident_digits / (short) (divisor_digit + 1));

                    // Our strategy to avoid underflow might yield multiple == 0.
                    // We know however, that divident >= divisor, therefore make
                    // sure multiple is at least 1.
                    if (multiple < 1) {
                        multiple = 1;
                    }

                    times_minus(divisor, divisor_shift, multiple);

                    // build quotient if desired
                    if (quotient != null) {
                        // Express the size constraint only here. The check is
                        // essential only in the first round, because
                        // divisor_shift decreases. divisor_shift must be
                        // strictly lesser than quotient.size, otherwise
                        // quotient is not big enough. Note that the initially
                        // computed divisor_shift might be bigger, this
                        // is OK, as long as we don't reach this point.

                        quotient_digit = (short) ((quotient.value[(short) (quotient.size - 1 - divisor_shift)] & digit_mask) + multiple);
                        quotient.value[(short) (quotient.size - 1 - divisor_shift)] = (byte) (quotient_digit);
                    }
                }

                // treat loop indices
                division_round++;
                divisor_shift--;
            }
        }

        /**
         * Addition. Adds other to this number. 
         * <P>
         * Same as {@link #times_add times_add}{@code (other, 1)} but without the
         * multiplication overhead.
         * <P>
         * Asserts that the size of other is not greater than the size of this.
         * 
         * @param other
         *            Bignat to add
         */
        private void add(Bignat other) 
        {
            add_carry(other);
        }

        /**
         * Addition with carry report. Adds other to this number. If this is too
         * small for the result (i.e., an overflow occurs) the method returns true.
         * Further, the result in {@code this} will then be the correct result of an
         * addition modulo the first number that does not fit into {@code this} (
         * {@code 2^(}{@link #digit_len}{@code * }{@link #size this.size}{@code )}),
         * i.e., only one leading 1 bit is missing. If there is no overflow the
         * method will return false.
         * <P>
         * 
         * It would be more natural to report the overflow with an
         * {@link javacard.framework.UserException}, however its
         * {@link javacard.framework.UserException#throwIt throwIt} method dies with
         * a null pointer exception when it runs in a host test frame...
         * <P>
         * 
         * Asserts that the size of other is not greater than the size of this.
         * 
         * @param other Bignat to add
         * @param otherOffset start offset within other buffer
         * @param otherLen length of other
         * @return true if carry occurs, false otherwise
         */
        private boolean add_carry(byte[] other, short otherOffset, short otherLen) 
        {
            short akku = 0;
            short j = (short) (this.size - 1);
            for (short i = (short) (otherLen - 1); i >= 0 && j >= 0; i--, j--) {
                akku = (short) (akku + (short) (this.value[j] & digit_mask) + (short) (other[(short) (i + otherOffset)] & digit_mask));

                this.value[j] = (byte) (akku & digit_mask);
                akku = (short) ((akku >> digit_len) & digit_mask);
            }
            // add carry at position j
            while (akku > 0 && j >= 0) {
                akku = (short) (akku + (short) (this.value[j] & digit_mask));
                this.value[j] = (byte) (akku & digit_mask);
                akku = (short) ((akku >> digit_len) & digit_mask);
                j--;
            }

            return akku != 0;
        }

        /**
         * Add with carry. See {@code add_cary()} for full description
         * @param other value to be added
         * @return true if carry happens, false otherwise
         */
        private boolean add_carry(Bignat other) 
        {
            return add_carry(other.value, (short) 0, other.size);
        }

        /**
         * Add other bignat to this bignat modulo {@code modulo} value. 
         * @param other value to add
         * @param modulo value of modulo to compute 
         */
        void mod_add(Bignat other, Bignat modulo)
        { 
            short tmp_size = this.size;
            if (tmp_size < other.size) {
                tmp_size = other.size;
            }
            tmp_size++;
            helper_BN_A.setSize(tmp_size); 
            helper_BN_A.zero();
            helper_BN_A.copy(this);
            helper_BN_A.add(other);
            helper_BN_A.mod(modulo);
            helper_BN_A.shrink();
            this.clone(helper_BN_A);
        }

        /**
         * Substract other bignat from this bignat modulo {@code modulo} value.
         *
         * @param other value to substract
         * @param modulo value of modulo to apply
         */
        private void mod_sub(Bignat other, Bignat modulo) 
        {
            if (other.lesser(this)) 
            { //CTO
                this.subtract(other);
                this.mod(modulo);
            }
            else
            { //other > this ( mod - other + this)
                helper_BN_C.clone(other);
                helper_BN_C.mod(modulo);

                helper_BN_A.clone(this);
                helper_BN_A.mod(modulo);

                helper_BN_B.clone(modulo);
                helper_BN_B.subtract(helper_BN_C);
                helper_BN_B.add(helper_BN_A); //this will never overflow as "other" is larger than "this"
                helper_BN_B.mod(modulo);
                helper_BN_B.shrink();
                this.clone(helper_BN_B);
            }
        }

        /**
         * Scaled addition. Adds mult * other * 2^(digit_len * shift)} 
         * to this. That is, shifts other shift digits to
         * the left, multiplies it with mult and adds them.
         * <P>
         * mult must be less than {@link #bignat_base}, that is, it must fit
         * into one digit. It is only declared as a short here to avoid negative
         * numbers.
         * <P>
         * Asserts that the size of this is greater than or equal to
         * {@code other.size + shift + 1}.
         *
         * @param x Bignat to add
         * @param mult of short, factor to multiply {@code other} with before
         * addition. Must be less than {@link #bignat_base}.
         * @param shift number of digits to shift {@code other} to the left, before
         * addition.
         */
        private void times_add_shift(Bignat x, short shift, short mult) 
        {
            short akku = 0;
            short j = (short) (this.size - 1 - shift);
            for (short i = (short) (x.size - 1); i >= 0; i--, j--) 
            {
                akku = (short) (akku + (short) (this.value[j] & digit_mask) + (short) (mult * (x.value[i] & digit_mask)));

                this.value[j] = (byte) (akku & digit_mask);
                akku = (short) ((akku >> digit_len) & digit_mask);
            }
            // add carry at position j
            akku = (short) (akku + (short) (this.value[j] & digit_mask));
            this.value[j] = (byte) (akku & digit_mask);
            // BUGUG: assert no overflow
        }

        /**
         * Multiplication. Automatically selects fastest available algorithm. 
         * Stores {@code x * y} in this. To ensure this is big
         * enough for the result it is asserted that the size of this is greater
         * than or equal to the sum of the sizes of {@code x} and {@code y}.
         * 
         * @param x
         *            first factor
         * @param y
         *            second factor
         */
        private void mult(Bignat x, Bignat y)
        {
            // If not supported, use slow multiplication
            // Use slow multiplication also when numbers are small => faster to do in software 
            if (FLAG_FAST_MULT_VIA_RSA || x.length() < FAST_MULT_VIA_RSA_TRESHOLD_LENGTH)
                mult_schoolbook(x, y);
            else
                mult_rsa_trick(x, y, null, null);
        }

        /** 
         * Slow schoolbook algorithm for multiplication
         * @param x first number to multiply
         * @param y second number to multiply
         */
        private void mult_schoolbook(Bignat x, Bignat y) 
        {
            this.zero(); // important to keep, used in exponentiation()
            for (short i = (short) (y.size - 1); i >= 0; i--)
                this.times_add_shift(x, (short) (y.size - 1 - i), (short) (y.value[i] & digit_mask));
        }

        /**
         * Performs multiplication of two bignats x and y and stores result into this. 
         * RSA engine is used to speedup operation for large values.
         * Idea of speedup: 
         * We need to mutiply x.y where both x and y are 32B 
         * (x + y)^2 == x^2 + y^2 + 2xy 
         * Fast RSA engine is available (a^b mod n) 
         * n can be set bigger than 64B => a^b mod n == a^b 
         * [(x + y)^2 mod n] - [x^2 mod n] - [y^2 mod n] => 2xy where [] means single RSA operation 
         * 2xy / 2 => result of mult(x,y) 
         * Note: if multiplication is used with either x or y argument same repeatedly, 
         * [x^2 mod n] or [y^2 mod n] can be precomputed and passed as arguments x_pow_2 or y_pow_2
         *
         * @param x first value to multiply
         * @param y second value to multiply
         * @param x_pow_2 if not null, array with precomputed value x^2 is expected
         * @param y_pow_2 if not null, array with precomputed value y^2 is expected
         */
        private void mult_rsa_trick(Bignat x, Bignat y, byte[] x_pow_2, byte[] y_pow_2) {
            short xOffset;
            short yOffset;

            // x+y
            Util.arrayFillNonAtomic(helper_BN_array1, (short) 0, (short) helper_BN_array1.length, (byte) 0);
            // We must copy bigger number first
            if (x.size > y.size) {
                // Copy x to the end of mult_resultArray
                xOffset = (short) (helper_BN_array1.length - x.length());
                Util.arrayCopyNonAtomic(x.value, (short) 0, helper_BN_array1, xOffset, x.length());
                if (add(helper_BN_array1, xOffset, x.size, y.value, (short) 0, y.size)) {
                    xOffset--;
                    helper_BN_array1[xOffset] = 0x01;
                }
            } else {
                // Copy x to the end of mult_resultArray
                yOffset = (short) (helper_BN_array1.length - y.length());
                Util.arrayCopyNonAtomic(y.value, (short) 0, helper_BN_array1, yOffset, y.length());
                if (add(helper_BN_array1, yOffset, y.size, x.value, (short) 0, x.size)) {
                    yOffset--;
                    helper_BN_array1[yOffset] = 0x01; // add carry if occured
                }
            }

            // ((x+y)^2)
            fnc_mult_cipher.doFinal(helper_BN_array1, (byte) 0, (short) helper_BN_array1.length, helper_BN_array1, (short) 0);

            // x^2
            if (x_pow_2 == null) {
                // x^2 is not precomputed
                Util.arrayFillNonAtomic(helper_BN_array2, (short) 0, (short) helper_BN_array2.length, (byte) 0);
                xOffset = (short) (helper_BN_array2.length - x.length());
                Util.arrayCopyNonAtomic(x.value, (short) 0, helper_BN_array2, xOffset, x.length());
                fnc_mult_cipher.doFinal(helper_BN_array2, (byte) 0, (short) helper_BN_array2.length, helper_BN_array2, (short) 0);
            } else {
                // x^2 is precomputed
                if ((short) x_pow_2.length != (short) helper_BN_array2.length) {
                    Util.arrayFillNonAtomic(helper_BN_array2, (short) 0, (short) helper_BN_array2.length, (byte) 0);
                    xOffset = (short) ((short) helper_BN_array2.length - (short) x_pow_2.length);
                } else {
                    xOffset = 0;
                }
                Util.arrayCopyNonAtomic(x_pow_2, (short) 0, helper_BN_array2, xOffset, (short) x_pow_2.length);
            }
            // ((x+y)^2) - x^2
            subtract(helper_BN_array1, (short) 0, (short) helper_BN_array1.length, helper_BN_array2, (short) 0, (short) helper_BN_array2.length);

            // y^2
            if (y_pow_2 == null) {
                // y^2 is not precomputed
                Util.arrayFillNonAtomic(helper_BN_array2, (short) 0, (short) helper_BN_array2.length, (byte) 0);
                yOffset = (short) (helper_BN_array2.length - y.length());
                Util.arrayCopyNonAtomic(y.value, (short) 0, helper_BN_array2, yOffset, y.length());
                fnc_mult_cipher.doFinal(helper_BN_array2, (byte) 0, (short) helper_BN_array2.length, helper_BN_array2, (short) 0);
            } else {
                // y^2 is precomputed
                if ((short) y_pow_2.length != (short) helper_BN_array2.length) {
                    Util.arrayFillNonAtomic(helper_BN_array2, (short) 0, (short) helper_BN_array2.length, (byte) 0);
                    yOffset = (short) ((short) helper_BN_array2.length - (short) y_pow_2.length);
                } else {
                    yOffset = 0;
                }
                Util.arrayCopyNonAtomic(y_pow_2, (short) 0, helper_BN_array2, yOffset, (short) y_pow_2.length);
            }


            // {(x+y)^2) - x^2} - y^2
            subtract(helper_BN_array1, (short) 0, (short) helper_BN_array1.length, helper_BN_array2, (short) 0, (short) helper_BN_array2.length);

            // we now have 2xy in mult_resultArray, divide it by 2 => shift by one bit and fill back into this
            short multOffset = (short) ((short) helper_BN_array1.length - 1);
            short res = 0;
            short res2 = 0;
            // this.length() must be different from multOffset, set proper ending condition
            short stopOffset = 0;

            if (this.length() > multOffset)
                stopOffset = (short) (this.length() - multOffset); // only part of this.value will be filled
            else
                stopOffset = 0; // whole this.value will be filled

            if (stopOffset > 0)
                Util.arrayFillNonAtomic(this.value, (short) 0, stopOffset, (byte) 0);

            for (short i = (short) (this.length() - 1); i >= stopOffset; i--) {
                res = (short) (helper_BN_array1[multOffset] & 0xff);
                res = (short) (res >> 1);
                res2 = (short) (helper_BN_array1[(short) (multOffset - 1)] & 0xff);
                res2 = (short) (res2 << 7);
                this.value[i] = (byte) (short) (res | res2);
                multOffset--;
            }
        }

        /**
         * Computes and stores modulo of this bignat. 
         * @param modulo value of modulo
         */
        private void mod(Bignat modulo) 
        {
            this.remainder_divide(modulo, null);
            // NOTE: attempt made to utilize crypto co-processor in pow2Mod_RSATrick_worksOnlyAbout30pp, but doesn't work for all inputs 
        }

        /**
         * Multiplication of Bignats x and y computed by modulo {@code modulo}. 
         * The result is stored to this.
         * @param x first value to multiply
         * @param y second value to multiply
         * @param modulo value of modulo
         */
        void mod_mult(Bignat x, Bignat y, Bignat modulo) 
        {
            helper_BN_A.resize_to_max(false);
            // Perform fast multiplication using RSA trick
            helper_BN_A.mult(x, y);
            // Compute modulo 
            helper_BN_A.mod(modulo);
            helper_BN_A.shrink();
            this.clone(helper_BN_A);
        }
        // Potential speedup for  modular multiplication
        // Binomial theorem: (op1 + op2)^2 - (op1 - op2)^2 = 4 * op1 * op2 mod (mod)

        /**
         * Computes inversion of this bignat taken modulo {@code modulo}. 
         * The result is stored into this.
         * @param modulo value of modulo
         */
        private void mod_inv(Bignat modulo) 
        {
            helper_BN_B.clone(modulo);
            helper_BN_B.decrement_one();
            helper_BN_B.decrement_one();

            mod_exp(helper_BN_B, modulo);
        }

        /**
         * Computes {@code res := this ** exponent mod modulo} and store results into this. 
         * Uses RSA engine to quickly compute this^exponent % modulo
         * @param exponent value of exponent
         * @param modulo value of modulo
         */
        private void mod_exp(Bignat exponent, Bignat modulo) 
        {
            short tmp_size = (short)(MODULO_RSA_ENGINE_MAX_LENGTH_BITS / 8);
            helper_BN_F.setSize(tmp_size);

            short len = n_mod_exp(tmp_size, this, exponent.asByteArray(), exponent.length(), modulo, helper_BN_F.value, (short) 0);
            if (len != tmp_size)
                ISOException.throwIt(SW_ECPOINT_UNEXPECTED_KA_LEN);

            helper_BN_F.mod(modulo);
            helper_BN_F.shrink();
            this.clone(helper_BN_F);
        }

        private void mod_exp2(Bignat modulo) 
        {
            mod_exp(TWO, modulo);
        }

        /**
         * Calculates {@code res := base ** exp mod mod} using RSA engine. 
         * Requirements:
         * 1. Modulo must be either 521, 1024, 2048 or other lengths supported by RSA (see appendzeros() and mod() method)
         * 2. Base must have the same size as modulo (see prependzeros())
         * @param baseLen   length of base rounded to size of RSA engine
         * @param base      value of base (if size is not equal to baseLen then zeroes are appended)
         * @param exponent  array with exponent
         * @param exponentLen length of exponent
         * @param modulo    value of modulo 
         * @param resultArray array for the computed result
         * @param resultOffset start offset of resultArray
         */
        private short n_mod_exp(short baseLen, Bignat base, byte[] exponent, short exponentLen, Bignat modulo, byte[] resultArray, short resultOffset) 
        {
            // Verify if pre-allocated engine match the required values
            if (fnc_NmodE_pubKey.getSize() < (short) (modulo.length() * 8)) // attempt to perform modulo with higher or smaller than supported length - try change constant MODULO_ENGINE_MAX_LENGTH
                ISOException.throwIt(SW_BIGNAT_MODULOTOOLARGE);

            if (fnc_NmodE_pubKey.getSize() < (short) (base.length() * 8))
                ISOException.throwIt(SW_BIGNAT_MODULOTOOLARGE);

            // Potential problem: we are changing key value for publicKey already used before with occ.bnHelper.modCipher. 
            // Simulator and potentially some cards fail to initialize this new value properly (probably assuming that same key object will always have same value)
            // Fix (if problem occure): generate new key object: RSAPublicKey publicKey = (RSAPublicKey) KeyBuilder.buildKey(KeyBuilder.TYPE_RSA_PUBLIC, (short) (baseLen * 8), false);

            fnc_NmodE_pubKey.setExponent(exponent, (short) 0, exponentLen);
            modulo.append_zeros(baseLen, helper_BN_array1, (short) 0);
            fnc_NmodE_pubKey.setModulus(helper_BN_array1, (short) 0, baseLen);
            fnc_NmodE_cipher.init(fnc_NmodE_pubKey, Cipher.MODE_DECRYPT);     
            base.prepend_zeros(baseLen, helper_BN_array1, (short) 0);
            // BUGBUG: Check if input is not all zeroes (causes out-of-bound exception on some cards)
            return fnc_NmodE_cipher.doFinal(helper_BN_array1, (short) 0, baseLen, resultArray, resultOffset);
        }

        /**
         * Negate current Bignat modulo provided modulus
         *
         * @param mod value of modulus
         */
        private void mod_negate(Bignat mod) 
        {
            helper_BN_B.setSize(mod.length());
            helper_BN_B.copy(mod); //-y=mod-y

            if (this.lesser(mod)) { // y<mod
                helper_BN_B.subtract(this);//-y=mod-y
                this.copy(helper_BN_B);
            } else {// y>=mod
                this.mod(mod);//-y=y-mod
                helper_BN_B.subtract(this);
                this.copy(helper_BN_B);
            }
        }

        /**
         * Set content of Bignat internal array
         *
         * @param from_array_length available data in {@code from_array}
         * @param this_offset offset where data should be stored
         * @param from_array data array to deserialize from
         * @param from_array_offset offset in {@code from_array}
         * @return the number of shorts actually read, except for the case where
         * deserialization finished by reading precisely {@code len} shorts, in this
         * case {@code len + 1} is returned.
         */
        private short from_byte_array(short from_array_length, short this_offset, byte[] from_array, short from_array_offset) 
        {
            short max = (short) (this_offset + from_array_length) <= this.size ? from_array_length : (short) (this.size - this_offset);
            Util.arrayCopyNonAtomic(from_array, from_array_offset, value, this_offset, max);
            if ((short) (this_offset + from_array_length) == this.size) {
                return (short) (from_array_length + 1);
            } else {
                return max;
            }
        }
    }

    static class ECCurve
    {
        private final short KEY_LENGTH; //Bits
        private final short POINT_SIZE; //Bytes
        private final short COORD_SIZE; //Bytes

        //Parameters
        private byte[] p = null;
        private byte[] a = null;
        private byte[] b = null;
        private byte[] G = null;
        private byte[] r = null;

        private Bignat pBN;
        private Bignat aBN;

        private KeyPair disposable_pair;
        private ECPrivateKey disposable_priv;

        /**
         * Creates new curve object from provided parameters. Either copy of provided
         * arrays is performed (bCopyArgs == true, input arrays can be reused later for other
         * purposes) or arguments are directly stored (bCopyArgs == false, usable for fixed static arrays) .
         * @param bCopyArgs if true, copy of arguments is created, otherwise reference is directly stored
         * @param p_arr array with p
         * @param a_arr array with a
         * @param b_arr array with b
         * @param G_arr array with base point G
         * @param r_arr array with r
         */
        ECCurve(boolean bCopyArgs, byte[] p_arr, byte[] a_arr, byte[] b_arr, byte[] G_arr, byte[] r_arr) 
        {
            //ECCurve_initialize(p_arr, a_arr, b_arr, G_arr, r_arr);
            this.KEY_LENGTH = (short) (p_arr.length * 8);
            this.POINT_SIZE = (short) G_arr.length;
            this.COORD_SIZE = (short) ((short) (G_arr.length - 1) / 2);

            if (bCopyArgs)
            {
                // Copy curve parameters into newly allocated arrays in EEPROM (will be only read, not written later => good performance even when in EEPROM)
                this.p = new byte[(short) p_arr.length];
                this.a = new byte[(short) a_arr.length];
                this.b = new byte[(short) b_arr.length];
                this.G = new byte[(short) G_arr.length];
                this.r = new byte[(short) r_arr.length];

                Util.arrayCopyNonAtomic(p_arr, (short) 0, p, (short) 0, (short) p.length);
                Util.arrayCopyNonAtomic(a_arr, (short) 0, a, (short) 0, (short) a.length);
                Util.arrayCopyNonAtomic(b_arr, (short) 0, b, (short) 0, (short) b.length);
                Util.arrayCopyNonAtomic(G_arr, (short) 0, G, (short) 0, (short) G.length);
                Util.arrayCopyNonAtomic(r_arr, (short) 0, r, (short) 0, (short) r.length);
            }
            else
            {
                // No allocation, store directly provided arrays 
                this.p = p_arr;
                this.a = a_arr;
                this.b = b_arr;
                this.G = G_arr;
                this.r = r_arr;
            }

            // We will not modify values of p/a/b during the lifetime of curve => allocate helper bignats directly from the array
            // Additionally, these Bignats will be only read from so Bignat_Helper can be null (saving need to pass as argument to ECCurve)
            this.pBN = new Bignat(this.p);
            this.aBN = new Bignat(this.a);

            this.disposable_pair = this.newKeyPair(null);
            this.disposable_priv = (ECPrivateKey) this.disposable_pair.getPrivate();
        }

        /**
         * Creates a new keyPair based on this curve parameters. KeyPair object is reused if provided. Fresh keyPair value is generated.
         * @param existingKeyPair existing KeyPair object which is reused if required. If null, new KeyPair is allocated
         * @return new or existing object with fresh key pair value
         */
        private KeyPair newKeyPair(KeyPair existingKeyPair) 
        {
            ECPrivateKey privKey;
            ECPublicKey pubKey;
            if (existingKeyPair == null) // Allocate if not supplied
                existingKeyPair = new KeyPair(KeyPair.ALG_EC_FP, KEY_LENGTH);

            // Some implementation will not return valid pub key until ecKeyPair.genKeyPair() is called
            // Other implementation will fail with exception if same is called => try catch and drop any exception 
            try 
            {
                pubKey = (ECPublicKey) existingKeyPair.getPublic();
                if (pubKey == null)
                    existingKeyPair.genKeyPair();
            } catch (Exception e) {} //Intentionally do nothing

            privKey = (ECPrivateKey) existingKeyPair.getPrivate();
            pubKey = (ECPublicKey) existingKeyPair.getPublic();

            //Set required values
            privKey.setFieldFP(p, (short) 0, (short) p.length);
            privKey.setA(a, (short) 0, (short) a.length);
            privKey.setB(b, (short) 0, (short) b.length);
            privKey.setG(G, (short) 0, (short) G.length);
            privKey.setR(r, (short) 0, (short) r.length);
            privKey.setK((short) 1);

            pubKey.setFieldFP(p, (short) 0, (short) p.length);
            pubKey.setA(a, (short) 0, (short) a.length);
            pubKey.setB(b, (short) 0, (short) b.length);
            pubKey.setG(G, (short) 0, (short) G.length);
            pubKey.setR(r, (short) 0, (short) r.length);
            pubKey.setK((short) 1);

            existingKeyPair.genKeyPair();

            return existingKeyPair;
        }
    }

    /**
     * 
     * @author Vasilios Mavroudis and Petr Svenda
     */
    static class ECPoint
    {
        private ECPublicKey         thePoint;
        private KeyPair             thePointKeyPair;
        private final ECCurve       theCurve;

        /**
         * Creates new ECPoint object for provided {@code curve}. Random initial point value is generated. 
         * The point will use helper structures from provided ECPoint_Helper object.
         * @param curve point's elliptic curve
         * @param ech object with preallocated helper objects and memory arrays
         */
        ECPoint(ECCurve curve) 
        {
            this.theCurve = curve;      
            this.thePointKeyPair = this.theCurve.newKeyPair(this.thePointKeyPair);
            this.thePoint = (ECPublicKey) thePointKeyPair.getPublic();
        }

        /**
         * Set this point value (parameter W) from array with value encoded as per ANSI X9.62. 
         * The uncompressed form is always supported. If underlying native JavaCard implementation 
         * of {@code ECPublickKey} supports compressed points, then this method accepts also compressed points. 
         * @param buffer array with serialized point
         * @param offset start offset within input array
         * @param length length of point
         */
        void setW(byte[] buffer, short offset, short length) 
        {
            this.thePoint.setW(buffer, offset, length);
        }

        /**
         * Returns current value of this point. 
         * @param buffer    memory array where to store serailized point value
         * @param offset    start offset for output serialized point    
         * @return length of serialized point (number of bytes)
         */
        short getW(byte[] buffer, short offset) 
        {
            return thePoint.getW(buffer, offset);
        }

        /**
         * Adds this (P) and provided (Q) point. Stores a resulting value into this point.
         * @param other point to be added to this.
         */
        void add(ECPoint other) 
        {
            this.thePoint.getW(uncompressed_point_arr1, (short) 0);
            helperEC_BN_D.setSize(this.theCurve.COORD_SIZE);
            helperEC_BN_D.from_byte_array(this.theCurve.COORD_SIZE, (short) 0, uncompressed_point_arr1, (short) 1);
            helperEC_BN_E.setSize(this.theCurve.COORD_SIZE);
            helperEC_BN_E.from_byte_array(this.theCurve.COORD_SIZE, (short) 0, uncompressed_point_arr1, (short) (1 + this.theCurve.COORD_SIZE));

            // l = (y_q-y_p)/(x_q-x_p)
            // x_r = l^2 - x_p - x_q
            // y_r = l(x_p-x_r) - y_p

            // P+Q=R
            if (this == other) {
                //lambda = (3(x_p^2)+a)/(2y_p)
                //(3(x_p^2)+a)
                helperEC_BN_B.clone(helperEC_BN_D);
                helperEC_BN_B.mod_exp(TWO, this.theCurve.pBN);
                helperEC_BN_B.mod_mult(helperEC_BN_B, THREE, this.theCurve.pBN);
                helperEC_BN_B.mod_add(this.theCurve.aBN, this.theCurve.pBN);
                // (2y_p)
                helperEC_BN_C.clone(helperEC_BN_E);
                helperEC_BN_C.mod_mult(helperEC_BN_E, TWO, this.theCurve.pBN);
                helperEC_BN_C.mod_inv(this.theCurve.pBN);

            } else {
                // lambda=(y_q-y_p)/(x_q-x_p) mod p
                other.thePoint.getW(uncompressed_point_arr1, (short) 0);
                helperEC_BN_F.setSize(this.theCurve.COORD_SIZE);
                helperEC_BN_F.from_byte_array(other.theCurve.COORD_SIZE, (short) 0, uncompressed_point_arr1, (short) 1);
                helperEC_BN_B.setSize(this.theCurve.COORD_SIZE);
                helperEC_BN_B.from_byte_array(this.theCurve.COORD_SIZE, (short) 0, uncompressed_point_arr1, (short) (1 + this.theCurve.COORD_SIZE));

                helperEC_BN_B.mod(this.theCurve.pBN);

                helperEC_BN_B.mod_sub(helperEC_BN_E, this.theCurve.pBN);

                // (x_q-x_p)
                helperEC_BN_C.clone(helperEC_BN_F);
                helperEC_BN_C.mod(this.theCurve.pBN);
                helperEC_BN_C.mod_sub(helperEC_BN_D, this.theCurve.pBN);
                helperEC_BN_C.mod_inv(this.theCurve.pBN);
            }

            helperEC_BN_A.resize_to_max(false);
            helperEC_BN_A.zero();
            helperEC_BN_A.mod_mult(helperEC_BN_B, helperEC_BN_C, this.theCurve.pBN);

            // (x_p,y_p)+(x_q,y_q)=(x_r,y_r)
            // lambda=(y_q-y_p)/(x_q-x_p)

            //x_r=lambda^2-x_p-x_q
            if (this == other)
            {
                short len = this.multiplication_x_KA(TWO, helperEC_BN_B.asByteArray(), (short) 0);
                helperEC_BN_B.setSize(len);
            }
            else
            {
                helperEC_BN_B.clone(helperEC_BN_A);
                //m_occ.ecHelper.fnc_add_x_r.mod_exp(occ.bnHelper.TWO, this.TheCurve.pBN);
                helperEC_BN_B.mod_exp2(this.theCurve.pBN);
                helperEC_BN_B.mod_sub(helperEC_BN_D, this.theCurve.pBN);
                helperEC_BN_B.mod_sub(helperEC_BN_F, this.theCurve.pBN);
            }

            //y_r=lambda(x_p-x_r)-y_p
            helperEC_BN_C.clone(helperEC_BN_D);
            helperEC_BN_C.mod_sub(helperEC_BN_B, this.theCurve.pBN);
            helperEC_BN_C.mod_mult(helperEC_BN_C, helperEC_BN_A, this.theCurve.pBN);
            helperEC_BN_C.mod_sub(helperEC_BN_E, this.theCurve.pBN);

            uncompressed_point_arr1[0] = (byte)0x04;
            // If x_r.length() and y_r.length() is smaller than this.TheCurve.COORD_SIZE due to leading zeroes which were shrinked before, then we must add these back
            helperEC_BN_B.prepend_zeros(this.theCurve.COORD_SIZE, uncompressed_point_arr1, (short) 1);
            helperEC_BN_C.prepend_zeros(this.theCurve.COORD_SIZE, uncompressed_point_arr1, (short) (1 + this.theCurve.COORD_SIZE));
            this.setW(uncompressed_point_arr1, (short) 0, this.theCurve.POINT_SIZE);
        }

        /**
         * Multiplies this point value with provided scalar and stores result into
         * provided array. No modification of this point is performed.
         * Native KeyAgreement engine is used.
         *
         * @param scalar value of scalar for multiplication
         * @param outBuffer output array for resulting value
         * @param outBufferOffset offset within output array
         * @return length of resulting value (in bytes)
         */
        private short multiplication_x_KA(Bignat scalar, byte[] outBuffer, short outBufferOffset) 
        {
            // NOTE: potential problem on real cards (j2e) - when small scalar is used (e.g., Bignat.TWO), operation sometimes freezes
            theCurve.disposable_priv.setS(scalar.asByteArray(), (short) 0, scalar.length());

            fnc_multiplication_x_keyAgreement.init(theCurve.disposable_priv);

            short len = this.getW(uncompressed_point_arr1, (short) 0); 
            len = fnc_multiplication_x_keyAgreement.generateSecret(uncompressed_point_arr1, (short) 0, len, outBuffer, outBufferOffset);
            // Return always length of whole coordinate X instead of len - some real cards returns shorter value equal to SHA-1 output size although PLAIN results is filled into buffer (GD60) 
            return this.theCurve.COORD_SIZE;
        }

        //Computes negation of the point
        void negate() 
        {
            //Operation will dump point into uncompressed_point_arr, negate Y and restore back
            thePoint.getW(uncompressed_point_arr1, (short) 0);
            helperEC_BN_C.setSize(this.theCurve.COORD_SIZE);
            helperEC_BN_C.from_byte_array(this.theCurve.COORD_SIZE, (short) 0, uncompressed_point_arr1, (short) (1 + this.theCurve.COORD_SIZE));
            helperEC_BN_C.mod_negate(this.theCurve.pBN);

            //Restore whole point back
            helperEC_BN_C.prepend_zeros(this.theCurve.COORD_SIZE, uncompressed_point_arr1, (short) (1 + this.theCurve.COORD_SIZE));
            this.setW(uncompressed_point_arr1, (short) 0, this.theCurve.POINT_SIZE);
        }
    }

    //Custom error response codes
    private static final short SW_BIGNAT_RESIZETOLONGER          = (short) 0x7000;
    private static final short SW_BIGNAT_REALLOCATIONNOTALLOWED  = (short) 0x7001;
    private static final short SW_BIGNAT_MODULOTOOLARGE          = (short) 0x7002;
    private static final short SW_BIGNAT_INVALIDCOPYOTHER        = (short) 0x7003;
    private static final short SW_BIGNAT_INVALIDRESIZE           = (short) 0x7004;
    private static final short SW_ECPOINT_UNEXPECTED_KA_LEN      = (short) 0x700b;
}