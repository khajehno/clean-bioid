package applet;

import applet.JCMathLite.Bignat;
import applet.JCMathLite.ECCurve;
import applet.JCMathLite.ECPoint;
import javacard.framework.APDU;
import javacard.framework.ISO7816;
import javacard.framework.ISOException;
import javacard.framework.JCSystem;
import javacard.framework.Util;
import javacard.security.AESKey;
import javacard.security.CryptoException;
import javacard.security.ECKey;
import javacard.security.ECPrivateKey;
import javacard.security.ECPublicKey;
import javacard.security.KeyAgreement;
import javacard.security.KeyBuilder;
import javacard.security.KeyPair;
import javacard.security.MessageDigest;
import javacard.security.RandomData;
import javacardx.crypto.Cipher;

/**
 * HOLY GRAIL : 
 * https://docs.oracle.com/javacard/3.0.5/api/javacard/security/KeyAgreement.html#ALG_EC_PACE_GM for point addition
 * https://docs.oracle.com/javacard/3.0.5/api/javacard/security/KeyAgreement.html#ALG_EC_SVDP_DH_PLAIN_XY for point multiplication with XY returned
 * but we need different cards for this !
 */

/**
 * BioID Card Applet
 * @author Julien Corsin SC-MA1 @ LASEC EPFL
 * @author Betül Durak PhD @ LASEC EPFL
 */
public class BioID extends javacard.framework.Applet {

    //Weak Access Control instructions
    private static final byte INS_SEND_M = (byte) 0xA6; //Send M
    private static final byte INS_REC_KL = (byte) 0xA7; //Receive L and Kverify

    //Confirmer Data/Chip Authentication instructions
    private static final byte INS_REC_NT_SEND_NC = (byte) 0xA8;

    //Secure Channel Establishment instructions
    private static final byte INS_REC_PUB = (byte) 0xB1; //Receive public key of the terminal
    private static final byte INS_GEN_PAIR = (byte) 0xB2; //Generate key pair on the chip
    private static final byte INS_GEN_SECRET = (byte) 0xB3; //Generate shared secret
    private static final byte INS_REC_VERIFY = (byte) 0xB4; //Verify

    //Data/Chip Authentication instructions
    private static final byte INS_SEND_PK_ISSUER = (byte) 0xC0;
    private static final byte INS_REC_H_SEND_U = (byte) 0xC1; //Receive h and send U
    private static final byte INS_SEND_R = (byte) 0xC2; //Send R
    private static final byte INS_REC_v = (byte) 0xC3; //Receive v
    private static final byte INS_REC_r = (byte) 0xC4; //Receive r (R and r are different).
    private static final byte INS_SEND_NEW_s = (byte) 0xC5; //Send s'

    //Enrollment instructions
    private static final byte INS_REQ_DATASIZE = (byte) 0xD4;
    private static final byte INS_SEND_EEPROM = (byte) 0xD5;
    private static final byte INS_REQUEST_STORAGE = (byte) 0xD6;
    private static final byte INS_STOREIN_EEPROM = (byte) 0xD7; 
    private static final byte INS_DELETE_FROM_EEPROM = (byte) 0xD8;

    //Enrollment Constants
    private static final short MAX_BIODATA_TYPES = (short) 10; //signature, PK_term, PK_issuer, fingervein, personal data, password, MAC key
    private static final short INDEX_SIG = (short) 0;
    private static final short INDEX_PKterm = (short) 1;
    private static final short INDEX_PKissuer = (short) 2;
    private static final short INDEX_Bio = (short) 3;
    private static final short INDEX_DG2 = (short) 4;
    private static final short INDEX_Kchip = (short) 5;
    private static final short INDEX_PWDG = (short) 6;

    //Generic variables
    private byte[] bufferTemp = JCSystem.makeTransientByteArray(MAX_BUFFER_BYTES , JCSystem.CLEAR_ON_RESET); // temporary array for general use new byte[MAX_BUFFER_BYTES];
    private byte[] baTemp = JCSystem.makeTransientByteArray((short)256 , JCSystem.CLEAR_ON_RESET); // temporary array for general use 

    private byte lastInst = (byte) 0xAA;
    private byte protocolFlag = (byte) 0x00;
    private Object[] eepromData = new Object[MAX_BIODATA_TYPES];
    private boolean[] eepromDataReady = new boolean[MAX_BIODATA_TYPES];
    private static final short MAX_BUFFER_BYTES = (short) 240;

    //SAC variables
    private short len; // stores array lengths
    private byte[] kEnc = JCSystem.makeTransientByteArray((short)16 , JCSystem.CLEAR_ON_RESET);; // encryption key array
    private byte[] kVerify = JCSystem.makeTransientByteArray((short)32 , JCSystem.CLEAR_ON_RESET); // verification key array
    private KeyPair kpC = new KeyPair(KeyPair.ALG_EC_FP, KeyBuilder.LENGTH_EC_FP_256); // generate KeyPair object

    private ECPrivateKey privKeyC;
    private ECPublicKey pubKeyC; 
    private ECPublicKey pubKeyT = (ECPublicKey) KeyBuilder.buildKey(KeyBuilder.TYPE_EC_FP_PUBLIC, KeyBuilder.LENGTH_EC_FP_256, false);  ;
    private KeyAgreement ecdhC = KeyAgreement.getInstance(KeyAgreement.ALG_EC_SVDP_DH, false); // create KeyAgreement object using ECDH;
    private MessageDigest md = MessageDigest.getInstance(MessageDigest.ALG_SHA_256,false); // create message digest object
    private Cipher aesCipher = Cipher.getInstance(Cipher.ALG_AES_BLOCK_128_CBC_NOPAD, false);;
    private AESKey aesKey = (AESKey) KeyBuilder.buildKey(KeyBuilder.TYPE_AES_TRANSIENT_DESELECT, KeyBuilder.LENGTH_AES_128, false);
    private static final byte[] IV = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    private Bignat ord = new Bignat(q); // Bignat that stores the curve order.

    //WAC variables
    private ECCurve secp256r1 = new ECCurve(false, p, a, b, G, q); //Beautiful Curve
    private ECPoint P1 = new ECPoint(secp256r1); //Placeholder for A, then L, then K 
    private ECPoint P2 = new ECPoint(secp256r1); //Placeholder for M, then -pwd*G3
    private ECPrivateKey kPriv;
    private ECPublicKey kPub;
    private byte[] hash = JCSystem.makeTransientByteArray(MessageDigest.LENGTH_SHA_256, JCSystem.CLEAR_ON_RESET);
    private byte[] kEncWAC = JCSystem.makeTransientByteArray((short)16 , JCSystem.CLEAR_ON_RESET); // encryption key array
    private byte[] NtT = JCSystem.makeTransientByteArray((short)40 , JCSystem.CLEAR_ON_RESET);
    private RandomData rand = RandomData.getInstance(RandomData.ALG_SECURE_RANDOM);

    //DCA variables
    private byte[] h = JCSystem.makeTransientByteArray((short)32 , JCSystem.CLEAR_ON_RESET);
    private byte[] v = JCSystem.makeTransientByteArray((short)32 , JCSystem.CLEAR_ON_RESET);
    private byte[] r = JCSystem.makeTransientByteArray((short)32 , JCSystem.CLEAR_ON_RESET);
    private byte[] temp = JCSystem.makeTransientByteArray((short)32 , JCSystem.CLEAR_ON_RESET); // temporary array for creating empty bignats
    private byte[] temp2 = JCSystem.makeTransientByteArray((short) 32, JCSystem.CLEAR_ON_RESET); // temporary array for creating empty bignats
    private byte[] temp3 = JCSystem.makeTransientByteArray((short) 32, JCSystem.CLEAR_ON_RESET); // temporary array for creating empty bignats

    //Applet Install method
    public static void install(byte[] bArray, short bOffset, byte bLength) 
    {
        JCMathLite.initlib();
        new BioID().register(bArray, (short) (bOffset + 1), bArray[bOffset]);
    }

    @Override
    public void process(APDU apdu) throws ISOException 
    {
        byte[] buffer = apdu.getBuffer();

        if(selectingApplet())
            return;

        if(buffer[ISO7816.OFFSET_CLA] != (byte) 0x00)
            ISOException.throwIt((short) 0xF000); // Wrong command class

        switch(buffer[ISO7816.OFFSET_INS])
        {
            //Weak Access Control
            case INS_SEND_M:
                send_M(apdu);
                return;

            case INS_REC_KL:
                if(lastInst != INS_SEND_M)
                    ISOException.throwIt((short) 0xF101);
                rec_LK(apdu);
                return;

            case INS_REC_NT_SEND_NC:
                if(lastInst != INS_SEND_EEPROM)
                    ISOException.throwIt((short) 0xF101);
                confDataAuth(apdu);
                return;


                //Strong Access Control and Secure Channel Establishment
            case INS_REC_PUB:
                procRecPub(apdu);
                return;

            case INS_GEN_PAIR:
                if(lastInst != INS_REC_PUB)
                    ISOException.throwIt((short) 0xF101);
                procGenKeyPair(apdu);
                return;

            case INS_GEN_SECRET:
                if(lastInst != INS_GEN_PAIR)
                    ISOException.throwIt((short) 0xF101);
                procGenSharedSecret(apdu);
                return;

            case INS_REC_VERIFY:
                if(lastInst != INS_GEN_SECRET)
                    ISOException.throwIt((short) 0xF101);
                procVerify(apdu);
                return;


                //Data Authentication
            case INS_SEND_PK_ISSUER:
                if(lastInst != INS_SEND_EEPROM)
                    ISOException.throwIt((short) 0xF101);
                send_pk_issuer(apdu);
                return;

            case INS_REC_H_SEND_U:
                if(lastInst != INS_SEND_PK_ISSUER)
                    ISOException.throwIt((short) 0xF101);
                rec_h_send_U(apdu);
                return;

            case INS_SEND_R:
                if(lastInst != INS_REC_H_SEND_U)
                    ISOException.throwIt((short) 0xF101);
                send_R(apdu);
                return;

            case INS_REC_v:
                if(lastInst != INS_SEND_R)
                    ISOException.throwIt((short) 0xF101);
                rec_v(apdu);
                return;

            case INS_REC_r:
                if(lastInst != INS_REC_v)
                    ISOException.throwIt((short) 0xF101);
                rec_r(apdu);
                return;

            case INS_SEND_NEW_s:
                if(lastInst != INS_REC_r)
                    ISOException.throwIt((short) 0xF101);
                send_new_s(apdu);
                return;


                //Load Biometry
            case INS_REQUEST_STORAGE:
                requestBioDataStorage(apdu);
                return;

            case INS_STOREIN_EEPROM:
                storeBioDataInEEPROM(apdu);
                return;

            case INS_REQ_DATASIZE:
                if(lastInst != INS_REC_VERIFY && lastInst != INS_SEND_EEPROM && lastInst != INS_REC_KL)
                    ISOException.throwIt((short) 0xF101);
                sendBioDataSize(apdu);
                return;

            case INS_SEND_EEPROM:
                if(lastInst != INS_REQ_DATASIZE && lastInst != INS_SEND_EEPROM)
                    ISOException.throwIt((short) 0xF101);
                sendEEPROMBioData(apdu);
                return;

            case INS_DELETE_FROM_EEPROM:
                deleteEEPROMdata(apdu);
                return;

            default:
                ISOException.throwIt((short) 0xF100);
        }
    }

    //===========================================================================================================================================================
    //= WEAK ACCESS CONTROL =====================================================================================================================================
    //===========================================================================================================================================================

    //Generate Key Pair for the chip and compute then send M to terminal
    private void send_M(APDU apdu)
    {   
        setCurveParams(kpC);
        kpC.genKeyPair();
        kPriv = (ECPrivateKey) kpC.getPrivate(); // generates a
        kPub = (ECPublicKey) kpC.getPublic(); // generates A = aG
        kPub.getW(baTemp, (short) 32); // copy A into baTem starting from offset 32. It has 65 bytes.
        P1.setW(baTemp, (short)32, (short)65); //A
        Util.arrayCopyNonAtomic((byte[]) eepromData[INDEX_PWDG], (short) 0, bufferTemp, (short) 0, (short) 65);
        P2.setW(bufferTemp, (short)0, (short)65);
        P2.add(P1); // M = A + pwd*G2
        P2.getW(baTemp, (short)0); // Store M into baTemp in raw form by overwriting the private and public keys a and A!
        sendByteArray(apdu, baTemp, (short) 65); //Send M to the terminal
        lastInst = INS_SEND_M;
    }

    //Receive L and Kverify, parse them, and check equality with own Kverify
    private void rec_LK(APDU apdu) 
    {
        recByteArray(apdu); //Received bytes are placed in baTemp

        //We expect 32 bytes of kVerify and 65 bytes of rawL in baTemp
        //Getting rawL from baTemp
        P1.setW(baTemp, (short)32, (short)65); //L
        Util.arrayCopyNonAtomic((byte[]) eepromData[INDEX_PWDG], (short) 65, bufferTemp, (short) 0, (short) 65);            
        P2.setW(bufferTemp, (short)0, (short)65);
        P2.negate(); //-pwd*G3
        P1.add(P2); //L - pwd*G3
        P1.getW(baTemp, (short)32); //Storing (L - pwd*G3) temporarily in baTemp starting from 32 and its size is 65. It overwrites L.
        // Here we generate a public key a*(L-pwd*G3) and it has 32 bytes.
        // Instead we can just compute the hash of it which is 20 bytes
        ecdhC.init(kPriv);
        short lenK = ecdhC.generateSecret(baTemp, (short)32, (short)65, baTemp, (short) 97); //a(L-pwd*G3) //baTemp[97..116] NOW HOLDS rawxK in length lenK (i.e. hash of it)
        /** KDF input array
         K = SHA-1(shared secret)
         i = KDF parameter
         KDF_i(K) = SHA-256(K|X|i), len(K) + len(i) = 20 + 1 = 21 bytes.
         **/
        baTemp[(short) (lenK + (short) 97)]= (byte) 7;
        md.doFinal(baTemp, (short)97, (short) (lenK+1), hash, (short)0); //hash = KDF7(rawxK)
        //baTemp HOLDS kverifyT
        
        //Abort if kverifyC doesn't match the terminal's hash kverify
        if(Util.arrayCompare(hash, (short)0, baTemp, (short)0, (short)32) != 0)
            ISOException.throwIt((short)0xF0B4);

        md.reset();
        baTemp[(short) (lenK + (short) 97)] = (byte) 8;
        md.doFinal(baTemp, (short)97, (short) (lenK+1), hash, (short)0); //hash = KDF8(rawxK)
        Util.arrayCopyNonAtomic(hash, (short) 0, kEncWAC, (short)0, (short) kEncWAC.length);
        md.reset();
        lastInst = INS_REC_KL;
        protocolFlag = INS_REC_KL;
    }

    //Confirm authenticity of data by sending NC
    private void confDataAuth(APDU apdu)
    {
        // baTemp keeps N_t and time = 40 bytes
        decryptReceivedAPDU(apdu, kEncWAC, NtT, (short) 40);    
        // We compute 32 bytes of m and store it in temp
        len = (short) ( (byte[]) eepromData[INDEX_DG2]).length;
        Util.arrayCopyNonAtomic( (byte[]) eepromData[INDEX_DG2], (short)0, baTemp, (short)0, len);
        baTemp[len] = (byte) 6;
        md.doFinal(baTemp, (short)0, (short) (len+1), temp, (short)0);
        md.reset();
        rand.generateData(baTemp, (short)0, (short)32); //N_c stored in baTemp
        // copy everything to baTemp N_c || N_t || time|| m = 32 bytes + 8 bytes + 32 bytes + 32 bytes = 104 bytes
        Util.arrayCopyNonAtomic(NtT, (short)0, baTemp, (short)32, (short)NtT.length);
        Util.arrayCopyNonAtomic(temp, (short)0, baTemp, (short) (32+NtT.length), (short)temp.length);
        Util.arrayCopyNonAtomic((byte[]) eepromData[INDEX_Kchip], (short)0, baTemp, (short)(32 + NtT.length + temp.length), (short)((byte[]) eepromData[INDEX_Kchip]).length); 
        len = (short) (32 + NtT.length + temp.length+((byte[]) eepromData[INDEX_Kchip]).length);
        baTemp[len] = (byte) 8;
        md.doFinal(baTemp, (short)0, (short)(len+1), hash, (short)0);
        md.reset();
        Util.arrayCopyNonAtomic(hash, (short)0, baTemp, (short)32, (short)hash.length); // to send N_c || hash
        sendEncryptedByteArray(apdu, kEncWAC, baTemp, (short) (32+hash.length));        
        lastInst = INS_REC_NT_SEND_NC;
    }

    //===========================================================================================================================================================
    //= STRONG ACCESS CONTROL AND SECURE CHANNEL ESTABLISHMENT===================================================================================================
    //===========================================================================================================================================================

    //Receive the public key of the terminal and store it in a java PublicKey object
    private void procRecPub(APDU apdu) 
    {
        short offset = recByteArray(apdu); //Received bytes are placed into baTemp

        if(Util.arrayCompare((byte[]) eepromData[INDEX_PKterm], (short) 0, baTemp, (short) 0, (short) ((byte[]) eepromData[INDEX_PKterm]).length) != 0)
            ISOException.throwIt((short) 0xF0B4);

        setCurveParams(pubKeyT); // set curve parameters for the public key 
        pubKeyT.setW(baTemp, (short)0, offset); // initialize public key using baTemp
        lastInst = INS_REC_PUB;
    }

    //Generate a key Pair and send the public key to the terminal
    private void procGenKeyPair(APDU apdu) 
    {
        try 
        {       
            setCurveParams(kpC); // set curve parameters;
            kpC.genKeyPair(); // generate key pair          
            privKeyC = (ECPrivateKey) kpC.getPrivate(); // initialize key objects
            pubKeyC = (ECPublicKey) kpC.getPublic();            
            len = pubKeyC.getW(baTemp, (short)0); // write public key to baTemp         
            sendByteArray(apdu, baTemp, len); // send public key to terminal
        }
        catch(CryptoException e) 
        {
            ISOException.throwIt((short) 0xF0B1);
        }
        lastInst = INS_GEN_PAIR;
    }

    //Establish a secure channel thanks to Elliptic Curve Diffie-Hellman
    private void procGenSharedSecret(APDU apdu) 
    {
        try
        {
            short lenY = pubKeyT.getW(baTemp, (short) 0); // write public key of the terminal to baTemp
            ecdhC.init(privKeyC); // initialize KeyAgreement with privKeyC      
            short lenK = ecdhC.generateSecret(baTemp, (short) 0, lenY , baTemp, lenY); // generate secret using pubKeyT, write the result to baTemp
            /** KDF input array
             K = SHA-1(shared secret)
             X = public key of the chip
             i = KDF parameter
             KDF_i(K|X) = SHA-256(K|X|i), len(K) + len(X) + len(i) = 20 + 65 + 1 = 86 bytes.    
             **/
            short lenX = pubKeyC.getW(baTemp, (short)(lenY + lenK)); // concat public key of the chip to KDF_in
            len = (short) (lenY + lenK + lenX);
            baTemp[len]= (byte) 1;
            lenK = md.doFinal(baTemp, lenY, (short) 86, baTemp, (short) (len + 1)); // store encryption key in kEnc using KDF_1     
            Util.arrayCopyNonAtomic(baTemp, (short) (len+1), kEnc, (short) 0, (short) kEnc.length);
            md.reset();         
            baTemp[len] = (byte) 2;             
            len = md.doFinal(baTemp, lenY, (short) 86, kVerify, (short) 0); // store verification key in kVerify using KDF_2
            md.reset();     
            sendByteArray(apdu, kEnc, (short) kEnc.length);
        }
        catch(CryptoException e)
        {
            ISOException.throwIt((short) 0xF0B3);
        }
        lastInst = INS_GEN_SECRET;
    }

    //Compare verification keys 
    private void procVerify(APDU apdu) 
    {
        recByteArray(apdu); //Received bytes are placed into baTemp

        // compare verification key of the terminal with kVerify
        if(Util.arrayCompare(kVerify, (short) 0, baTemp, (short) 0, (short) kVerify.length) != 0)
            ISOException.throwIt((short) 0xF0B4);

        lastInst = INS_REC_VERIFY;
        protocolFlag = INS_REC_VERIFY;
    }

    //===========================================================================================================================================================
    //= DATA AUTHENTICATION =====================================================================================================================================
    //===========================================================================================================================================================

    //Send the issuer's public key
    private void send_pk_issuer(APDU apdu) 
    {
        len = padArray((byte[]) eepromData[INDEX_PKissuer], baTemp, (short) 65, (short) 80);
        sendEncryptedByteArray(apdu, kEnc, baTemp, len);        
        lastInst = INS_SEND_PK_ISSUER;
    }

    //Receive h, send U
    private void rec_h_send_U(APDU apdu) 
    {
        decryptReceivedAPDU(apdu, kEnc, h, (short) 32); // to be used later to verify h=H_5(v||r)
        try { // send U 
            setCurveParams(kpC); // set curve parameters            
            kpC.genKeyPair(); // generate key pair(u and U)
            //initialize public and private key
            privKeyC = (ECPrivateKey) kpC.getPrivate(); // u
            pubKeyC = (ECPublicKey) kpC.getPublic(); // U               
            len = pubKeyC.getW(baTemp, (short) 0); 
            len = padArray(baTemp, baTemp, len,  (short) 80); // send public key to terminal
            sendEncryptedByteArray(apdu, kEnc, baTemp, len);
        }
        catch(CryptoException e) {
            ISOException.throwIt((short)0xF0C1);
        }
        lastInst = INS_REC_H_SEND_U;
    }

    //Send R
    private void send_R(APDU apdu)
    {
        //retrieve R from eeprom sizeR||RsizeS||s
        //byte[] sigma= (byte[]) eepromData[signatureIndex];
        //short lenR = Util.makeShort(sigma[0], sigma[1]);
        // R must be 65 bytes
        Util.arrayCopyNonAtomic((byte[]) eepromData[INDEX_SIG], (short) 2, baTemp, (short) 0, (short) 65);      
        len = padArray(baTemp, baTemp, (short) 65, (short) 80);
        sendEncryptedByteArray(apdu, kEnc, baTemp, len); // send R to the terminal.
        lastInst = INS_SEND_R;
    }

    //Receive v
    private void rec_v(APDU apdu)
    {   
        decryptReceivedAPDU(apdu, kEnc, v, (short) 32); //Decrypt v and do everything we do for v in the following              
        Bignat vBig = new Bignat(v); 
        if(vBig.is_zero()) 
            ISOException.throwIt((short)0xF0C2);    //Abort if v is zero
        if(!vBig.lesser(ord)) 
            ISOException.throwIt((short)0xF0C3); //Abort if v is not in Z_q

        lastInst = INS_REC_v;
    }
    
    //Receive r
    private void rec_r(APDU apdu) 
    {   
        decryptReceivedAPDU(apdu, kEnc, r, (short) 32);
        // KDF input array
        // v, r, and i = KDF parameter
        // KDF_i(v|r) = SHA-256(v|r|i), len(v) + len(r) + len(i) = 32 + 32 + 1 = 65 bytes.      
        //v stores SHA-1 digest of the shared secret, write it to KDF_in
        Util.arrayCopyNonAtomic(v, (short) 0, baTemp, (short) 0, (short) 32);   
        Util.arrayCopyNonAtomic(r, (short) 0, baTemp, (short) 32, (short) 32);  
        baTemp[64] = (byte) 5; // set KDF parameter             
        len = md.doFinal(baTemp, (short) 0, (short) 65, baTemp, (short) 0); // store KDF_5(v|r) in h_verify using KDF_5 

        if (Util.arrayCompare(h, (short) 0, baTemp, (short) 0, (short) h.length) != 0) 
            ISOException.throwIt((short) 0xF0B4);

        md.reset();
        lastInst = INS_REC_r;
    }
    
    //Send s'
    private void send_new_s(APDU apdu)
    {
        // IMPORTANT: changes in Bignat changes the initialized byte array.
        // Example: if vBig = 0. All elements in the byte array v will become 0.
        // read s from eeprom [ (2 bytes to store the size of R = 65) || R|| (2 bytes to store the size of s=32) | s
        Bignat vBig = new Bignat(v);
        privKeyC.getS(temp, (short)0); // store private key u inside temp
        Bignat uBig = new Bignat(temp);
        Util.arrayCopyNonAtomic((byte[]) eepromData[INDEX_SIG], (short) 69, temp2, (short) 0, (short) 32);
        Bignat sBig = new Bignat(temp2);
        Bignat sBigNew = new Bignat(temp3);

        sBigNew.mod_mult(vBig, uBig, ord);

        sBigNew.mod_add(sBig, ord);
        // send sBigNew = s' to terminal
        sendEncryptedByteArray(apdu, kEnc, sBigNew.asByteArray(), sBigNew.length());
        lastInst = INS_SEND_NEW_s;
    }

    //===========================================================================================================================================================
    //= LOAD BIOMETRY ===========================================================================================================================================
    //===========================================================================================================================================================

    private void requestBioDataStorage(APDU apdu)
    {       
        recByteArray(apdu); //Received bytes are placed into baTemp

        short size = Util.makeShort(baTemp[0], baTemp[1]); 
        short dataID = Util.makeShort(baTemp[2], baTemp[3]);

        if (JCSystem.getAvailableMemory(JCSystem.MEMORY_TYPE_PERSISTENT) < (short) (size + (short) 16))
            ISOException.throwIt((short) 0x6000); // no space available exception

        if (dataID < MAX_BIODATA_TYPES) 
        {
            if (eepromData[dataID] == null) 
            {
                eepromData[dataID] = new byte[size];
                eepromDataReady[dataID] = true;           
            }
        } 
        else
            ISOException.throwIt((short) 0x6000); // invalid dataID exception
    }

    private void storeBioDataInEEPROM(APDU apdu) throws ISOException
    {
        recByteArray(apdu); //Received bytes are placed into baTemp

        short dataID = Util.makeShort(baTemp[0], baTemp[1]);
        short offset = Util.makeShort(baTemp[2],baTemp[3]);
        short size = Util.makeShort(baTemp[4], baTemp[5]);

        if (dataID >= MAX_BIODATA_TYPES)
            ISOException.throwIt((short) 0x6000); // invalid dataID exception

        if (eepromData[dataID] == null)
            ISOException.throwIt((short) 0x6001); // no biometry data is stored

        if (((short) (offset +  size)) > ( (byte[]) eepromData[dataID]).length)
            ISOException.throwIt((short) 0x6003); //invalid argument value

        Util.arrayCopyNonAtomic(baTemp, (short)6 , (byte[]) eepromData[dataID], offset, size);
    }

    private void sendBioDataSize(APDU apdu) throws ISOException 
    {
        recByteArray(apdu); //Received bytes are placed into baTemp

        short dataID = Util.makeShort(baTemp[0], baTemp[1]);

        if (dataID >= MAX_BIODATA_TYPES)
            ISOException.throwIt((short) 0x6000); // invalid dataID exception

        if(dataID==INDEX_DG2 || dataID==INDEX_Bio || dataID==INDEX_PKissuer || dataID==INDEX_SIG || dataID==INDEX_PWDG) 
        {
            if (eepromData[dataID] != null) 
            {
                short size= (short) ((byte[]) eepromData[dataID]).length;
                baTemp[1] = (byte)(size & 0xff);
                baTemp[0] = (byte)((size >> 8) & 0xff);
                sendByteArray(apdu,baTemp, (short) 2);
            } 
            else
                ISOException.throwIt((short) 0x6001); // no biometry data is stored
            lastInst= INS_REQ_DATASIZE;
        }
    }

    private void sendEEPROMBioData(APDU apdu) throws ISOException
    {
        recByteArray(apdu); //Received bytes are placed into baTemp

        short dataID = Util.makeShort(baTemp[0], baTemp[1]);
        short offset = Util.makeShort(baTemp[2], baTemp[3]);
        short size = Util.makeShort(baTemp[4], baTemp[5]);

        if (dataID >= MAX_BIODATA_TYPES)
            ISOException.throwIt((short) 0x6000); //Invalid dataID exception

        if(dataID == INDEX_DG2 || dataID == INDEX_Bio || dataID == INDEX_PKissuer || dataID == INDEX_SIG || dataID == INDEX_PWDG) 
        {
            if ((eepromData[dataID] == null) || eepromDataReady[dataID]==false)
                ISOException.throwIt((short) 0x6001); //No biometry data is stored at index dataID

            if (((short) (offset +  size)) > ((byte[]) eepromData[dataID]).length)
                ISOException.throwIt((short) 0x6003); //Invalid argument exception

            Util.arrayCopy((byte[]) eepromData[dataID], offset, bufferTemp, (short) 0, size);
            len = padArray(bufferTemp, bufferTemp, size, (short) bufferTemp.length);
            len = (short) (size + ((16 - size % 16 ) % 16));

            if (protocolFlag == INS_REC_VERIFY)
                sendEncryptedByteArray(apdu, kEnc, bufferTemp, len);
            if (protocolFlag == INS_REC_KL)
                sendEncryptedByteArray(apdu, kEncWAC, bufferTemp, len);

            lastInst = INS_SEND_EEPROM;
        }
    }

    private void deleteEEPROMdata(APDU apdu) throws ISOException 
    {
        recByteArray(apdu); //Received bytes are placed into baTemp

        short dataID = Util.makeShort(baTemp[0], baTemp[1]);

        if(dataID >= MAX_BIODATA_TYPES)
            ISOException.throwIt((short) 0x6000); // invalid dataID exception

        if(eepromData[dataID] != null) 
        {
            eepromData[dataID] = null;
            JCSystem.requestObjectDeletion();
            eepromDataReady[dataID] = false;
        } 
        //else
        //ISOException.throwIt((short) 0x6001); // no biometry data is stored
        //TODO : No point in throwing an exception when deleting already absent data, right? 
    }
    
    //===========================================================================================================================================================
    //= HELPERS =================================================================================================================================================
    //===========================================================================================================================================================

    private static short padArray(byte[] inputData, byte[] outputData, short inputDataLength, short outputDataLength)
    {
        //We prepare the data for encryption by padding it to outputDataLength bytes 
        Util.arrayCopyNonAtomic(inputData, (short) 0, outputData, (short) 0, inputDataLength);
        while(inputDataLength < outputDataLength)
        {
            outputData[inputDataLength] = (short) 0;
            inputDataLength++;
        }

        return inputDataLength;
    }

    private void sendEncryptedByteArray(APDU apdu, byte[] key, byte[] data, short len) 
    {
        byte[] buffer = apdu.getBuffer();
        aesKey.setKey(key, (short)0);
        aesCipher.init(aesKey, Cipher.MODE_ENCRYPT, IV, (short) 0, (short) 16); 
        aesCipher.doFinal(data, (short)0, len, buffer, (short)0);
        if (len % 16 != (short) 0) 
            ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        
        sendByteArray(apdu, buffer, len);
    }

    private void decryptReceivedAPDU(APDU apdu, byte[] key, byte[] output_data, short expected_len) 
    {
        short offset = recByteArray(apdu);

        if (offset != expected_len)
        {
            aesKey.setKey(key, (short) 0);
            aesCipher.init(aesKey, Cipher.MODE_DECRYPT, IV, (short) 0, (short) 16); 
            byte[] outputData = JCSystem.makeTransientByteArray(offset, JCSystem.CLEAR_ON_RESET);
            aesCipher.doFinal(baTemp, (short)0, offset, outputData, (short)0);
            Util.arrayCopyNonAtomic(outputData, (short) 0, output_data, (short) 0, expected_len);
        }  
        else 
        {
            aesKey.setKey(key, (short) 0);
            aesCipher.init(aesKey, Cipher.MODE_DECRYPT, IV, (short) 0, (short) 16); 
            aesCipher.doFinal(baTemp, (short)0, offset, output_data, (short)0);
        }

    }

    private static void setCurveParams(KeyPair kp) 
    {
        ECPublicKey tempPub = (ECPublicKey) kp.getPublic(); // obtain empty public and private keys from KeyPair to set curve parameters
        ECPrivateKey tempPriv = (ECPrivateKey) kp.getPrivate();
        setCurveParams(tempPub);    // set curve parameters for public key
        setCurveParams(tempPriv); // set curve parameters for private key
    }

    private static void setCurveParams(ECKey ecKey) 
    {
        ecKey.setFieldFP(p, (short)0, (short) p.length);
        ecKey.setA(a, (short)0, (short) a.length);
        ecKey.setB(b, (short)0, (short) b.length);
        ecKey.setG(G, (short)0, (short) G.length);
        ecKey.setR(q, (short)0, (short) q.length);
        ecKey.setK((short) 1); // according to secp256r1 specification.
    }

    private void sendByteArray(APDU apdu, byte[] data, short len) 
    {
        apdu.setOutgoing();
        apdu.setOutgoingLength(len);
        apdu.sendBytesLong(data, (short)0, len);
    }

    //Stores incoming apdu data bytes into baTemp, and returns the number of bytes read.
    private short recByteArray(APDU apdu)
    {
        byte[] buffer = apdu.getBuffer();    
        short bytesRead = apdu.setIncomingAndReceive(); // receive data, number of bytes read
        short offset = 0; // offset for the current data 
        
        while(bytesRead > 0) // read until no there are no more data
        { 
            // read data from buffer and write it to baTemp starting from the offset
            Util.arrayCopyNonAtomic(buffer, ISO7816.OFFSET_CDATA, baTemp, offset, bytesRead);
            offset += bytesRead; // update offset           
            bytesRead = apdu.receiveBytes(ISO7816.OFFSET_CDATA); // read remaining data
        }

        return offset;
    }

    //===========================================================================================================================================================
    //= SECP256R1 ===============================================================================================================================================
    //===========================================================================================================================================================

    //Prime modulus p
    final static byte[] p = {
            (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0x00, 
            (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0x00, 
            (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, 
            (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, 
            (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, 
            (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, 
            (byte) 0xff, (byte) 0xff};

    //Coefficient a
    final static byte[] a = {
            (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0x00, 
            (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0x00, 
            (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, 
            (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, 
            (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, 
            (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, 
            (byte) 0xff, (byte) 0xfc};

    //Coefficient b
    final static byte[] b = {
            (byte) 0x5a, (byte) 0xc6, (byte) 0x35, (byte) 0xd8, (byte) 0xaa, 
            (byte) 0x3a, (byte) 0x93, (byte) 0xe7, (byte) 0xb3, (byte) 0xeb, 
            (byte) 0xbd, (byte) 0x55, (byte) 0x76, (byte) 0x98, (byte) 0x86,
            (byte) 0xbc, (byte) 0x65, (byte) 0x1d, (byte) 0x06, (byte) 0xb0, 
            (byte) 0xcc, (byte) 0x53, (byte) 0xb0, (byte) 0xf6, (byte) 0x3b, 
            (byte) 0xce, (byte) 0x3c, (byte) 0x3e, (byte) 0x27, (byte) 0xd2, 
            (byte) 0x60, (byte) 0x4b};

    //Generator G of the curve's group
    final static byte[] G = {
            (byte) 0x04, (byte) 0x6b, (byte) 0x17, (byte) 0xd1, (byte) 0xf2, 
            (byte) 0xe1, (byte) 0x2c, (byte) 0x42, (byte) 0x47, (byte) 0xf8,
            (byte) 0xbc, (byte) 0xe6, (byte) 0xe5, (byte) 0x63, (byte) 0xa4, 
            (byte) 0x40, (byte) 0xf2, (byte) 0x77, (byte) 0x03, (byte) 0x7d, 
            (byte) 0x81, (byte) 0x2d, (byte) 0xeb, (byte) 0x33, (byte) 0xa0, 
            (byte) 0xf4, (byte) 0xa1, (byte) 0x39, (byte) 0x45, (byte) 0xd8, 
            (byte) 0x98, (byte) 0xc2, (byte) 0x96, (byte) 0x4f, (byte) 0xe3, 
            (byte) 0x42, (byte) 0xe2, (byte) 0xfe, (byte) 0x1a, (byte) 0x7f, 
            (byte) 0x9b, (byte) 0x8e, (byte) 0xe7, (byte) 0xeb, (byte) 0x4a, 
            (byte) 0x7c, (byte) 0x0f, (byte) 0x9e, (byte) 0x16, (byte) 0x2b, 
            (byte) 0xce, (byte) 0x33, (byte) 0x57, (byte) 0x6b, (byte) 0x31, 
            (byte) 0x5e, (byte) 0xce, (byte) 0xcb, (byte) 0xb6, (byte) 0x40, 
            (byte) 0x68, (byte) 0x37, (byte) 0xbf, (byte) 0x51, (byte) 0xf5};

    //Order of G modulo p
    final static byte[] q = {
            (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0x00, 
            (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xff, (byte) 0xff, 
            (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff, (byte) 0xff,
            (byte) 0xff, (byte) 0xbc, (byte) 0xe6, (byte) 0xfa, (byte) 0xad, 
            (byte) 0xa7, (byte) 0x17, (byte) 0x9e, (byte) 0x84, (byte) 0xf3, 
            (byte) 0xb9, (byte) 0xca, (byte) 0xc2, (byte) 0xfc, (byte) 0x63, 
            (byte) 0x25, (byte) 0x51};
}
