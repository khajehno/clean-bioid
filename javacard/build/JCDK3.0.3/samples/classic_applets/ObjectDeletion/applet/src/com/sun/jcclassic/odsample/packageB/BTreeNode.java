/** 
 * Copyright (c) 2010, 2010, Oracle and/or its affiliates. All rights reserved. 
 */

/*
 */

package com.sun.jcclassic.samples.odsample.packageB;

/**
 * Class represents node in a tree contained in B.java
 */
public class BTreeNode {
    public short data;
}
