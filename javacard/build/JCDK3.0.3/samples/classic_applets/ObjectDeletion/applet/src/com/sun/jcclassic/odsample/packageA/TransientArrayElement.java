/** 
 * Copyright (c) 2010, 2010, Oracle and/or its affiliates. All rights reserved. 
 */

/*
 */

package com.sun.jcclassic.samples.odsample.packageA;

/**
 * Class represents object used as elements of transient array
 */
public class TransientArrayElement {
    public short data;

    public TransientArrayElement(short s) {
        data = s;
    }
}
