/** 
 * Copyright (c) 2010, 2010, Oracle and/or its affiliates. All rights reserved. 
 */

/*
 */

package com.sun.jcclassic.samples.odsample.libPackageC;

/**
 * package AID - 0xA0 0x00 0x00 0x00 0x62 0x03 0x01 0x0C 0x07 0x03
 *
 * This class is the only one in this package which is used to demonstrate
 * dependencies on package and package deletion.
 */
public class C {
    public static short DATA = 1; // constant used by other packages
}
