# Clean BioID
##### Khajehnouri Shayan 
##### JANUARY 2022

## 1. GENERAL STRUCTURE

This repository contains code relative to an Android application client for BioID as well as information to install BioID applets on smart cards. The description of the files that were recovered from the old BioID project comes from the README file written by Julien Corsin.
Some explications on the structure of the code.

- app/src/main/ : 

    - **python** : Contains all python files needed for the extraction of the veins and the calculation of the score.

        - biometrics (Python package)
        - utils (Python package)
	    - biocore : file that contains the "extract_features" function
	    - finger : file that contains the "fingerfocus" function
	    - scores : file that contains the "miurascore" function
	    - main : python file called in the java code

    - **java/** : 

        - **com/shayan/BioID** : contains the classes representing the different screens and parts of the app
        
            - Main : the first screen you have at launch
            - Connection : where you set the connection settings
            - Enrollment : where you enroll
            - Matching : where you do the Matching


        - **processes** : contains the classes core of the BioID project, containing all the communications processes

            - LBT : contains the "Enrollment" protocol
            - SAC_DCA : contains the "Strong Access Control" protocol
            - Terminal : contains all communications with the smart cards functions
            - WAC_DCA : contains the "Weak Access Control" protocol

        - **libraries** : contains files that couldn't be imported, so were copy pasted manually
        - **tools** : contains tools needed for the files in **id**

    - **res** : contains every design resource
        - layout : contains the files that describe each screen display

- app/build.gradle : file that defines the build configuration. To import more python libraries, you first need to write them in here (after checking that the libraries are available on Chaquopy : https://chaquo.com/pypi-7.0/)


## 2. INSTALL APP PROCEDURE

1. Clone the project on your computer.
2. Modify the "local.properties" file, situated in the main directory. You need to write the path to your Android SDK (if you don't have it, download it).
3. Modify "build.gradle" (the module one), where you have to write the path to your Python folder (if you don't find the file, try to open the project in Android Studio and use the Android view of the files).
4. Follow the steps on : https://developer.android.com/studio/run/device to run the app on your device.
5. You are ready to deploy the code on your device.


## 3. NOTES


- If you want to install the Java Card applet on a smart card, go and check Julien's README in the "javacard" folder (more precisely, find the procedure to follow at "id_cardappplet").

- To find a guide on how to use the app, you can check the pdf of my report on the repository.

- This is the "clean" git version of my project, if you want commit details, you may find what you want here : https://gitlab.epfl.ch/khajehno/BioID.

- As it uses some code from the "ApduSenderContactLess" app (https://github.com/jmarroyo/ApduSenderContactLess) and the code is distributed under GPL version 3, so is this code project.
